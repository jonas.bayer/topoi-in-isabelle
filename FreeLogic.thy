theory FreeLogic imports Main LaTeXsugar

begin 

locale free_logic = 
  fixes fExistence :: "'a\<Rightarrow>bool" ("E") (*Existence/definedness predicate in free logic*)

begin

named_theorems fl

definition KlEq (infixr "\<cong>" 56) (* Kleene equality *) 
   where "x \<cong> y \<equiv> (E x \<or> E y) \<longrightarrow> x = y"  

definition ExId (infixr "\<simeq>" 56) (* Existing identity *)   
   where "x \<simeq> y \<equiv> E x \<and> E y \<and> x = y"

declare KlEq_def[fl] ExId_def[fl]

end

end
