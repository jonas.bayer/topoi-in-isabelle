theory DistributiveProperty imports BinaryProductCategory

begin

context binaryProductCategory
begin

lemma "True" nitpick[satisfy, card=5] (* any cardinality seems to work *) oops

text \<open>A test on distributivity of products and coproducts:\<close>
lemma
  fixes A B C :: "'a"
  assumes "isType A" and "isType B" and "isType C"
  assumes "isCoproduct B \<midarrow>i1\<rightarrow> B_C \<leftarrow>i2\<midarrow> C" 
      and "isCoproduct (A \<^bold>\<times> B) \<midarrow>j1\<rightarrow> AB_AC \<leftarrow>j2\<midarrow> (A \<^bold>\<times> C)"
  shows "(A \<^bold>\<times> B_C) isomorphicTo AB_AC"
  (* nitpick[card=6] *) 
  (* no counterexamples for card \<le> 6 *)
  oops

end

end