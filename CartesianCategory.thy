theory CartesianCategory imports BinaryProductCategory

begin

section \<open>Cartesian Categories\<close>

locale cartesianCategory = binaryProductCategory + 
  fixes 
          equalizer :: "'a \<Rightarrow> 'a \<Rightarrow> 'a" (infix "\<doteq>" 120) and
          finalObject :: "'a" ("\<one>")
  assumes 
          equalizer:   "isParallelPair f g \<longrightarrow> (f \<doteq> g) equalizerOf f g" and
          finalObject: "isFinal \<one>" and

          equalizerImpliesParallelPair: "E (f \<doteq> g) \<longrightarrow> isParallelPair f g"
begin
  lemma "True"  oops
    
  lemma equalizerMap: "isParallelPair f g \<Longrightarrow> (f \<doteq> g):dom (f\<doteq>g)\<rightarrow>(dom f)" and 
                      "isParallelPair f g \<Longrightarrow> (f \<doteq> g):dom (f\<doteq>g)\<rightarrow>(dom g)"
    using equalizer unfolding equalizerOf_def equalizes_def arrow_def
    using local.compositionExistence local.factorsUniquelyThrough_def fl by auto

subsection \<open>Construction of the final map\<close>

  text \<open>This lemma motivates the following definition\<close>
  lemma "\<forall>t. \<exists>!x. (isType t \<longrightarrow> x:t\<rightarrow>\<one>) \<and> (\<not> isType t \<longrightarrow> x = *)"
    using finalObject domImpliesExistence isFinal_def by smt
  
  definition finalMap :: "'a \<Rightarrow> 'a" ("!\<^sub>1") where
    "!\<^sub>1 t \<equiv> (THE x::'a. (isType t \<longrightarrow> x:t\<rightarrow>\<one>) \<and> (\<not> isType t \<longrightarrow> x = *))"
    (* \<longrightarrow> use Free Logic THE operator JAR paper [Achtung: Fehler im Haupttext] *)
  
  lemma finalMap: "isType t \<Longrightarrow> !\<^sub>1 t:t\<rightarrow>\<one>" 
  proof -  
    assume "isType t"
    hence 1: "!\<^sub>1 t = (THE x::'a. x:t\<rightarrow>\<one>)" using finalMap_def by auto
    have "\<exists>!x. x:t\<rightarrow>\<one>" using isFinal_def finalObject \<open>isType t\<close> by metis
    hence "!\<^sub>1 t:t\<rightarrow>\<one>" using 1 theI'[of "\<lambda>x. x:t\<rightarrow>\<one>"] by smt
    thus ?thesis by auto
  qed
  
  lemma contraposition: "\<not>B \<longrightarrow> \<not>A \<Longrightarrow> A \<longrightarrow> B" 
    by blast

  lemma finalMapImpliesType: "E (!\<^sub>1 t) \<Longrightarrow> isType t" 
    apply (rule contrapos_pp[of "E (!\<^sub>1 t)"]) using inexistentElement finalMap_def by auto

subsection \<open>Properties of the final object\<close>

  lemma product_final: "isType A \<longrightarrow> isProduct \<one> \<leftarrow>!\<^sub>1 A\<midarrow> A \<midarrow>A\<rightarrow> A"
    using local.finalObject apply (simp add: isProduct_def isWedge_def isFinal_def)
    using finalMap[of A] fl
    local.arrowImpliesIsType local.arrow_def local.associativity local.codImpliesExistence 
    local.compositionExistence local.domImpliesExistence local.isType_def local.leftCompIdentity 
    local.rightCompIdentity
    unfolding fl by metis (* takes long *)
  
  lemma isomorphicToProductWithFinal: "isType A \<Longrightarrow> A isomorphicTo (\<one> \<^bold>\<times> A)"
  proof - 
    assume "isType A"
    hence "isProduct \<one> \<leftarrow>\<pi>\<^sub>1 (\<one> \<^bold>\<times> A)\<midarrow> (\<one> \<^bold>\<times> A) \<midarrow>\<pi>\<^sub>2 (\<one> \<^bold>\<times> A)\<rightarrow> A" 
      using \<open>isType A\<close> local.productOfTypes[of "\<one>" A] finalObject isFinal_def by auto
    moreover have "isProduct \<one> \<leftarrow>!\<^sub>1 A\<midarrow> A \<midarrow>A\<rightarrow> A" 
      using \<open>isType A\<close> product_final[of A] by auto
    ultimately show ?thesis using products_isomorphic[where ?A="\<one>" and ?P="\<one> \<^bold>\<times> A" and ?B ="A"
                                                      and ?Q=A and ?q1.0="!\<^sub>1 A" and ?q2.0 = A 
                                                      and ?p1.0="\<pi>\<^sub>1 (\<one> \<^bold>\<times> A)" and ?p2.0="\<pi>\<^sub>2 (\<one> \<^bold>\<times> A)"] 
    by blast
  qed 

subsection \<open>Concatentation and the final map\<close>
  
  lemma finalMapConcatenation: "E s \<Longrightarrow> !\<^sub>1 (cod s) \<cdot> s \<simeq> !\<^sub>1 (dom s)"
  proof - 
    assume "E s"
    hence "isType (cod s) \<and> isType (dom s)" 
      by (metis compositionExistence local.isType_def local.leftCompIdentity 
          local.rightCompIdentity fl)
    hence "!\<^sub>1 (cod s) \<cdot> s:(dom s)\<rightarrow>\<one>"
      using finalMap[of "cod s"] unfolding arrow_def by (smt fl catAx)
    thus ?thesis 
      using finalObject unfolding isFinal_def using finalMap catAx 
      by (smt fl \<open>isType (cod s) \<and> isType (dom s)\<close> local.products_isomorphism product_final) 
  qed
  
  declare finalMapConcatenation[cat] product_final[cat] isomorphicToProductWithFinal[cat]
          finalMap[cat] finalMapImpliesType[cat]

end

end