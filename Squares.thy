theory Squares imports Category

begin

named_theorems squares

context category 
begin

text \<open>Convention: The labels of vertical arrows are always written on the outside or if the arrow
      is in the center of the diagram on the left. \<close>

definition RDSquareCommutes :: "'a \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> bool" where
  "RDSquareCommutes f h k g \<equiv> h \<cdot> f \<simeq> k \<cdot> g"

abbreviation squareRDDiagram :: "'a \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> bool"
 ("squareCommutes \<box> \<midarrow>_\<rightarrow> \<box> // _\<down> \<down>_ // \<box> \<midarrow>_\<rightarrow> \<box>") where
  "squareCommutes
     \<box> \<midarrow>g\<rightarrow> \<box> 
    f\<down>      \<down>k
     \<box> \<midarrow>h\<rightarrow> \<box>  
  \<equiv> RDSquareCommutes f h k g"

abbreviation doubleSquareRDDiagram :: "'a \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> bool" 
 ("doubleSquareCommutes \<box> \<midarrow>_\<rightarrow> \<box> \<midarrow>_\<rightarrow> \<box> _\<down> _\<down> \<down>_ \<box> \<midarrow>_\<rightarrow> \<box> \<midarrow>_\<rightarrow> \<box>") where
  "doubleSquareCommutes
     \<box> \<midarrow>k\<rightarrow> \<box> \<midarrow>m\<rightarrow> \<box>
    h\<down>      i\<down>      \<down>j
     \<box> \<midarrow>f\<rightarrow> \<box> \<midarrow>g\<rightarrow> \<box> 
  \<equiv> squareCommutes
     \<box> \<midarrow>k\<rightarrow> \<box>
    h\<down>       \<down>i
     \<box> \<midarrow>f\<rightarrow> \<box>
  \<and> squareCommutes
     \<box> \<midarrow>m\<rightarrow> \<box>
    i\<down>      \<down>j
     \<box> \<midarrow>g\<rightarrow> \<box>"

lemma outerSquareCommutes:
  assumes "doubleSquareCommutes
              \<box> \<midarrow>k\<rightarrow> \<box> \<midarrow>m\<rightarrow> \<box>
             h\<down>      i\<down>       \<down>j
              \<box> \<midarrow>f\<rightarrow> \<box> \<midarrow>g\<rightarrow> \<box>"
  shows "squareCommutes
         \<box> \<midarrow> m\<cdot>k \<rightarrow> \<box>
        h\<down>          \<down>j
         \<box> \<midarrow> g\<cdot>f \<rightarrow> \<box>"
  using assms unfolding squares using catAx fl by (smt RDSquareCommutes_def)

 
declare RDSquareCommutes_def[squares] outerSquareCommutes[squares]

end

end