theory BinaryProductCategory imports Constructions

begin

section \<open>Categories with binary products\<close>

locale binaryProductCategory = category +
    fixes product :: "'a \<Rightarrow> 'a \<Rightarrow> 'a" (infix "\<^bold>\<times>" 120) and
          projection1 :: "'a \<Rightarrow> 'a" ("\<pi>\<^sub>1") 
          (* Takes a product and returns the morphism from the product to its first factor *) and
          projection2 :: "'a \<Rightarrow> 'a" ("\<pi>\<^sub>2")
          (* Takes a product and returns the morphism from the product to its second factor *)
  assumes 
          productOfTypes:  "isType A \<and> isType B \<longrightarrow> 
                            isProduct A \<leftarrow>\<pi>\<^sub>1 (A \<^bold>\<times> B)\<midarrow> (A \<^bold>\<times> B) \<midarrow>\<pi>\<^sub>2 (A \<^bold>\<times> B)\<rightarrow> B" and
          
          productExistence:     "E (A \<^bold>\<times> B) \<longrightarrow> (isType A \<and> isType B)" and
          projection1Existence: "E (\<pi>\<^sub>1 A) \<longrightarrow> (\<exists>B C. A \<simeq> B \<^bold>\<times> C)" and
          projection2Existence: "E (\<pi>\<^sub>2 A) \<longrightarrow> (\<exists>B C. A \<simeq> B \<^bold>\<times> C)"
          
begin

subsection \<open>Comments on the axioms of binaryProductCategories\<close>

(* 
  It was for some time unclear what the best RHS of the latter two implications is. 
  Possibilities included
   (1) E A
       may be to weak. Allows \<pi>\<^sub>1 to exist if A is only a morphism which is bad
   (2) isType A
       also potentially to weak. It is possible that A is a type that does not have factors
   (3) (\<exists>B C p1 p2. isProduct B \<leftarrow>p1\<midarrow> A \<midarrow>p2\<rightarrow> C) = factorizes A
       probably the correct one but rather complicated
   (4) (\<exists>B C. A \<simeq> B \<^bold>\<times> C) 
       nice because shorter but potentially to strong. Requires A to be equal to the "chosen"
       product. 
   (5) (\<exists>B C. A isomorphicTo B \<^bold>\<times> C)
       equivalent to (3) [check this again] and maybe nicer to use

  New thoughts: The map \<pi>\<^sub>1 should really only exist for the chosen products, that is if A = B \<^bold>\<times> C.
  i.e. the right choice is (4). 
  Requiring one of the weaker versions would mean that \<pi>\<^sub>1 needs to be defined for all products. But
  it is e.g. not clear for an arbitrary product P which of its projection maps should be \<pi>\<^sub>1 and 
  which one \<pi>\<^sub>2.
 *)

declare productOfTypes[cat] productExistence[cat] projection1Existence[cat] 
        projection2Existence[cat]

lemma "True" nitpick[satisfy, card=4] oops

subsection \<open>Basic statements on the chosen product\<close>

  lemma isomorphicToChosenProduct:
    assumes "isProduct A \<leftarrow>p1\<midarrow> P \<midarrow>p2\<rightarrow> B"
      shows "P isomorphicTo (A \<^bold>\<times> B)"
    by (smt assms local.arrowImpliesIsType local.isProduct_def local.isWedge_def productOfTypes 
        local.products_isomorphic)
  
  lemma product_switch:
    assumes "isProduct A \<leftarrow>p1\<midarrow> (A \<^bold>\<times> B) \<midarrow>p2\<rightarrow> B"
      shows "isProduct B \<leftarrow>p2\<midarrow> (A \<^bold>\<times> B) \<midarrow>p1\<rightarrow> A"
    using assms unfolding pdefs by blast
  
  lemma productIsType: "isType A \<and> isType B \<longleftrightarrow> isType (A \<^bold>\<times> B)"
    by (meson fl isomorphicToChosenProduct local.arrowImpliesIsType local.isType_def 
        local.isomorphicTo_def local.isomorphismArrow_def local.productExistence productOfTypes)
  
  lemma productExistsImpliesIsType: "E (A \<^bold>\<times> B) \<longrightarrow> isType (A \<^bold>\<times> B)"
    by (meson isomorphicToChosenProduct local.arrowImpliesIsType local.isType_def 
        local.isomorphicTo_def local.isomorphismArrow_def local.productExistence productOfTypes)
  
  lemma projection1Map: "isType A \<longrightarrow> isType B \<longrightarrow> (\<pi>\<^sub>1 (A \<^bold>\<times> B)):(A \<^bold>\<times> B)\<rightarrow>A"
    using local.productOfTypes isProduct_def isWedge_def by auto
  
  lemma projection2Map: "isType A \<longrightarrow> isType B \<longrightarrow> (\<pi>\<^sub>2 (A \<^bold>\<times> B)):(A \<^bold>\<times> B)\<rightarrow>B"
    using local.productOfTypes isProduct_def isWedge_def by auto
  
  lemma isomorphicToSwitch: "isType A \<and> isType B \<Longrightarrow> (A \<^bold>\<times> B) isomorphicTo (B \<^bold>\<times> A)"
    by (meson local.productOfTypes local.products_isomorphic product_switch)
  
  lemma productsEqualFactorsEqual: "A \<^bold>\<times> B \<simeq> C \<^bold>\<times> D \<longrightarrow> A \<simeq> C \<and> B \<simeq> D" 
    by (metis ExId_def local.arrow_def local.productExistence projection1Map projection2Map)
      
  lemma ternaryProductAssociative:
    assumes "isType A" and "isType B" and "isType C"
    shows "((A \<^bold>\<times> B) \<^bold>\<times> C) isomorphicTo (A \<^bold>\<times> (B \<^bold>\<times> C))"
    oops

subsection \<open>Factoring products\<close>

  definition factor1 :: "'a \<Rightarrow> 'a" ("\<Pi>\<^sub>1") where
    "\<Pi>\<^sub>1 P \<equiv> cod (\<pi>\<^sub>1 P)"
  
  definition factor2 :: "'a \<Rightarrow> 'a" ("\<Pi>\<^sub>2") where
    "\<Pi>\<^sub>2 P \<equiv> cod (\<pi>\<^sub>2 P)"
  
  definition factorizes :: "'a \<Rightarrow> bool" where
    "factorizes A \<equiv> (\<exists>B C. A \<simeq> B \<^bold>\<times> C)"
  
  declare factor1_def[pdefs] factor2_def[pdefs] factorizes_def[pdefs]
  
  lemma productTypeIsType: "factorizes A \<Longrightarrow> isType A" 
    using factorizes_def[of A]
    by (metis ExId_def local.arrowImpliesIsType local.productExistence local.productOfTypes 
        local.products_isomorphism)
  
  lemma productTypeFactor1IsType: "factorizes A \<Longrightarrow> isType (\<Pi>\<^sub>1 A)" 
    by (metis fl factor1_def factorizes_def local.compositionExistence isType_def 
        local.leftCompIdentity local.productExistence local.productOfTypes products_isomorphism)
  
  lemma productTypeFactor2IsType: "factorizes A \<Longrightarrow> isType (\<Pi>\<^sub>2 A)" 
    by (metis fl factor2_def factorizes_def local.compositionExistence isType_def 
        local.leftCompIdentity local.productExistence local.productOfTypes products_isomorphism)
  
  lemma "(\<exists>B C. A \<simeq> B \<^bold>\<times> C) \<longleftrightarrow> E (\<pi>\<^sub>1 A)"
    by (smt fl local.productExistence productOfTypes products_isomorphism projection1Existence)
   
  lemma multiplyFactors: "factorizes A \<Longrightarrow> A \<simeq> (\<Pi>\<^sub>1 A \<^bold>\<times> \<Pi>\<^sub>2 A)" 
    by (smt fl factor1_def factor2_def factorizes_def local.arrow_def isProduct_def 
        local.isWedge_def local.productExistence local.productOfTypes)
  
subsection \<open>Induced arrows\<close>
  
  text \<open>This topic is more generic and could be formalized in a more general manner in
        Constructions.thy. The special case when a chosen product exists would then be instantiated 
        here.

        The more general version would define the function inducedBy with an additional argument
        for the relevant product.\<close>

  definition inducedBy :: "'a \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> bool" ("_ inducedBy _ _" [120, 120, 120] 120)where
    "m inducedBy f g \<equiv> (\<not> dom f \<simeq> dom g \<longrightarrow> m = *) 
                     \<and> (  dom f \<simeq> dom g \<longrightarrow> \<pi>\<^sub>1 ((cod f) \<^bold>\<times> (cod g)) \<cdot> m \<simeq> f 
                                          \<and> \<pi>\<^sub>2 ((cod f) \<^bold>\<times> (cod g)) \<cdot> m \<simeq> g)"

  declare inducedBy_def[pdefs]
  
  lemma uniqueInducedBy: "\<exists>!m. m inducedBy f g"
  proof (cases "dom f \<simeq> dom g")
    case False
    show ?thesis using inducedBy_def Let_def False by auto
  next
    case True
    have areTypes:"isType (cod f) \<and> isType (cod g)"
      by (metis fl True local.compositionExistence local.domImpliesExistence local.isType_def 
          local.leftCompIdentity)
    hence "(cod f)\<leftarrow>f\<midarrow> (dom f) \<midarrow>g\<rightarrow>(cod g) \<longrightarrow> (\<exists>!u. \<pi>\<^sub>1((cod f) \<^bold>\<times> (cod g)) \<cdot> u \<simeq> f 
                                                   \<and> \<pi>\<^sub>2((cod f) \<^bold>\<times> (cod g)) \<cdot> u \<simeq> g)"
      using productOfTypes[of "cod f" "cod g"] isProduct_def by auto
    moreover have "(cod f) \<leftarrow>f\<midarrow> (dom f) \<midarrow>g\<rightarrow> (cod g)" using isWedge_def True areTypes
      by (simp add: fl local.arrow_def local.isType_def) 
    ultimately show ?thesis by (simp add: inducedBy_def True) 
  qed
  
  definition bracketOperator :: "'a \<Rightarrow> 'a \<Rightarrow> 'a" ("\<langle>_,_\<rangle>") where
    "\<langle>f,g\<rangle> \<equiv> (THE m. m inducedBy f g)"

  lemma inducedByProperty: "\<langle>f,g\<rangle> inducedBy f g"
    using theI'[of "\<lambda>m. m inducedBy f g"] bracketOperator_def uniqueInducedBy by simp

  lemma inducedByMap: "dom f \<simeq> dom g \<Longrightarrow> \<langle>f,g\<rangle>:(dom f)\<rightarrow>(cod f)\<^bold>\<times>(cod g)" 
    by (smt inducedByProperty inducedBy_def local.arrow_def local.associativity 
        local.compositionExistence local.isType_def local.leftCompIdentity local.rightCompIdentity 
        projection1Map fl)

  lemma inducedByAndMonics: 
    assumes "isMonic f" and "isMonic g" and "dom f \<simeq> dom g"
      shows "isMonic (\<langle>f, g\<rangle>)"
    unfolding isMonic_def using assms unfolding isMonic_def using inducedByProperty[of f g] 
    unfolding inducedBy_def by (metis fl inducedByMap local.arrow_def local.associativity 
                                local.codImpliesExistence local.compositionExistence)

subsection \<open>Products of arrows\<close>

  definition productDoubleWedgeCommutes 
    ("productDoubleWedgeCommutes \<box> \<leftarrow>\<pi>\<^sub>1\<midarrow>_\<midarrow>\<pi>\<^sub>2\<rightarrow> \<box>   \<down>_\<down>_\<down>_  \<box> \<leftarrow>\<pi>\<^sub>1\<midarrow>_\<midarrow>\<pi>\<^sub>2\<rightarrow> \<box>") where
     "productDoubleWedgeCommutes \<box> \<leftarrow>\<pi>\<^sub>1\<midarrow>  P  \<midarrow>\<pi>\<^sub>2\<rightarrow> \<box>
                                 \<down>h        \<down>x       \<down>k
                                 \<box> \<leftarrow>\<pi>\<^sub>1\<midarrow>  P' \<midarrow>\<pi>\<^sub>2\<rightarrow> \<box>
                              \<equiv> (h \<cdot> (\<pi>\<^sub>1 P) \<simeq> (\<pi>\<^sub>1 P') \<cdot> x \<and> k \<cdot> (\<pi>\<^sub>2 P) \<simeq> (\<pi>\<^sub>2 P') \<cdot> x)"
    
  definition arrowTimes :: "'a \<Rightarrow> 'a \<Rightarrow> 'a" (infix "\<^bold>\<times>\<^sub>a" 120) where
    "f \<^bold>\<times>\<^sub>a g \<equiv> \<langle>f \<cdot> \<pi>\<^sub>1 ((dom f) \<^bold>\<times> (dom g)), g \<cdot> \<pi>\<^sub>2 ((dom f) \<^bold>\<times> (dom g))\<rangle>"

  declare productDoubleWedgeCommutes_def[defs]

  lemma productOfArrows: 
    assumes "E f" and "E g"
    shows "(f \<^bold>\<times>\<^sub>a g):(dom f)\<^bold>\<times>(dom g)\<rightarrow>(cod f)\<^bold>\<times>(cod g)" and
          "productDoubleWedgeCommutes \<box> \<leftarrow>\<pi>\<^sub>1\<midarrow> ((dom f) \<^bold>\<times> (dom g)) \<midarrow>\<pi>\<^sub>2\<rightarrow> \<box>
                                      \<down>f                \<down>(f \<^bold>\<times>\<^sub>a g)        \<down>g
                                      \<box> \<leftarrow>\<pi>\<^sub>1\<midarrow> ((cod f) \<^bold>\<times> (cod g)) \<midarrow>\<pi>\<^sub>2\<rightarrow> \<box>"
  proof -
    have types: "isType (dom f) \<and> isType (dom g) \<and> isType (cod f) \<and> isType (cod g)"
      unfolding isType_def by (metis fl assms local.compositionExistence local.leftCompIdentity 
      local.rightCompIdentity)

    have 1: "dom (f \<cdot> \<pi>\<^sub>1 ((dom f) \<^bold>\<times> (dom g))) \<simeq> (dom f) \<^bold>\<times> (dom g)"
      using assms types projection1Map
      by (smt fl local.arrow_def local.associativity local.codImpliesExistence 
          local.compositionExistence local.rightCompIdentity)
    have 2: "dom (g \<cdot> \<pi>\<^sub>2 ((dom f) \<^bold>\<times> (dom g))) \<simeq> (dom f) \<^bold>\<times> (dom g)"
      using assms types projection2Map
      by (smt fl local.arrow_def local.associativity local.codImpliesExistence 
          local.compositionExistence local.rightCompIdentity)
    have 3: "cod (f \<cdot> \<pi>\<^sub>1 ((dom f) \<^bold>\<times> (dom g))) = cod f" 
      by (metis fl \<open>dom f \<cdot> \<pi>\<^sub>1 ((dom f) \<^bold>\<times> (dom g)) \<simeq> (dom f) \<^bold>\<times> (dom g)\<close> local.associativity 
          local.compositionExistence local.domImpliesExistence local.leftCompIdentity)
    have 4: "cod (g \<cdot> \<pi>\<^sub>2 ((dom f) \<^bold>\<times> (dom g))) = cod g" 
      by (metis fl \<open>dom g \<cdot> \<pi>\<^sub>2 ((dom f) \<^bold>\<times> (dom g)) \<simeq> (dom f) \<^bold>\<times> (dom g)\<close> local.associativity 
          local.compositionExistence local.domImpliesExistence local.leftCompIdentity)
  
    have map: "(f \<^bold>\<times>\<^sub>a g):(dom f)\<^bold>\<times>(dom g)\<rightarrow>(cod f)\<^bold>\<times>(cod g)"
      by (smt fl "1" "2" "3" "4" arrowTimes_def inducedByProperty inducedBy_def local.arrow_def 
          local.associativity local.compositionExistence local.productOfTypes 
          local.rightCompIdentity local.uniqueArrowIntoProduct projection2Map types)

    show "(f \<^bold>\<times>\<^sub>a g):(dom f)\<^bold>\<times>(dom g)\<rightarrow>(cod f)\<^bold>\<times>(cod g)" using map by auto

    show  "productDoubleWedgeCommutes \<box> \<leftarrow>\<pi>\<^sub>1\<midarrow> ((dom f) \<^bold>\<times> (dom g)) \<midarrow>\<pi>\<^sub>2\<rightarrow> \<box>
                                      \<down>f                \<down>(f \<^bold>\<times>\<^sub>a g)        \<down>g
                                      \<box> \<leftarrow>\<pi>\<^sub>1\<midarrow> ((cod f) \<^bold>\<times> (cod g)) \<midarrow>\<pi>\<^sub>2\<rightarrow> \<box>" using map
    unfolding productDoubleWedgeCommutes_def arrowTimes_def  
    using inducedByProperty[of "f \<cdot> \<pi>\<^sub>1 ((dom f) \<^bold>\<times> (dom g))" "g \<cdot> \<pi>\<^sub>2 ((dom f) \<^bold>\<times> (dom g))"] 
    unfolding inducedBy_def using 1 2 3 4 fl by simp
  qed
  
  lemma arrowProductExistence1: "E (f \<^bold>\<times>\<^sub>a g) \<longrightarrow> E f \<and> E g" 
    by (metis arrowTimes_def inducedByProperty inducedBy_def local.compositionExistence 
        local.inexistentElement ExId_def)

  lemma arrowProductExistence2: "E f \<and> E g \<longrightarrow> E (f \<^bold>\<times>\<^sub>a g)"
    using local.codImpliesExistence local.compositionExistence productDoubleWedgeCommutes_def 
    productOfArrows fl by meson
  
  lemma arrowProductExistence: "E (f \<^bold>\<times>\<^sub>a g) \<longleftrightarrow> E f \<and> E g" 
    using arrowProductExistence1 arrowProductExistence2 fl by metis

  lemma compositionAndArrowProduct:
    assumes "cod f \<simeq> dom h" and "cod g \<simeq> dom i"
    shows "(h \<^bold>\<times>\<^sub>a i) \<cdot> (f \<^bold>\<times>\<^sub>a g) = (h \<cdot> f) \<^bold>\<times>\<^sub>a (i \<cdot> g)"
  proof - 
    define A where "A \<equiv> (dom f) \<^bold>\<times> (dom g)"
    define C where "C \<equiv> (cod h) \<^bold>\<times> (cod i)"

    have ex: "E f \<and> E g \<and> E h \<and> E i" 
      using assms(1) assms(2) local.codImpliesExistence local.domImpliesExistence fl by blast

    have comp1: "(h \<cdot> f) \<cdot> \<pi>\<^sub>1 A \<simeq> \<pi>\<^sub>1 C \<cdot> ((h \<cdot> f) \<^bold>\<times>\<^sub>a (i \<cdot> g))"
      using assms(1) unfolding A_def C_def using productOfArrows ex 
      unfolding productDoubleWedgeCommutes_def 
      by (smt assms(2) local.associativity local.codImpliesExistence local.compositionExistence 
          local.leftCompIdentity fl)

    hence 1: "\<pi>\<^sub>1 C \<cdot> ((h \<^bold>\<times>\<^sub>a i) \<cdot> (f \<^bold>\<times>\<^sub>a g)) = \<pi>\<^sub>1 C \<cdot> ((h \<cdot> f) \<^bold>\<times>\<^sub>a (i \<cdot> g))"
      unfolding C_def using productOfArrows unfolding productDoubleWedgeCommutes_def
      by (smt A_def assms ex local.associativity fl)
      
    have comp2: "(i \<cdot> g) \<cdot> \<pi>\<^sub>2 A \<simeq> \<pi>\<^sub>2 C \<cdot> ((h \<cdot> f) \<^bold>\<times>\<^sub>a (i \<cdot> g))"
      unfolding A_def C_def using productOfArrows ex unfolding productDoubleWedgeCommutes_def 
      by (smt assms local.associativity local.codImpliesExistence local.compositionExistence 
          local.leftCompIdentity fl)

    hence 2: "\<pi>\<^sub>2 C \<cdot> ((h \<^bold>\<times>\<^sub>a i) \<cdot> (f \<^bold>\<times>\<^sub>a g)) = \<pi>\<^sub>2 C \<cdot> ((h \<cdot> f) \<^bold>\<times>\<^sub>a (i \<cdot> g))"
      unfolding C_def using productOfArrows unfolding productDoubleWedgeCommutes_def
      by (metis fl A_def assms ex local.associativity)

    show ?thesis using 1 2 comp1 comp2 uniqueArrowIntoProduct
      by (smt local.compositionExistence local.domImpliesExistence local.productExistence 
          local.productOfTypes local.projection1Existence fl)
  qed

  lemma domAndArrowProduct:
    assumes "dom (f \<^bold>\<times>\<^sub>a g) \<simeq> A \<^bold>\<times> B" 
    shows "dom f = A" and "dom g = B"
    subgoal by (metis arrowProductExistence assms local.arrow_def local.domImpliesExistence 
                local.productExistence productOfArrows(1) projection1Map ExId_def)
    subgoal by (metis arrowProductExistence assms local.arrow_def local.domImpliesExistence 
                local.productExistence productOfArrows(1) projection2Map ExId_def)
    done

  lemma codAndArrowProduct:
    assumes "cod (f \<^bold>\<times>\<^sub>a g) \<simeq> A \<^bold>\<times> B" 
    shows "cod f = A" and "cod g = B"
    by (metis arrowProductExistence assms local.arrow_def local.codImpliesExistence 
        productOfArrows(1) productsEqualFactorsEqual ExId_def)+

end

end