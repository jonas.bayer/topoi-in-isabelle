theory Functors imports Category FreeLogic

begin

record 'a abstract_category =
  existence :: "'a \<Rightarrow> bool"
  domain :: "'a \<Rightarrow> 'a"
  codomain :: "'a \<Rightarrow> 'a"
  composition :: "'a \<Rightarrow> 'a \<Rightarrow> 'a"
  inexistent_element :: "'a"

definition is_category :: "'a abstract_category \<Rightarrow> bool" where
  "is_category C \<equiv> category (existence C) 
                            (domain C) (codomain C) 
                            (composition C) (inexistent_element C)"

datatype ('a, 'b) abstract_functor = AbstractFunctor (map: "'a \<Rightarrow> 'b")
                                                     (origin: "'a abstract_category")
                                                     (destination: "'b abstract_category")

definition is_functor :: "('a, 'b) abstract_functor \<Rightarrow> bool" where
  "is_functor Functor \<equiv> let F = map Functor; 
                      E\<^sub>O = existence (origin Functor);
                      dom\<^sub>O = domain (origin Functor); cod\<^sub>O = codomain (origin Functor); 
                      comp\<^sub>O = composition (origin Functor); 
                      E\<^sub>D = existence (destination Functor);
                      dom\<^sub>D = domain (destination Functor); cod\<^sub>D = codomain (destination Functor); 
                      comp\<^sub>D = composition (destination Functor) 
                in (\<forall>x. E\<^sub>O x \<longleftrightarrow> E\<^sub>D (F x))
                 \<and> (\<forall>x. F (dom\<^sub>O x) = dom\<^sub>D (F x))
                 \<and> (\<forall>x. F (cod\<^sub>O x) = cod\<^sub>D (F x))
                 \<and> (\<forall>x y. F (comp\<^sub>O x y) = comp\<^sub>D (F x) (F y))" 


end