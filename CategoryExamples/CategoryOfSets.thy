theory CategoryOfSets imports "../Category"

begin

section \<open>A type set map\<close>
text \<open>A type that can be instantiated as a category\<close>
datatype 'a setmap = Setmap (smap: "'a \<Rightarrow> 'a") 
                            (sdom: "'a set") (* check what happens in the case of 'a \<Rightarrow> bool *)
                            (scod: "'a set")

definition of :: "'a setmap \<Rightarrow> 'a \<Rightarrow> 'a" ("_ of _" [120, 120] 120) where
  "f of a \<equiv> (smap f) a"

subsection \<open>Existence of set maps\<close>

definition setmap_existence :: "'a setmap \<Rightarrow> bool" ("E") where
  "E (f::'a setmap) \<equiv> (\<forall>x. x\<in>sdom f \<longrightarrow> f of x \<in> scod f)"

interpretation free_logic setmap_existence by (unfold_locales)

notation KlEq (infixr "\<cong>" 56)
notation ExId (infixr "\<simeq>" 56) 


text \<open>Sanity check: There are also inexistent elements\<close>
lemma "\<exists>(x::'a setmap). \<not> E x"
  using setmap_existence_def UNIV_I equals0D setmap.sel(2-3) by metis

definition setmap_inexistent_element :: "'a setmap" ("*") where
  "setmap_inexistent_element = (SOME (x::'a setmap). \<not> E x)"

lemma inexistent_element: "\<not> (E * )"
  using setmap_inexistent_element_def setmap.sel(2-3) setmap_existence_def
  by (metis Collect_mem_eq empty_Collect_eq insertI1 verit_sko_forall) 

subsection \<open>Defining domain, codomain and composition\<close>
definition setmap_identity :: "'a set \<Rightarrow> 'a setmap" ("Id") where
  "Id A = Setmap (\<lambda>x. x) A A"

definition setmap_domain :: "'a setmap \<Rightarrow> 'a setmap" ("dom") where
  "dom f \<equiv> if E f 
            then Id (sdom f) 
            else *" 

definition setmap_codomain :: "'a setmap \<Rightarrow> 'a setmap" ("cod") where
  "cod f \<equiv> if E f 
            then Id (scod f) 
            else *" 

text \<open>Composable set maps and composition:\<close>
definition composable :: "'a setmap \<Rightarrow> 'a setmap \<Rightarrow> bool" where 
  "composable f g \<equiv> E f \<and> E g \<and> scod g = sdom f"

definition setmap_composition :: "'a setmap \<Rightarrow> 'a setmap \<Rightarrow> 'a setmap" (infix "\<cdot>" 110)
  where "f \<cdot> g \<equiv> if composable f g
                  then Setmap (smap f o smap g) (sdom g) (scod f)
                  else *"

text \<open>Some simple lemmas that will later be used in the interpretation proof.\<close>
lemma compdom: "composable f g \<Longrightarrow> sdom (f \<cdot> g) = sdom g"
  by (simp add: setmap_composition_def)

lemma compcod: "composable f g \<Longrightarrow> scod (f \<cdot> g) = scod f"
  by (simp add: setmap_composition_def)

lemma composableExistence:
  "E (f \<cdot> g) \<longleftrightarrow> composable f g"
  apply (cases "composable f g")
  using composable_def[of f g] inexistent_element setmap_composition_def[of f g] 
        by (auto simp add: setmap_existence_def of_def)

lemma domExistence: "E f \<longrightarrow> E (Id (sdom f))"
  by (simp add: of_def setmap_identity_def setmap_existence_def)

lemma codExistence: "E f \<longrightarrow> E (Id (scod f))"
  by (simp add: of_def setmap_identity_def setmap_existence_def)


section \<open>The interpretation proof\<close>
interpretation category setmap_existence
                        setmap_domain 
                        setmap_codomain 
                        setmap_composition 
                        setmap_inexistent_element
proof (unfold_locales) 
  show "(f \<cdot> g) \<cdot> h \<cong> f \<cdot> (g \<cdot> h)" for f g h :: "'a setmap"
    unfolding fl apply (auto)
    subgoal
    proof -

      assume "E ((f \<cdot> g) \<cdot> h)"

      hence comp1: "composable (f \<cdot> g) h" 
                                                  using composableExistence by auto
      hence comp2: "composable f g" 
                                                  using composable_def[of "f \<cdot> g" "h"] 
                                                        composableExistence by auto
      hence existence: "E f \<and> E g \<and> E h" 
                                                  using comp1 comp2 composable_def by auto
      have "scod h = sdom g" 
                                                  using comp1 composable_def[of "f \<cdot> g"] compdom 
                                                        comp2 by smt
      hence "composable g h" 
                                                  using existence composable_def by auto
      hence "scod (g \<cdot> h) = sdom f" 
                                                  using compcod[of g h] comp2 composable_def by auto
      hence "composable f (g \<cdot> h)" 
                                                  using composable_def[of f "g \<cdot> h"] existence 
                                                        \<open>composable g h\<close> composableExistence 
                                                        by blast
      show ?thesis 
                                                  by (smt \<open>composable f (g \<cdot> h)\<close> \<open>composable g h\<close> 
                                                      comp1 comp2 comp_assoc compcod compdom 
                                                      setmap.sel(1) setmap_composition_def)
    qed
    subgoal 
      by (metis (mono_tags, hide_lams) \<open>E ((f \<cdot> g) \<cdot> h) \<Longrightarrow> (f \<cdot> g) \<cdot> h = f \<cdot> (g \<cdot> h)\<close> compcod 
          compdom composableExistence composable_def)
    done

  show "x \<cdot> dom x \<cong> x" for x :: "'a setmap"
    using setmap_domain_def setmap_composition_def setmap.sel
    by (smt inexistent_element comp_id composable_def setmap_composition_def
        setmap_domain_def fun.map_ident of_def setmap_identity_def setmap.exhaust 
        setmap_existence_def fl)
  
  show "cod x \<cdot> x \<cong> x" for x :: "'a setmap"
    using setmap_codomain_def setmap_composition_def setmap.sel
    by (smt inexistent_element comp_id composable_def  
        setmap_codomain_def fun.map_ident of_def setmap_identity_def setmap.exhaust 
        setmap_composition_def setmap_existence_def fl)

  show "E (x \<cdot> y) \<longleftrightarrow> dom x \<simeq> cod y" for x y :: "'a setmap"
    unfolding fl
    apply (simp add: setmap_composition_def composableExistence composable_def)
    apply (simp add: setmap_domain_def setmap_codomain_def inexistent_element)
    apply (simp add: setmap_identity_def) 
    by (smt comp_apply of_def setmap.sel(1) setmap.sel(2) setmap.sel(3) setmap_existence_def)

  show "E (dom x) \<longrightarrow> E x" for x :: "'a setmap" 
    by (simp add: setmap_domain_def setmap_identity_def inexistent_element)

  show "E (cod x) \<longrightarrow> E x" for x :: "'a setmap" 
    by (simp add: setmap_codomain_def setmap_identity_def inexistent_element)

  show "\<not> E *" using inexistent_element by auto
qed



end