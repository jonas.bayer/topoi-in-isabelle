theory CategoryOfCategories imports "../Category"

begin

section \<open>Preliminary thoughts and definitions\<close>

text \<open>It is not possible to work with the category of categories as generally as with the naive 
      approach on paper. However, it is possible to consider a restricted case that also works
      formally. The precise power of this approach depends on the used type system, i.e. the "more"
      types are allowed the larger the category of categories presented here can get. 

      More concretely, it is only possible to consider the category of categories over the same
      type. The maps between categories are functors, so this is what we start with. Similarly to
      the category of sets, each functor also needs to be equipped with the structur of the 
      category it goes from and the category it goes to. We call these the origin and destination
      category here in order to not create confusion about the multiple use of the terms domain and
      codomain. \<close>

datatype 'a abstract_functor = AbstractFunctor 
                               (map: "'a \<Rightarrow> 'a")
 
                               (origin_existence: "'a \<Rightarrow> bool") 
                               (origin_domain: "'a \<Rightarrow> 'a") (origin_codomain: "'a \<Rightarrow> 'a")
                               (origin_composition: "'a \<Rightarrow> 'a \<Rightarrow> 'a") 
                               (origin_inexistent_element: "'a")

                               (destination_existence: "'a \<Rightarrow> bool")
                               (destination_domain: "'a \<Rightarrow> 'a") (destination_codomain: "'a \<Rightarrow> 'a")
                               (destination_composition: "'a \<Rightarrow> 'a \<Rightarrow> 'a") 
                               (destination_inexistent_element: "'a")

text \<open>We now define when this abstract structure indeed has the property of a functor. Subscript O
      is used for the structures of the origin category and subscript D for the destination
      category. 

      Note that the functor axioms used here are not the traditional ones but tailored
      to the axiomatic approach not thinking about objects. It is easy to prove (on paper) that 
      these axioms indeed imply the traditional functor axioms.\<close>

definition is_functor :: "'a abstract_functor \<Rightarrow> bool" where
  "is_functor F \<equiv> let f = map F; 
                      E\<^sub>O = origin_existence F;
                      dom\<^sub>O = origin_domain F; cod\<^sub>O = origin_codomain F; 
                      comp\<^sub>O = origin_composition F; 
                      E\<^sub>D = destination_existence F; 
                      dom\<^sub>D = destination_domain F; cod\<^sub>D = destination_codomain F; 
                      comp\<^sub>D = destination_composition F
                in (\<forall>x. E\<^sub>O x \<longleftrightarrow> E\<^sub>D (f x))
                 \<and> (\<forall>x. f (dom\<^sub>O x) = dom\<^sub>D (f x))
                 \<and> (\<forall>x. f (cod\<^sub>O x) = cod\<^sub>D (f x))
                 \<and> (\<forall>x y. f (comp\<^sub>O x y) = comp\<^sub>D (f x) (f y))" 

section \<open>Defining existence\<close>
text \<open>With this it is possible to define the existence of functors in the category of categories.\<close>

definition functor_existence :: "'a abstract_functor \<Rightarrow> bool" ("E\<^sub>F") where
  "E\<^sub>F F \<equiv> category (origin_existence F) 
                  (origin_domain F) (origin_codomain F) 
                  (origin_composition F) (origin_inexistent_element F)
       \<and> category (destination_existence F) 
                  (destination_domain F) (destination_codomain F) 
                  (destination_composition F) (destination_inexistent_element F)
       \<and> is_functor F"

text \<open>Note that restricting to categories over the type 'a does not mean restricting to
      endofunctors. The functors we allow to exist here go from one category to a different 
      category, it is only required, that both category structures are built on the same type.\<close>

text \<open>A sanity check to show that not all abstract functors exist:\<close>
lemma exists_inexistent: "\<exists>(F::'a abstract_functor). \<not> E\<^sub>F F"
  by (metis (no_types, hide_lams) abstract_functor.sel(11) abstract_functor.sel(7) 
      category.inexistentElement functor_existence_def)

definition functor_inexistent_element :: "'a abstract_functor" ("*") where
  "* \<equiv> (SOME F. \<not> E\<^sub>F F)"

lemma inexistent_element: "\<not> (E\<^sub>F *)" 
  unfolding functor_inexistent_element_def using exists_inexistent someI_ex by metis 

section \<open>Defining domain, codomain and composition\<close>

text \<open>The domain of a functor is, of course, the identity functor on its origin when considering
      objects as identity arrows. This motivates the following definition, which then allows to 
      easily define domain and codomain in the category of categories: \<close>

definition identity_functor :: "('a \<Rightarrow> bool) \<Rightarrow> ('a \<Rightarrow> 'a) \<Rightarrow> ('a \<Rightarrow> 'a) \<Rightarrow> ('a \<Rightarrow> 'a \<Rightarrow> 'a) \<Rightarrow> 'a 
                                \<Rightarrow> 'a abstract_functor" where
  "identity_functor E domain codomain composition inex = AbstractFunctor id
                                                         E domain codomain composition inex
                                                         E domain codomain composition inex"

definition functor_domain :: "'a abstract_functor \<Rightarrow> 'a abstract_functor" ("dom\<^sub>F") where
  "functor_domain F \<equiv> if E\<^sub>F F 
                      then identity_functor (origin_existence F)
                                            (origin_domain F) (origin_codomain F) 
                                            (origin_composition F) (origin_inexistent_element F)
                      else *"

definition functor_codomain :: "'a abstract_functor \<Rightarrow> 'a abstract_functor" ("cod\<^sub>F") where
  "functor_codomain F \<equiv> if E\<^sub>F F 
                      then identity_functor (destination_existence F)
                                            (destination_domain F) (destination_codomain F) 
                                            (destination_composition F) 
                                            (destination_inexistent_element F)
                      else *"

section \<open>Defining composition\<close>

text \<open>We first define what it means for two functors to be composable and from this build up the 
      composition operation. \<close>

definition composable :: "'a abstract_functor \<Rightarrow> 'a abstract_functor \<Rightarrow> bool" where
  "composable F G \<equiv> E\<^sub>F F \<and> E\<^sub>F G \<and> (destination_existence G = origin_existence F)
                                \<and> (destination_domain G = origin_domain F)
                                \<and> (destination_codomain G = origin_codomain F)
                                \<and> (destination_composition G = origin_composition F)
                                \<and> (destination_inexistent_element G = origin_inexistent_element F)"

definition composite :: "'a abstract_functor \<Rightarrow> 'a abstract_functor
                         \<Rightarrow> 'a abstract_functor" where
  "composite F G \<equiv> AbstractFunctor (map F \<circ> map G)
                                    (origin_existence G) 
                                    (origin_domain G) (origin_codomain G) 
                                    (origin_composition G) (origin_inexistent_element G)
                                    (destination_existence F) 
                                    (destination_domain F) (destination_codomain F) 
                                    (destination_composition F) (destination_inexistent_element F)"

definition functor_composition :: "'a abstract_functor \<Rightarrow> 'a abstract_functor 
                                   \<Rightarrow> 'a abstract_functor" (infix "\<cdot>" 110) where
  "F \<cdot> G \<equiv> if composable F G 
           then composite F G 
           else *"

text \<open>These two lemmas will be helpful in the instantiation proof.\<close>
lemma composite_existence: "composable F G \<Longrightarrow> E\<^sub>F (composite F G)"
  unfolding composable_def composite_def functor_existence_def is_functor_def Let_def 
            functor_domain_def functor_codomain_def identity_functor_def by auto

lemma composable: "E\<^sub>F (F \<cdot> G) \<longleftrightarrow> composable F G"
  apply(cases "composable F G", simp) 
    subgoal using composite_existence  unfolding functor_composition_def by auto 
    subgoal unfolding functor_composition_def using inexistent_element by auto
  done

section \<open>Instantiating the category of categories\<close>

interpretation free_logic functor_existence by (unfold_locales)

notation KlEq (infixr "\<cong>" 56)
notation ExId (infixr "\<simeq>" 56) 

text \<open>All proofs annotated with "by sledgehammer" were entirely found through sledgehammer. The 
      only part that required some more manual work was proving associativity. \<close>

interpretation category functor_existence
                        functor_domain
                        functor_codomain
                        functor_composition
                        functor_inexistent_element
proof (unfold_locales)
 show "E\<^sub>F (dom\<^sub>F x) \<longrightarrow> E\<^sub>F x" for x :: "'a abstract_functor" 
   by (simp add: functor_domain_def inexistent_element) (* by sledgehammer *)

 show "E\<^sub>F (cod\<^sub>F x) \<longrightarrow> E\<^sub>F x" for x :: "'a abstract_functor" 
   by (simp add: functor_codomain_def inexistent_element) (* by sledgehammer *)

 show "\<not> E\<^sub>F *" using inexistent_element by auto

 show "E\<^sub>F (x \<cdot> y) = dom\<^sub>F x \<simeq> cod\<^sub>F y" for x y :: "'a abstract_functor" 
  unfolding composable fl apply auto (* at this point, sledgehammer could prove all the subgoals *)
    subgoal by (simp add: composable_def functor_domain_def functor_existence_def 
                identity_functor_def is_functor_def)
    subgoal by (smt \<open>composable x y \<Longrightarrow> E\<^sub>F (dom\<^sub>F x)\<close> composable_def functor_codomain_def 
                functor_domain_def)
    subgoal by (simp add: composable_def functor_codomain_def functor_domain_def)
    subgoal by (smt \<open>\<And>x. E\<^sub>F (dom\<^sub>F x) \<longrightarrow> E\<^sub>F x\<close> abstract_functor.inject composable_def 
                functor_codomain_def functor_domain_def identity_functor_def)
    done

 show "x \<cdot> dom\<^sub>F x \<cong> x" for x :: "'a abstract_functor" 
  by (smt ExId_def KlEq_def \<open>\<And>x. E\<^sub>F (dom\<^sub>F x) \<longrightarrow> E\<^sub>F x\<close> \<open>\<And>y x. E\<^sub>F (x \<cdot> y) = dom\<^sub>F x \<simeq> cod\<^sub>F y\<close> 
      abstract_functor.collapse abstract_functor.sel comp_id composable 
      composite_def functor_codomain_def functor_composition_def functor_domain_def 
      functor_existence_def id_def identity_functor_def is_functor_def) (* by sledgehammer *)

 show "cod\<^sub>F x \<cdot> x \<cong> x" for x :: "'a abstract_functor" 
  by (smt ExId_def KlEq_def \<open>\<And>x. E\<^sub>F (cod\<^sub>F x) \<longrightarrow> E\<^sub>F x\<close> \<open>\<And>y x. E\<^sub>F (x \<cdot> y) = dom\<^sub>F x \<simeq> cod\<^sub>F y\<close> 
      abstract_functor.collapse abstract_functor.sel(1-11) composable composite_def fun.map_id 
      functor_codomain_def functor_composition_def functor_domain_def functor_existence_def id_def 
      identity_functor_def is_functor_def) (* by sledgehammer *)

 show "(x \<cdot> y) \<cdot> z \<cong> x \<cdot> (y \<cdot> z)" for x y z :: "'a abstract_functor" 
  unfolding fl apply (auto)
    subgoal 
      proof - 
        assume "E\<^sub>F ((x \<cdot> y) \<cdot> z)"

        hence comp1: "composable (x \<cdot> y) z" 
          by (simp add: composable)

        hence comp2: "composable x y"
          by (simp add: composable composable_def)

        hence "E\<^sub>F x" and "E\<^sub>F y" and "E\<^sub>F z" 
          using comp2 composable_def apply auto[2]
          using comp1 composable_def by blast

        have "composable y z" 
          by (metis abstract_functor.collapse abstract_functor.inject comp1 comp2 composable_def 
              composite_def functor_composition_def)

        have "composable x (y \<cdot> z)" 
          by (metis \<open>composable y z\<close> abstract_functor.collapse abstract_functor.inject comp2 
              composable composable_def composite_def functor_composition_def)

        show "(x \<cdot> y) \<cdot> z = x \<cdot> (y \<cdot> z)" 
          by (metis (no_types, lifting) \<open>composable x (y \<cdot> z)\<close> \<open>composable y z\<close> abstract_functor.sel
               comp1 comp2 comp_assoc composite_def functor_composition_def) (* by sledgehammer *)
      qed
    subgoal
      by (metis \<open>E\<^sub>F ((x \<cdot> y) \<cdot> z) \<Longrightarrow> (x \<cdot> y) \<cdot> z = x \<cdot> (y \<cdot> z)\<close> abstract_functor.collapse 
          abstract_functor.inject composable composable_def composite_def functor_composition_def)
      (* by sledgehammer *)
    done

qed

end