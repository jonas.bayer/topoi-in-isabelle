theory BinaryCategoryOfSets imports "../BinaryProductCategory"

begin

section \<open>A type set map\<close>
text \<open>A type that can be instantiated as a category\<close>
datatype 'a tree = Root (val: 'a)
                 | Node (left:  "'a tree")
                        (right: "'a tree")         


datatype 'a setmap = Setmap (smap: "'a tree \<Rightarrow> 'a tree") 
                            (sdom: "'a set tree") 
                            (scod: "'a set tree")

definition of :: "'a setmap \<Rightarrow> 'a tree \<Rightarrow> 'a tree" ("_ of _" [120, 120] 120) where
  "f of a \<equiv> (smap f) a"

fun in_tree :: "'a tree \<Rightarrow> 'a set tree \<Rightarrow> bool" (infix "\<in>\<^sub>T" 50) where
  "(Root a \<in>\<^sub>T Root A) = (a \<in> A)" |
  "(Node l r \<in>\<^sub>T Node L R) = (l \<in>\<^sub>T L \<and> r \<in>\<^sub>T R)" | 
  "_ \<in>\<^sub>T _ = False"


subsection \<open>Existence of set maps\<close>


definition setmap_existence :: "'a setmap \<Rightarrow> bool" ("E") where
  "E (f::'a setmap) \<equiv> (\<forall>x. (x \<in>\<^sub>T sdom f \<longrightarrow> (f of x) \<in>\<^sub>T scod f)
                       \<and> (\<not> x \<in>\<^sub>T sdom f \<longrightarrow> (f of x) = x))"

interpretation free_logic setmap_existence by (unfold_locales)

notation KlEq (infixr "\<cong>" 56)
notation ExId (infixr "\<simeq>" 56) 

text \<open>Sanity check: There are also inexistent elements\<close>
lemma existence_of_inexistent_element: "\<exists>(x::'a setmap). \<not> E x" 
  unfolding setmap_existence_def of_def 
  by (metis in_tree.simps(4) setmap.sel(1) setmap.sel(2) tree.distinct(1)) 

definition setmap_inexistent_element :: "'a setmap" ("*") where
  "* = (SOME (x::'a setmap). \<not> E x)"

lemma inexistent_element: "\<not> (E * )"
  using existence_of_inexistent_element setmap_inexistent_element_def 
  by (metis (full_types) someI_ex)

subsection \<open>Defining domain, codomain and composition\<close>
definition setmap_identity :: "'a set tree \<Rightarrow> 'a setmap" ("Id") where
  "Id A = Setmap (\<lambda>x. x) A A"

definition setmap_domain :: "'a setmap \<Rightarrow> 'a setmap" ("dom") where
  "dom f \<equiv> if E f 
            then Id (sdom f) 
            else *" 

definition setmap_codomain :: "'a setmap \<Rightarrow> 'a setmap" ("cod") where
  "cod f \<equiv> if E f 
            then Id (scod f) 
            else *" 

text \<open>Composable set maps and composition:\<close>
definition composable :: "'a setmap \<Rightarrow> 'a setmap \<Rightarrow> bool" where 
  "composable f g \<equiv> E f \<and> E g \<and> scod g = sdom f"

definition setmap_composition :: "'a setmap \<Rightarrow> 'a setmap \<Rightarrow> 'a setmap" (infix "\<cdot>" 110)
  where "f \<cdot> g \<equiv> if composable f g
                  then Setmap (\<lambda>x. if x \<in>\<^sub>T sdom g then (smap f  (smap g x)) else x) 
                              (sdom g) (scod f)
                  else *"

text \<open>Some simple lemmas that will later be used in the interpretation proof.\<close>
lemma compdom: "composable f g \<Longrightarrow> sdom (f \<cdot> g) = sdom g"
  by (simp add: setmap_composition_def)

lemma compcod: "composable f g \<Longrightarrow> scod (f \<cdot> g) = scod f"
  by (simp add: setmap_composition_def)

lemma composableExistence:
  "E (f \<cdot> g) \<longleftrightarrow> composable f g" 
  apply (cases "composable f g")
  using composable_def[of f g] inexistent_element setmap_composition_def[of f g] 
        using setmap_existence_def of_def apply auto 
  unfolding setmap_existence_def apply rule
    subgoal for x unfolding of_def by simp
    done (* shorten proof *)

lemma dom_existence: "E f \<longrightarrow> E (Id (sdom f))"
  by (simp add: of_def setmap_identity_def setmap_existence_def)

lemma cod_existence: "E f \<longrightarrow> E (Id (scod f))"
  by (simp add: of_def setmap_identity_def setmap_existence_def)

lemma smap_existence: "E f \<longrightarrow> smap f = (\<lambda>x. if x \<in>\<^sub>T sdom f then smap f x else x)"
  by (metis of_def setmap_existence_def)

section \<open>The interpretation proof\<close>
interpretation set_cat: category setmap_existence
                                 setmap_domain 
                                 setmap_codomain 
                                 setmap_composition 
                                 setmap_inexistent_element 
proof (unfold_locales) 
  show "(f \<cdot> g) \<cdot> h \<cong> f \<cdot> (g \<cdot> h)" for f g h :: "'a setmap"
    unfolding fl apply (auto)
    subgoal
    proof -

      assume "E ((f \<cdot> g) \<cdot> h)"

      hence comp1: "composable (f \<cdot> g) h" 
                                                  using composableExistence by auto
      hence comp2: "composable f g" 
                                                  using composable_def[of "f \<cdot> g" "h"] 
                                                        composableExistence by auto
      hence existence: "E f \<and> E g \<and> E h" 
                                                  using comp1 comp2 composable_def by auto
      have "scod h = sdom g" 
                                                  using comp1 composable_def[of "f \<cdot> g"] compdom 
                                                        comp2 by smt
      hence "composable g h" 
                                                  using existence composable_def by auto
      hence "scod (g \<cdot> h) = sdom f" 
                                                  using compcod[of g h] comp2 composable_def by auto
      hence "composable f (g \<cdot> h)" 
                                                  using composable_def[of f "g \<cdot> h"] existence 
                                                        \<open>composable g h\<close> composableExistence 
                                                        by blast
      show ?thesis 
                                                  unfolding setmap_composition_def 
                                                  using \<open>composable f g\<close> \<open>composable g h\<close> 
                                                  \<open>composable f (g \<cdot> h)\<close> \<open>composable (f \<cdot> g) h\<close>
                                                  unfolding composable_def apply auto 
                                                  apply(rule HOL.ext) subgoal for x 
                                                  apply (cases "x \<in>\<^sub>T sdom h", auto) 
                                                  by (simp add: of_def setmap_existence_def)
                                                  apply (simp add: \<open>composable g h\<close> 
                                                         setmap_composition_def)
                                                  by (simp add: comp2 setmap_composition_def)
    qed
    subgoal 
      by (metis (mono_tags, hide_lams) \<open>E ((f \<cdot> g) \<cdot> h) \<Longrightarrow> (f \<cdot> g) \<cdot> h = f \<cdot> (g \<cdot> h)\<close> compcod 
          compdom composableExistence composable_def)
    done

  show "x \<cdot> dom x \<cong> x" for x :: "'a setmap" 
    proof (cases "E x")
      case True
      hence "composable x (dom x)" unfolding composable_def 
        by (metis dom_existence setmap.sel(3) setmap_domain_def setmap_identity_def)

      have "Setmap (\<lambda>xa. if xa \<in>\<^sub>T sdom x 
                         then smap x (smap (Setmap (\<lambda>x. x) (sdom x) (sdom x)) xa) 
                         else xa) 
                   (sdom x) (scod x) = x" 
        apply(rule setmap.expand, auto, rule HOL.ext) 
        subgoal for y
          using True apply(cases "y \<in>\<^sub>T sdom x") apply (auto) 
          by (simp add: of_def setmap_existence_def) 
        done
        
      thus ?thesis using \<open>composable x (dom x)\<close> True 
        unfolding setmap_composition_def setmap_domain_def setmap_identity_def fl by auto
    next
      case False
      then show ?thesis by (simp add: composableExistence composable_def fl)
    qed
  
  show "cod x \<cdot> x \<cong> x" for x :: "'a setmap"
    proof (cases "E x")
      case True
      hence "composable (cod x) x" unfolding composable_def 
        by (metis cod_existence setmap.sel(2) setmap_codomain_def setmap_identity_def)

      have "Setmap (\<lambda>xa. if xa \<in>\<^sub>T sdom x 
                         then smap (Setmap (\<lambda>x. x) (scod x) (scod x)) (smap x xa) 
                         else xa) 
                   (sdom x) (scod x) = x"
       apply(rule setmap.expand, auto, rule HOL.ext) 
       subgoal for y
         using True apply(cases "y \<in>\<^sub>T sdom x", auto) by (simp add: smap_existence)
       done

      thus ?thesis using \<open>composable (cod x) x\<close> True 
        unfolding setmap_composition_def setmap_codomain_def setmap_identity_def fl by auto
    next
      case False
      then show ?thesis by (simp add: composableExistence composable_def fl)
    qed

  show "E (x \<cdot> y) \<longleftrightarrow> dom x \<simeq> cod y" for x y :: "'a setmap" unfolding fl
    apply (simp add: setmap_composition_def composableExistence composable_def)
    apply (simp add: setmap_domain_def setmap_codomain_def inexistent_element)
    apply (simp add: setmap_identity_def) 
    using comp_apply of_def setmap.sel(1) setmap.sel(2) setmap.sel(3) setmap_existence_def 
    by (smt composableExistence composable_def setmap_composition_def)

  show "E (dom x) \<longrightarrow> E x" for x :: "'a setmap" 
    by (simp add: setmap_domain_def setmap_identity_def inexistent_element)

  show "E (cod x) \<longrightarrow> E x" for x :: "'a setmap" 
    by (simp add: setmap_codomain_def setmap_identity_def inexistent_element)

  show "\<not> E *" using inexistent_element by auto
qed


notation set_cat.isType ("isType")
notation set_cat.arrow ("_:_\<rightarrow>_" [120,120,120] 119) 
notation set_cat.isWedge ("_\<leftarrow>_\<midarrow> _ \<midarrow>_\<rightarrow>_" [120,120,120,120,120] 120)
notation set_cat.isProduct ("isProduct _\<leftarrow>_\<midarrow> _ \<midarrow>_\<rightarrow>_" [120,120,120,120,120] 120) 


lemma setmap_isType: "isType A \<longleftrightarrow> (A = Id (sdom A))"
  unfolding set_cat.isType_def setmap_identity_def fl apply auto 
  apply (simp add: setmap_domain_def setmap_identity_def) 
  apply (metis (full_types) of_def setmap.sel(1) setmap.sel(3) setmap_existence_def)
  apply (metis of_def setmap.sel(1) setmap.sel(3) setmap_domain_def setmap_existence_def 
         setmap_identity_def)
  by (metis of_def setmap.sel(1) setmap.sel(3) setmap_domain_def setmap_existence_def 
      setmap_identity_def)
 

text \<open>Products of trees:\<close>

definition setmap_product :: "'a setmap \<Rightarrow> 'a setmap \<Rightarrow> 'a setmap" (infix "\<^bold>\<times>" 120) where
  "A \<^bold>\<times> B \<equiv> if isType A \<and> isType B 
            then  Setmap (\<lambda>x. x) 
                    (Node (sdom A) (sdom B)) 
                    (Node (scod A) (scod B))
            else *"

definition setmap_projection1 :: "'a setmap \<Rightarrow> 'a setmap" ("\<pi>\<^sub>1") where
  "\<pi>\<^sub>1 P \<equiv> if (\<exists>A B. P \<simeq> A \<^bold>\<times> B)
          then Setmap (\<lambda>x. if x \<in>\<^sub>T sdom P then left x else x)
                                (sdom P) 
                                (left (sdom P))
          else *"

definition setmap_projection2 :: "'a setmap \<Rightarrow> 'a setmap" ("\<pi>\<^sub>2") where
  "\<pi>\<^sub>2 P \<equiv> if (\<exists>A B. P \<simeq> A \<^bold>\<times> B)
          then Setmap (\<lambda>x. if x \<in>\<^sub>T sdom P then right x else x)
                                (sdom P) 
                                (right (sdom P))
          else *"


lemma not_a_root_property:
  assumes "\<not> is_Root T" and "x \<in>\<^sub>T T" 
  shows "left x \<in>\<^sub>T left T" and "right x \<in>\<^sub>T right T"
  using assms apply(induction T, auto) 
  by (metis in_tree.simps(2) in_tree.simps(4) tree.exhaust_sel)+

section \<open>Instantiating the type as a binary product category\<close>
interpretation binaryProductCategory setmap_existence
                                     setmap_domain setmap_codomain 
                                     setmap_composition 
                                     setmap_inexistent_element
                                     setmap_product setmap_projection1 setmap_projection2
proof(unfold_locales)

  show "E (A \<^bold>\<times> B) \<longrightarrow> isType A \<and> isType B" for A B :: "'a setmap" 
    by (simp add: set_cat.inexistentElement setmap_product_def)
  show "E (\<pi>\<^sub>1 A) \<longrightarrow> (\<exists>B C. A \<simeq> B \<^bold>\<times> C)" for A :: "'a setmap" 
    by (simp add: set_cat.inexistentElement setmap_projection1_def)
  show "E (\<pi>\<^sub>2 A) \<longrightarrow> (\<exists>B C. A \<simeq> B \<^bold>\<times> C)" for A :: "'a setmap"
    by (simp add: set_cat.inexistentElement setmap_projection2_def)

  show "isType A \<and> isType B 
        \<longrightarrow>  isProduct A \<leftarrow>\<pi>\<^sub>1(A\<^bold>\<times>B)\<midarrow> A\<^bold>\<times>B \<midarrow>\<pi>\<^sub>2(A\<^bold>\<times>B)\<rightarrow> B" for A B :: "'a setmap" 
  proof (auto)
    assume "isType A" and "isType B"
    hence "E A" and "E B" 
      and "A = Id (sdom A)" and "B = Id (sdom B)"
      using set_cat.typesExist setmap_isType by auto 
  
    hence "E (A \<^bold>\<times> B)" 
      by (metis set_cat.typesExist setmap.sel(2) setmap.sel(3) setmap_identity_def setmap_isType 
          setmap_product_def)
    
    have factors_exist: "(\<exists>Aa Ba. A \<^bold>\<times> B \<simeq> Aa \<^bold>\<times> Ba) = True"
      using \<open>E (A \<^bold>\<times> B)\<close> unfolding fl by auto
  
    have not_a_root: "\<not> is_Root (sdom (A \<^bold>\<times> B))"
      unfolding setmap_product_def
      by (simp add: \<open>isType A\<close> \<open>isType B\<close>)

    have sdom_product: "sdom (A \<^bold>\<times> B) = Node (sdom A) (sdom B)"
      by (simp add: \<open>isType A\<close> \<open>isType B\<close> setmap_product_def)

    have "E (\<pi>\<^sub>1 (A \<^bold>\<times> B))" and "E (\<pi>\<^sub>2 (A \<^bold>\<times> B))" 
      unfolding setmap_projection1_def setmap_projection2_def 
        apply (simp_all only: factors_exist if_True)
      unfolding setmap_existence_def of_def setmap.sel(1) 
      using not_a_root not_a_root_property by auto

    have dom_p1: "dom (\<pi>\<^sub>1 (A\<^bold>\<times>B)) = A \<^bold>\<times> B"
      unfolding setmap_domain_def 
      by (metis (no_types, lifting) \<open>A = Id (sdom A)\<close> \<open>B = Id (sdom B)\<close> \<open>E (\<pi>\<^sub>1 (A \<^bold>\<times> B))\<close> 
          \<open>isType A\<close> \<open>isType B\<close> factors_exist setmap.sel(2) setmap.sel(3) 
          setmap_identity_def setmap_product_def setmap_projection1_def)

    have dom_p2: "dom (\<pi>\<^sub>2 (A\<^bold>\<times>B)) = A \<^bold>\<times> B"
      unfolding setmap_domain_def 
      by (metis (no_types, lifting) \<open>A = Id (sdom A)\<close> \<open>B = Id (sdom B)\<close> \<open>E (\<pi>\<^sub>2 (A \<^bold>\<times> B))\<close> 
          \<open>isType A\<close> \<open>isType B\<close> factors_exist setmap.sel(2) setmap.sel(3) 
          setmap_identity_def setmap_product_def setmap_projection2_def)

    have cod_p1: "cod (\<pi>\<^sub>1 (A\<^bold>\<times>B)) = A"
      unfolding setmap_codomain_def 
      by (metis (no_types, lifting) \<open>A = Id (sdom A)\<close> \<open>E (\<pi>\<^sub>1 (A \<^bold>\<times> B))\<close> \<open>isType A\<close> 
          \<open>isType B\<close> factors_exist setmap.sel(2) setmap.sel(3) setmap_product_def 
          setmap_projection1_def tree.sel(2))

    have cod_p2: "cod (\<pi>\<^sub>2 (A\<^bold>\<times>B)) = B"
      unfolding setmap_codomain_def 
      by (metis (no_types, lifting) \<open>B = Id (sdom B)\<close> \<open>E (\<pi>\<^sub>2 (A \<^bold>\<times> B))\<close> \<open>isType A\<close> 
          \<open>isType B\<close> factors_exist setmap.sel(2) setmap.sel(3) setmap_product_def 
          setmap_projection2_def tree.sel(3))

    have isWedge: "A\<leftarrow>(\<pi>\<^sub>1 (A \<^bold>\<times> B))\<midarrow>(A \<^bold>\<times> B)\<midarrow>(\<pi>\<^sub>2 (A \<^bold>\<times> B))\<rightarrow>B"
      by (simp add: \<open>E (A \<^bold>\<times> B)\<close> \<open>E A\<close> \<open>E B\<close> cod_p1 cod_p2 dom_p1 dom_p2 set_cat.arrow_def 
          set_cat.isWedge_def fl)

    have product_property: "A \<leftarrow>h\<midarrow> T \<midarrow>k\<rightarrow> B 
                            \<Longrightarrow> (\<exists>!u. \<pi>\<^sub>1 (A \<^bold>\<times> B) \<cdot> u \<simeq> h \<and> \<pi>\<^sub>2 (A \<^bold>\<times> B) \<cdot> u \<simeq> k)" for h T k
    proof -
      assume "A \<leftarrow>h\<midarrow> T \<midarrow>k\<rightarrow> B "
      hence "E h" and "E k" and "h:T\<rightarrow> A" and "k:T\<rightarrow>B"
        unfolding set_cat.isWedge_def set_cat.arrow_def set_cat.isType_def using catAx fl by blast+
      
      hence "scod T = sdom h" and "scod T = sdom k"
        by (smt set_cat.arrow_def setmap.sel(2-3) setmap_domain_def setmap_identity_def fl)+


      have "isType T"
        using \<open>k:T\<rightarrow>B\<close> set_cat.arrowImpliesIsType by auto

      define u where "u \<equiv> Setmap (\<lambda>x. if x \<in>\<^sub>T (scod T) then Node (h of x) (k of x) else x)
                                 (scod T) 
                                 (sdom (A \<^bold>\<times> B))"

      have "E u" 
        unfolding u_def setmap_existence_def apply (rule allI)
        subgoal for x apply (cases x, auto simp: of_def not_a_root) 
          by (smt \<open>E (A \<^bold>\<times> B)\<close> \<open>E h\<close> \<open>\<And>B A. E (A \<^bold>\<times> B) \<longrightarrow> isType A \<and> isType B\<close> 
                 \<open>E k\<close> \<open>h:T\<rightarrow>A\<close> \<open>k:T\<rightarrow>B\<close> in_tree.elims(3) is_Root_def  
                 set_cat.arrow_def setmap.sel(2) setmap.sel(3) setmap_codomain_def setmap_domain_def 
                 setmap_existence_def setmap_identity_def setmap_product_def tree.discI(2) of_def
                 tree.sel(2) tree.sel(3) fl)+
        done
 
      hence u_map: "u:T\<rightarrow>(A \<^bold>\<times> B)"
        unfolding u_def set_cat.arrow_def setmap_domain_def setmap_codomain_def using isWedge
        unfolding set_cat.isWedge_def set_cat.arrow_def 
        by (smt \<open>isType T\<close> category.arrowImpliesIsType isWedge set_cat.category_axioms 
            set_cat.isWedge_def set_cat.typesExist setmap.sel(2) setmap.sel(3) setmap_identity_def 
            setmap_isType fl)

      have scod_u: "scod u = sdom (\<pi>\<^sub>1 (A \<^bold>\<times> B))"
        by (metis (no_types, lifting) \<open>E (\<pi>\<^sub>1 (A \<^bold>\<times> B))\<close> category.arrowImpliesIsType dom_p1 
            set_cat.category_axioms setmap.sel(3) setmap_domain_def setmap_identity_def 
            setmap_isType u_def u_map)

      have scod_u2: "scod u = sdom (\<pi>\<^sub>2 (A \<^bold>\<times> B))"
        by (metis (no_types, lifting) \<open>E (\<pi>\<^sub>2 (A \<^bold>\<times> B))\<close> category.arrowImpliesIsType dom_p2 
            set_cat.category_axioms setmap.sel(3) setmap_domain_def setmap_identity_def 
            setmap_isType u_def u_map)

      have sdom_u: "sdom u = scod T"
        unfolding u_def by simp

      have comp1: "\<pi>\<^sub>1 (A \<^bold>\<times> B) \<cdot> u \<simeq> h" (* needs some reworking *)
      proof - 
        have composable: "composable (\<pi>\<^sub>1 (A \<^bold>\<times> B)) u"
          by (simp add: \<open>E (\<pi>\<^sub>1 (A \<^bold>\<times> B))\<close> \<open>E u\<close> composable_def scod_u)
        
        have x_not_in: "\<not> x \<in>\<^sub>T scod T \<Longrightarrow> smap h x = x" for x
          using \<open>E h\<close> unfolding \<open>scod T = sdom h\<close> setmap_existence_def of_def by simp
        
        have x_in: "x \<in>\<^sub>T scod T \<Longrightarrow> Node (smap h x) (smap k x) \<in>\<^sub>T sdom (A \<^bold>\<times> B)" for x
          using \<open>E h\<close> \<open>E k\<close> unfolding setmap_existence_def sdom_product of_def apply auto 
          using \<open>E h\<close> \<open>E k\<close> \<open>scod T = sdom h\<close> \<open>scod T = sdom k\<close> \<open>h:T\<rightarrow>A\<close> 
                \<open>k:T\<rightarrow>B\<close>
            by (metis (mono_tags, hide_lams) composableExistence composable_def set_cat.arrow_def 
                set_cat.leftCompIdentity fl)+

        have smap_h: "smap h = (\<lambda>x. if x \<in>\<^sub>T sdom u then smap (\<pi>\<^sub>1 (A \<^bold>\<times> B)) (smap u x) else x)"
          unfolding sdom_u u_def setmap_projection1_def 
          apply(simp only: factors_exist if_True, auto, rule HOL.ext) 
          subgoal for x by (cases "x \<in>\<^sub>T scod T", auto simp: of_def x_in x_not_in) done

        have "h = Setmap (\<lambda>x. if x \<in>\<^sub>T sdom u 
                                 then smap (\<pi>\<^sub>1 (A \<^bold>\<times> B)) (smap u x) 
                                 else x) 
                            (sdom u) (scod (\<pi>\<^sub>1 (A \<^bold>\<times> B)))"
          apply (rule setmap.expand, auto simp: smap_h) 
          apply (metis (no_types, lifting) \<open>E h\<close> \<open>h:T\<rightarrow>A\<close> set_cat.arrow_def ExId_def
                 setmap.exhaust_sel setmap.inject setmap_domain_def setmap_identity_def u_def)
          by (metis \<open>E (\<pi>\<^sub>1 (A \<^bold>\<times> B))\<close> \<open>E h\<close> \<open>h:T\<rightarrow>A\<close> cod_p1 set_cat.arrow_def 
              setmap.inject setmap_codomain_def setmap_identity_def ExId_def)

        thus ?thesis
        unfolding setmap_composition_def using \<open>E h\<close> \<open>E u\<close> composable fl by auto
      qed

      have comp2: "\<pi>\<^sub>2 (A \<^bold>\<times> B) \<cdot> u \<simeq> k"
      proof -
        have composable: "composable (\<pi>\<^sub>2 (A \<^bold>\<times> B)) u"
          by (simp add: \<open>E (\<pi>\<^sub>2 (A \<^bold>\<times> B))\<close> \<open>E u\<close> composable_def scod_u2)
        
        have x_not_in: "\<not> x \<in>\<^sub>T scod T \<Longrightarrow> smap k x = x" for x
          using \<open>E k\<close> unfolding \<open>scod T = sdom k\<close> setmap_existence_def of_def by simp
        
        have x_in: "x \<in>\<^sub>T scod T \<Longrightarrow> Node (smap h x) (smap k x) \<in>\<^sub>T sdom (A \<^bold>\<times> B)" for x
          using \<open>E h\<close> \<open>E k\<close> unfolding setmap_existence_def sdom_product of_def apply auto 
          using \<open>E h\<close> \<open>E k\<close> \<open>scod T = sdom h\<close> \<open>scod T = sdom k\<close> \<open>h:T\<rightarrow>A\<close> 
                \<open>k:T\<rightarrow>B\<close>
            by (metis (mono_tags, hide_lams) composableExistence composable_def set_cat.arrow_def 
                set_cat.leftCompIdentity fl)+

        have smap_k: "smap k = (\<lambda>x. if x \<in>\<^sub>T sdom u then smap (\<pi>\<^sub>2 (A \<^bold>\<times> B)) (smap u x) else x)"
          unfolding sdom_u u_def setmap_projection2_def 
          apply(simp only: factors_exist if_True, auto, rule HOL.ext) 
          subgoal for x by (cases "x \<in>\<^sub>T scod T", auto simp: of_def x_in x_not_in) done

        have "k = Setmap (\<lambda>x. if x \<in>\<^sub>T sdom u 
                                 then smap (\<pi>\<^sub>2 (A \<^bold>\<times> B)) (smap u x) 
                                 else x) 
                            (sdom u) (scod (\<pi>\<^sub>2 (A \<^bold>\<times> B)))"
          apply (rule setmap.expand, auto simp: smap_k) 
          apply (metis (no_types, lifting) ExId_def \<open>E k\<close> \<open>k:T\<rightarrow>B\<close> set_cat.arrow_def 
                 setmap.exhaust_sel setmap.inject setmap_domain_def setmap_identity_def u_def)
          by (metis \<open>E (\<pi>\<^sub>2 (A \<^bold>\<times> B))\<close> \<open>E k\<close> \<open>k:T\<rightarrow>B\<close> ExId_def cod_p2 set_cat.arrow_def 
              setmap.inject setmap_codomain_def setmap_identity_def)

        thus ?thesis
        unfolding setmap_composition_def using \<open>E k\<close> \<open>E u\<close> fl composable by auto
      qed

      have uniqueness: "\<pi>\<^sub>1 (A \<^bold>\<times> B) \<cdot> v \<simeq> h \<Longrightarrow> \<pi>\<^sub>2 (A \<^bold>\<times> B) \<cdot> v \<simeq> k \<Longrightarrow> u = v" for v
        proof -
          assume asm1: "\<pi>\<^sub>1 (A \<^bold>\<times> B) \<cdot> v \<simeq> h" and asm2: "\<pi>\<^sub>2 (A \<^bold>\<times> B) \<cdot> v \<simeq> k"

          hence "E v"
            using set_cat.codImpliesExistence set_cat.compositionExistence fl by blast

          have "sdom v = sdom u"
            using \<open>scod T = sdom h\<close> compdom composableExistence sdom_u asm1 fl by fastforce

          have "scod v = scod u"
            using \<open>scod T = sdom h\<close> compdom composableExistence scod_u asm1 fl
            using composable_def by fastforce
      
          have c1: "composable (\<pi>\<^sub>1 (A \<^bold>\<times> B)) v = True"
            using asm1 composableExistence fl by blast

          have c2: "composable (\<pi>\<^sub>2 (A \<^bold>\<times> B)) v = True"
            using asm2 composableExistence fl by blast
        
          have smap_h: "x \<in>\<^sub>T sdom v \<Longrightarrow> smap h x = left (smap v x)" for x
            using asm1 unfolding setmap_composition_def c1 if_True 
            unfolding setmap_projection1_def factors_exist if_True setmap.sel(1) 
            by (smt \<open>E v\<close> \<open>scod v = scod u\<close> of_def setmap.sel(1) setmap.sel(3) setmap_existence_def 
                u_def fl)

          have smap_k: "x \<in>\<^sub>T sdom v \<Longrightarrow> smap k x = right (smap v x)" for x
            using asm2 unfolding setmap_composition_def c2 if_True 
            unfolding setmap_projection2_def factors_exist if_True setmap.sel(1) 
            by (smt \<open>E v\<close> \<open>scod v = scod u\<close> of_def setmap.sel(1) setmap.sel(3) setmap_existence_def 
                u_def fl)

          have "x \<in>\<^sub>T sdom v \<Longrightarrow> smap v x = Node (left (smap v x)) (right (smap v x))" for x
            by (metis (no_types, lifting) \<open>E v\<close> \<open>scod v = scod u\<close> in_tree.simps(4) of_def 
                sdom_product setmap.sel(3) setmap_existence_def tree.exhaust_sel u_def)

          hence x_in: "x \<in>\<^sub>T sdom v \<Longrightarrow> smap v x = smap u x" for x
            using smap_h smap_k by (smt \<open>sdom v = sdom u\<close> of_def sdom_u setmap.sel(1) u_def)
            
          have x_not_in: "\<not> x \<in>\<^sub>T sdom v \<Longrightarrow> smap v x = smap u x" for x
            using \<open>E v\<close> \<open>E u\<close> unfolding setmap_existence_def \<open>sdom v = sdom u\<close> of_def by auto

          show "u = v"  
            apply(rule setmap.expand, auto simp: \<open>sdom v = sdom u\<close> \<open>scod v = scod u\<close>)
            apply(rule HOL.ext) using x_in x_not_in by metis
        qed

      show "(\<exists>!u. \<pi>\<^sub>1 (A \<^bold>\<times> B) \<cdot> u \<simeq> h \<and> \<pi>\<^sub>2 (A \<^bold>\<times> B) \<cdot> u \<simeq> k)" 
        using comp1 comp2 uniqueness by auto
    qed

    show "isProduct A \<leftarrow>\<pi>\<^sub>1 (A \<^bold>\<times> B)\<midarrow> A \<^bold>\<times> B \<midarrow>\<pi>\<^sub>2 (A \<^bold>\<times> B)\<rightarrow> B" 
      unfolding set_cat.isProduct_def using isWedge product_property by auto 
  qed

qed


end