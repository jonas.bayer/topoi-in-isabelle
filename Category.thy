theory Category imports FreeLogic

begin

text \<open>Setup of automation tools\<close>
  declare [[ smt_solver = cvc4, smt_oracle = true, smt_timeout = 300]] 
  sledgehammer_params [provers = cvc4 z3 e vampire remote_leo3, timeout = 120, isar_proofs = false]
  nitpick_params [user_axioms, show_all, format = 2, expect = genuine]

text \<open>Define the following theorem lists for\<close>
  named_theorems defs  (* definitions in the context of a category *)
  named_theorems pdefs (* definitions of products and related concepts *)
  named_theorems cat   (* category axioms and lemmas *)
  named_theorems catAx (* category axioms *)

section \<open>The Basics of Category Theory\<close>

locale category = free_logic +    
  fixes 
          domain :: "'a \<Rightarrow> 'a" ("dom _" [120] 120) and
          codomain :: "'a \<Rightarrow> 'a" ("cod _" [120] 120) and
          composition :: "'a \<Rightarrow> 'a  \<Rightarrow> 'a" (infix "\<cdot>" 120) and
          inexistent_element :: "'a" ("*")

  assumes  
          associativity[cat, catAx]:        "(x\<cdot>y)\<cdot>z \<cong> x\<cdot>(y\<cdot>z)" and
          rightCompIdentity[cat, catAx]:    "x \<cdot> (dom x) \<cong> x" and
          leftCompIdentity[cat, catAx]:     "(cod x) \<cdot> x \<cong> x" and 
   
          compositionExistence[cat, catAx]: "E (x \<cdot> y) \<longleftrightarrow> dom x \<simeq> cod y" and
          domImpliesExistence[cat, catAx]:  "E (dom x) \<longrightarrow> E x" and
          codImpliesExistence[cat, catAx]:  "E (cod x) \<longrightarrow> E x" and
  
          inexistentElement[cat, catAx]:    "\<not> (E *)" 
begin

lemma "True" nitpick[satisfy, card=4] oops

subsection \<open>Basic definitions\<close>

  definition isType ("isType") where 
    "isType x \<equiv> x \<simeq> dom x" 

  definition arrow ("_:_\<rightarrow>_" [120,120,120] 119) where
    "f:A\<rightarrow>B \<equiv> dom f \<simeq> A \<and> cod f \<simeq> B"

  declare isType_def[defs] arrow_def[defs]

  lemma typesExist: "isType t \<longrightarrow> E t" unfolding ExId_def defs by simp

  lemma arrowImpliesIsType: "x:A\<rightarrow>B \<Longrightarrow> isType A \<and> isType B" 
    unfolding defs using cat fl by metis
  
  lemma arrowComposition: "f:A\<rightarrow>B \<Longrightarrow> g:B\<rightarrow>C \<Longrightarrow> g\<cdot>f:A\<rightarrow>C"
    unfolding fl arrow_def using cat fl by metis

  declare typesExist[cat] arrowImpliesIsType[cat]

subsection \<open>Isomorphisms\<close>

  definition inverseOf :: "'a \<Rightarrow> 'a \<Rightarrow> bool" (infix "inverseOf" 120) where
    "f inverseOf g \<equiv> isType (f\<cdot>g) \<and> isType (g\<cdot>f)"

  definition isIsomorphism :: "'a \<Rightarrow> bool" where
    "isIsomorphism f \<equiv> \<exists>g. g inverseOf f"

  definition isomorphismArrow ("_:_\<Rightarrow>_" [120,120,120] 119) where
    "f:A\<Rightarrow>B \<equiv> f:A\<rightarrow>B \<and> isIsomorphism f"

  definition isomorphicTo :: "'a \<Rightarrow> 'a \<Rightarrow> bool" (infix "isomorphicTo" 120) where
    "A isomorphicTo B \<equiv> \<exists>f. f:A\<Rightarrow>B"

  declare inverseOf_def[defs] isIsomorphism_def[defs] isomorphismArrow_def[defs] 
          isomorphicTo_def[defs]

  lemma isomorphismHasUniqueInverse: "isIsomorphism f \<Longrightarrow> \<exists>!g. g inverseOf f"
    unfolding defs using cat fl by smt

  lemma isomorphismInverse: 
    "isIsomorphism f \<Longrightarrow> g inverseOf f \<Longrightarrow> g:(cod f)\<rightarrow>(dom f) \<and> isIsomorphism g"
    unfolding defs using cat fl by smt

  lemma isomorphicTo_symmetric: "A isomorphicTo B \<longleftrightarrow> B isomorphicTo A" 
    unfolding defs using cat fl by smt

  declare isomorphismHasUniqueInverse[cat] isomorphismInverse[cat] isomorphicTo_symmetric[cat]

subsection \<open>Monics and Epis\<close>

  definition isMonic :: "'a \<Rightarrow> bool" where
    "isMonic m \<equiv> E m \<and> (\<forall>f g. m\<cdot>f \<simeq> m\<cdot>g \<longrightarrow> f \<simeq> g)"

  definition isEpi :: "'a \<Rightarrow> bool" where
    "isEpi m \<equiv> E m \<and> (\<forall>f g. f\<cdot>m \<simeq> g\<cdot>m \<longrightarrow> f \<simeq> g)"

  declare isMonic_def[defs] isEpi_def[defs] 

  lemma isomorphismsMonic: "isIsomorphism f \<Longrightarrow> isMonic f"
    unfolding defs using cat by (smt fl isomorphismHasUniqueInverse)

  lemma isomorphismsEpic: "isIsomorphism f \<Longrightarrow> isEpi f"
    unfolding defs using cat by (smt fl isomorphismHasUniqueInverse)

  lemma "isMonic m \<and> isEpi m \<longrightarrow> isIsomorphism m" nitpick oops

  declare isomorphismsMonic[cat] isomorphismsEpic[cat]

subsection \<open>Final and Initial objects\<close>

  (* When doing refactor: remove redundant part of definition: don't need the isType in the 
     beginnning! *)

  definition isFinal :: "'a \<Rightarrow> bool" where
    "isFinal P \<equiv> isType P \<and> (\<forall>A. (isType A) \<longrightarrow> (\<exists>!f. f:A\<rightarrow>P))"

  definition isInitial :: "'a \<Rightarrow> bool" where
    "isInitial I \<equiv> isType I \<and> (\<forall>A. (isType A) \<longrightarrow> (\<exists>!f. f:I\<rightarrow>A))"

  declare isFinal_def[defs] isInitial_def[defs]

  lemma IsomorphicToFinalIsFinal: "isFinal P \<Longrightarrow> f:Q\<Rightarrow>P \<Longrightarrow> isFinal Q"
    unfolding defs using cat fl by smt

  lemma FinalsAreIsomorphic: "isFinal P \<Longrightarrow> isFinal Q \<Longrightarrow> (\<exists>f. f:P\<Rightarrow>Q)" 
    unfolding defs by (smt associativity compositionExistence leftCompIdentity rightCompIdentity fl)

  lemma IsomorphicToInitialIsInitial: "isInitial A \<and> f:A\<Rightarrow>B \<longrightarrow> isInitial B" 
    unfolding defs using cat fl by smt

  lemma InitialsAreIsomorphic: "isInitial A \<and> isInitial B \<longrightarrow> A isomorphicTo B"
    unfolding defs fl using cat 
    by (smt ExId_def KlEq_def)      

  declare IsomorphicToFinalIsFinal[cat] FinalsAreIsomorphic[cat] IsomorphicToInitialIsInitial[cat]
          InitialsAreIsomorphic[cat]

subsection \<open>Subobjects\<close>
  definition generalized_element :: "'a \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> bool" ("_ _-in _" [120,120,120] 120) where
    "x A-in B \<equiv> x:A\<rightarrow>B"

  definition isSubobjectOf (infix "isSubobjectOf" 120) where
    "i isSubobjectOf A \<equiv> isMonic i \<and> cod i \<simeq> A"

  definition memberOf (infix "memberOf" 120) where
    "x memberOf i \<equiv> (x (dom x)-in (cod i)) \<and> (\<exists>h. h:(dom x)\<rightarrow>(dom i) \<and> i\<cdot>h \<simeq> x)"

  definition includedIn :: "'a \<Rightarrow> 'a \<Rightarrow> bool" (infix "includedIn" 120) where
    "i includedIn j \<equiv> i isSubobjectOf (cod j) \<and> j isSubobjectOf (cod j) \<and> (\<exists>s. E s \<and> j \<cdot> s = i)"

  definition equivalentTo :: "'a \<Rightarrow> 'a \<Rightarrow> bool" (infix "equivalentTo" 120) where
    "i equivalentTo j \<equiv> i includedIn j \<and> j includedIn i"

  declare generalized_element_def[defs] isSubobjectOf_def[defs] memberOf_def[defs] 
          includedIn_def[defs] equivalentTo_def[defs]

  lemma includedInMemberOf:
    assumes "i isSubobjectOf (cod j)" and "j isSubobjectOf (cod j)" 
    shows "i includedIn j \<longleftrightarrow> (\<forall>m. m memberOf i \<longrightarrow> m memberOf j)"
    apply auto
    subgoal for m unfolding defs using fl cat by smt
    subgoal using assms unfolding defs using fl cat by smt
    done

  lemma equivalentToMemberOf:
    assumes "i isSubobjectOf (cod j)" and "j isSubobjectOf (cod j)" 
    shows "i equivalentTo j \<longleftrightarrow> (\<forall>m. m memberOf i \<longleftrightarrow> m memberOf j)"
    using assms includedInMemberOf equivalentTo_def fl isSubobjectOf_def by auto

  lemma compositionOfMonics: "isMonic i \<Longrightarrow> isMonic j \<Longrightarrow> E (j \<cdot> i) \<Longrightarrow> isMonic (j \<cdot> i)"
    unfolding defs using cat fl by smt

  lemma compositionMonicImpliesFirstMonic: "isMonic g \<Longrightarrow> isMonic (g \<cdot> s) \<Longrightarrow> E s \<Longrightarrow> isMonic s"
    unfolding defs using cat fl sorry
    
  declare includedInMemberOf[cat] equivalentToMemberOf[cat] compositionOfMonics[cat] 
          compositionMonicImpliesFirstMonic[cat]

end

end