theory Limits imports Category

begin

context category 
begin

fun is_path :: "'a list \<Rightarrow> bool" where
  "is_path (a # b # p) = (cod a \<simeq> dom b \<and> is_path (b # p))" | 
  "is_path [a] = E a" |
  "is_path [] = False"

fun walk :: "'a list \<Rightarrow> 'a" where
  "walk [] = *" |
  "walk [f] = f" |
  "walk (f # g # p) = walk (g # p) \<cdot> f"

lemma walk_exists: "is_path p \<Longrightarrow> E (walk p)"
  apply (induction p, auto) subgoal for a p apply (cases p, auto) 
    by (smt fl associativity compositionExistence domImpliesExistence list.distinct(1) list.sel(1) 
        rightCompIdentity walk.elims)
  done

fun path_start :: "'a list \<Rightarrow> 'a" where
  "path_start (a # p) = dom a" |
  "path_start [] = *"

fun path_end :: "'a list \<Rightarrow> 'a" where
  "path_end [] = *" |
  "path_end [a] = cod a" |
  "path_end (a # b # p) = path_end (b # p)"

lemma "is_path p \<Longrightarrow> walk p:path_start p\<rightarrow>path_end p"
  apply (induction p, auto)
  subgoal for a p unfolding arrow_def apply (cases p)
    subgoal 
      by (metis compositionExistence is_path.simps(2) leftCompIdentity path_end.simps(2) 
          rightCompIdentity walk.simps(2) fl) 
    subgoal
      by (smt associativity category.codImpliesExistence category.is_path.simps(1) category_axioms 
          compositionExistence leftCompIdentity path_end.simps(3) path_start.simps(1) 
          rightCompIdentity walk.simps(3) fl)
    done
  done

definition parallel_paths :: "'a list \<Rightarrow> 'a list \<Rightarrow> bool" where
  "parallel_paths p q \<equiv> is_path p \<and> is_path q
                      \<and> path_start p = path_start q
                      \<and> path_end p = path_end q"

fun path_in :: "'a list \<Rightarrow> 'a set \<Rightarrow> bool" where
  "path_in [] A = False" |
  "path_in [f] A = (f \<in> A)" |
  "path_in (f # g # p) A = ((f \<in> A) \<and> path_in (g # p) A)" 

definition is_diagram :: "'a set \<Rightarrow> bool" where
  "is_diagram D \<equiv> (\<forall>p q. parallel_paths p q \<longrightarrow> path_in p D \<longrightarrow> path_in q D \<longrightarrow> walk p \<simeq> walk q)
                \<and> (\<forall>x \<in> D. E x)"

lemma walk_types: "isType a \<Longrightarrow> path_in q {a} \<Longrightarrow> a = walk q"
  apply(induction q, auto) 
  by (smt isType_def list.distinct(1) list.inject path_in.simps(2) path_in.simps(3) 
      rightCompIdentity singletonD walk.elims fl)

lemma "isType a \<Longrightarrow> is_diagram {a}"
  unfolding is_diagram_def unfolding parallel_paths_def apply(auto simp: walk_exists typesExist) 
  subgoal for p q
    apply(induction p, cases p) using walk_types unfolding fl 
    apply simp_all using fl by (simp add: KlEq_def typesExist)
  done

definition is_star :: "'a set \<Rightarrow> bool" where
  "is_star D \<equiv> (\<forall>f \<in> D. \<forall>g \<in> D. dom f \<simeq> dom g)"

definition center :: "'a set \<Rightarrow> 'a" where
  "center D \<equiv> dom (SOME x. x \<in> D)"

definition is_above :: "'a set \<Rightarrow> 'a set \<Rightarrow> bool" ("_ is'_above _") where
  "C is_above D \<equiv> is_diagram D \<and> is_star C \<and> (\<forall>d \<in> D. \<exists>!c \<in> C. cod c = d)"

definition is_cone_of :: "'a set \<Rightarrow> 'a set \<Rightarrow> bool" ("_ is'_cone'_of _") where
  "U is_cone_of D \<equiv> (U is_above D) \<and> (\<forall>C. (C is_above D) \<longrightarrow> (\<exists>!\<phi>. \<phi>:center C\<rightarrow>center U 
                                                              \<and> isIsomorphism \<phi>
                                                              \<and> is_diagram (U \<union> C \<union> {\<phi>})))"

end

end