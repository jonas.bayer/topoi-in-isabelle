theory BinaryProductCategory2 imports Constructions

begin

section \<open>Categories with binary products\<close>

definition (in category) isArrowWedge ("_\<leftarrow>_\<midarrow>_\<rightarrow>_" [120,120,120] 120) where
    "f \<leftarrow>h\<midarrow>k\<rightarrow> g \<equiv> dom h \<simeq> dom k \<and> dom f \<simeq> cod h \<and> dom g \<simeq> cod k"

definition (in category) isArrowProduct 
    ("isArrowProduct _\<leftarrow>_\<midarrow> _ \<midarrow>_\<rightarrow>_" [120,120,120,120] 120) where
    "isArrowProduct f \<leftarrow>p1\<midarrow> p \<midarrow>p2\<rightarrow> g \<equiv> (\<forall>h k. (f \<leftarrow>h\<midarrow>k\<rightarrow> g)
                                          \<longrightarrow> (\<exists>!u. p1 \<cdot> (p \<cdot> u) \<simeq> f \<cdot> h \<and> p2 \<cdot> (p \<cdot> u) \<simeq> g \<cdot> k))"

locale binaryProductCategory = category +
    fixes product :: "'a \<Rightarrow> 'a \<Rightarrow> 'a" (infix "\<^bold>\<times>" 120) and
          projection1 :: "'a \<Rightarrow> 'a" ("\<pi>\<^sub>1") 
          (* Takes a product and returns the morphism from the product to its first factor *) and
          projection2 :: "'a \<Rightarrow> 'a" ("\<pi>\<^sub>2")
          (* Takes a product and returns the morphism from the product to its second factor *)
  assumes 
          productOfArrows:  "E f \<and> E g 
                            \<longrightarrow> isArrowProduct f \<leftarrow>\<pi>\<^sub>1 (f \<^bold>\<times> g)\<midarrow> f \<^bold>\<times> g \<midarrow>\<pi>\<^sub>2 (f \<^bold>\<times> g)\<rightarrow> g" and
          
          productExistence:     "E (f \<^bold>\<times> g) \<longrightarrow> (E f \<and> E g)" and
          projection1Existence: "E (\<pi>\<^sub>1 f) \<longrightarrow> (E f)" and
          projection2Existence: "E (\<pi>\<^sub>2 f) \<longrightarrow> (E f)"
          
begin

end