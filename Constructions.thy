theory Constructions imports Squares 

begin

section \<open>Constructions in Category Theory\<close>

context category begin

subsection \<open>Equalizers\<close>

  definition isParallelPair :: "'a \<Rightarrow> 'a \<Rightarrow> bool" where
    "isParallelPair f g \<equiv> dom f \<simeq> dom g \<and> cod f \<simeq> cod g"

  definition equalizes :: "'a \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> bool" ("_ equalizes _ _" [120, 120, 120]) where
    "h equalizes f g \<equiv> f\<cdot>h \<simeq> g\<cdot>h"

  definition factorsUniquelyThrough :: "'a \<Rightarrow> 'a \<Rightarrow> bool" (infix "factorsUniquelyThrough" 120) where
    "h factorsUniquelyThrough e \<equiv> (\<exists>!u. e\<cdot>u \<simeq> h)"

  definition equalizerOf :: "'a \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> bool" 
    ("_ equalizerOf _ _" [120, 120, 120] 120) where 
    "(e equalizerOf f g) \<equiv> isParallelPair f g 
                         \<and> e equalizes f g 
                         \<and> (\<forall>h. h equalizes f g \<longrightarrow> h factorsUniquelyThrough e)"

  declare isParallelPair_def[defs] equalizes_def[defs] factorsUniquelyThrough_def[defs] 
          equalizerOf_def[defs]

  lemma equalizersAreMonic: "e equalizerOf f g \<Longrightarrow> isMonic e"
    unfolding defs fl by (metis associativity fl rightCompIdentity compositionExistence)
 
  lemma epicEqualizerIsIsomorphism:
    assumes "e equalizerOf f g" and "isEpi e"
    shows "isIsomorphism e" 
    using assms unfolding defs using cat fl by smt

  lemma equalizersAndSubobjects: 
    assumes "isParallelPair f g" and "e isSubobjectOf (dom f)"
    shows "e equalizerOf f g \<longleftrightarrow> (\<forall>x. cod x \<simeq> dom f \<longrightarrow> (x memberOf e \<longleftrightarrow> f\<cdot>x \<simeq> g\<cdot>x))" 
    using assms unfolding defs fl using cat fl by smt

  declare equalizersAreMonic[cat] epicEqualizerIsIsomorphism[cat] equalizersAndSubobjects[cat]

subsection \<open>Products\<close>

  definition isWedge ("_\<leftarrow>_\<midarrow> _ \<midarrow>_\<rightarrow>_" [120,120,120,120,120] 120) where
    "A \<leftarrow>f\<midarrow> C \<midarrow>g\<rightarrow> B \<equiv> f:C\<rightarrow>A \<and> g:C\<rightarrow>B"

  definition isProduct :: "'a \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> bool" 
    ("isProduct _\<leftarrow>_\<midarrow> _ \<midarrow>_\<rightarrow>_" [120,120,120,120,120] 120) where
    "isProduct A \<leftarrow>p1\<midarrow> P \<midarrow>p2\<rightarrow> B \<equiv> A \<leftarrow>p1\<midarrow> P \<midarrow>p2\<rightarrow> B \<and> 
                           (\<forall>h T k. A \<leftarrow>h\<midarrow> T \<midarrow>k\<rightarrow> B \<longrightarrow> (\<exists>!u. p1\<cdot>u \<simeq> h \<and> p2\<cdot>u \<simeq> k))"

  declare isWedge_def[pdefs] isProduct_def[pdefs]

  text \<open>Some simple lemmas on products: \<close>
  lemma isomorphicToProductIsProduct:
    assumes "isProduct A \<leftarrow>p1\<midarrow> P \<midarrow>p2\<rightarrow> B"
        and "f:Q\<rightarrow>P" and "isIsomorphism f"
      shows "isProduct A \<leftarrow>p1 \<cdot> f\<midarrow> Q \<midarrow>p2 \<cdot> f\<rightarrow> B"
    using assms unfolding defs pdefs apply simp unfolding fl by (smt cat fl)
  
  lemma products_isomorphism:
    assumes "isProduct A \<leftarrow>p1\<midarrow> P \<midarrow>p2\<rightarrow> B"
        and "isProduct A \<leftarrow>q1\<midarrow> Q \<midarrow>q2\<rightarrow> B"
      shows "\<exists>u. u:Q\<rightarrow>P \<and> p1\<cdot>u\<simeq>q1 \<and> p2\<cdot>u\<simeq>q2 \<and> isIsomorphism u"
    using assms unfolding defs pdefs apply simp by (smt cat fl)

  lemma products_isomorphic:
    assumes "isProduct A \<leftarrow>p1\<midarrow> P \<midarrow>p2\<rightarrow> B"
        and "isProduct A \<leftarrow>q1\<midarrow> Q \<midarrow>q2\<rightarrow> B"
      shows "Q isomorphicTo P"
    using assms unfolding defs pdefs apply simp by (smt cat fl)

  lemma uniqueArrowIntoProduct:
    assumes "isProduct A \<leftarrow>p1\<midarrow> P \<midarrow>p2\<rightarrow> B"
        and "p1 \<cdot> f \<simeq> p1 \<cdot> g" and "p2 \<cdot> f \<simeq> p2 \<cdot> g" 
      shows "f \<simeq> g"
    using assms unfolding isProduct_def isWedge_def arrow_def
    using associativity codImpliesExistence compositionExistence leftCompIdentity 
          rightCompIdentity unfolding fl 
    by smt

  declare isomorphicToProductIsProduct[cat] products_isomorphism[cat] products_isomorphic[cat]

subsection \<open>Coproducts\<close>

  definition isCowedge ("_\<midarrow>_\<rightarrow>_\<leftarrow>_\<midarrow>_" [120,120,120,120,120] 120) where
    "A \<midarrow>f\<rightarrow> C \<leftarrow>g\<midarrow> B \<equiv> f:A\<rightarrow>C \<and> g:B\<rightarrow>C"

  definition isCoproduct :: "'a \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> bool" 
    ("isCoproduct _ \<midarrow>_\<rightarrow> _ \<leftarrow>_\<midarrow> _" [120,120,120,120,120] 120) where
    "isCoproduct A \<midarrow>i1\<rightarrow> P \<leftarrow>i2\<midarrow> B \<equiv> A \<midarrow>i1\<rightarrow> P \<leftarrow>i2\<midarrow> B \<and> 
                           (\<forall>h T k. A \<midarrow>h\<rightarrow> T \<leftarrow>k\<midarrow> B \<longrightarrow> (\<exists>!u. u\<cdot>i1 \<simeq> h \<and> u\<cdot>i2 \<simeq> k))"

subsection \<open>Pullbacks\<close> 

  definition isCorner :: "'a \<Rightarrow> 'a \<Rightarrow> bool" where
    "isCorner f g = cod f \<simeq> cod g"

  definition isPullback :: "'a \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> bool" where
   "isPullback f g p1 p2 \<equiv> (f \<cdot> p1 \<simeq> g \<cdot> p2 \<and>  (\<forall>h k. f\<cdot>h \<simeq> g\<cdot>k \<longrightarrow> (\<exists>!u. p1 \<cdot> u \<simeq> h \<and> p2 \<cdot> u \<simeq> k)))"

text \<open>cf. the Picture on page 41 of McLarty's book \<close>
  abbreviation isPullbackDiagram :: "'a \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> bool"
  ("isPullbackDiagram \<box> \<midarrow>_\<rightarrow> \<box> // _\<down>  \<down>_ // \<box> \<midarrow>_\<rightarrow> \<box>" [120, 120, 120, 120] 120) where
   "isPullbackDiagram   \<box> \<midarrow>p2\<rightarrow> \<box> 
                      p1\<down>        \<down>g 
                        \<box> \<midarrow> f\<rightarrow> \<box>
    \<equiv> isPullback f g p1 p2"

  declare isCorner_def[defs] isPullback_def[defs]

  lemma EqualizerOfProductIsPullback:
    assumes "isProduct (dom f) \<leftarrow>p1\<midarrow> P \<midarrow>p2\<rightarrow> (dom g)"
        and "e equalizerOf (f\<cdot>p1) (g\<cdot>p2)"
      shows "isPullbackDiagram   \<box> \<midarrow>p2\<cdot>e\<rightarrow>\<box> 
                             p1\<cdot>e\<down>        \<down>g 
                                 \<box> \<midarrow> f \<rightarrow>\<box>"
  proof - 
      have "f \<cdot> (p1 \<cdot> e) \<simeq> g \<cdot> (p2 \<cdot> e)" 
        using assms(2) equalizerOf_def equalizes_def associativity fl by smt
  
      moreover have "f \<cdot> h \<simeq> g \<cdot> k \<longrightarrow> (\<exists>!u. (p1 \<cdot> e) \<cdot> u \<simeq> h \<and> (p2 \<cdot> e) \<cdot> u \<simeq> k)" for h k
      proof (rule impI)
        assume "f \<cdot> h \<simeq> g \<cdot> k" 

        hence "(dom f)\<leftarrow>h\<midarrow> (dom h) \<midarrow>k\<rightarrow>(dom g)" unfolding isWedge_def arrow_def using cat fl by smt

        hence "\<exists>!x. p1 \<cdot> x \<simeq> h \<and> p2 \<cdot> x \<simeq> k" 
          using assms(1) unfolding isProduct_def by auto 

        then obtain x where x: "p1 \<cdot> x \<simeq> h \<and> p2 \<cdot> x \<simeq> k" by auto
    
        have "(f \<cdot> p1) \<cdot> x \<simeq> (g \<cdot> p2) \<cdot> x \<longrightarrow> (\<exists>!u. e \<cdot> u \<simeq> x)"
          using assms(2) unfolding equalizerOf_def equalizes_def factorsUniquelyThrough_def by auto
    
        hence "\<exists>!u. e \<cdot> u \<simeq> x" 
          using associativity x \<open>f \<cdot> h \<simeq> g \<cdot> k\<close> fl by auto
        
        show "\<exists>!u. (p1 \<cdot> e) \<cdot> u \<simeq> h \<and> (p2 \<cdot> e) \<cdot> u \<simeq> k"
          by (smt \<open>\<exists>!u. e \<cdot> u \<simeq> x\<close> \<open>\<exists>!x. p1 \<cdot> x \<simeq> h \<and> p2 \<cdot> x \<simeq> k\<close> fl local.associativity x)
      qed
  
      ultimately show ?thesis unfolding isPullback_def by auto
  qed

  lemma pullbackArrowToProductIsEqualizer:
    assumes "isProduct (dom f) \<leftarrow>p1\<midarrow> P \<midarrow>p2\<rightarrow> (dom g)"
        and "isPullbackDiagram   \<box> \<midarrow>b2\<rightarrow> \<box>
                               b1\<down>        \<down>g
                                 \<box> \<midarrow> f\<rightarrow> \<box>"
      shows "\<exists>e. e:(dom b1)\<rightarrow>P \<and> e equalizerOf (f\<cdot>p1) (g\<cdot>p2)"
  proof - 

    have "(dom f)\<leftarrow>b1\<midarrow> (dom b1) \<midarrow>b2\<rightarrow>dom g" unfolding isWedge_def arrow_def
      by (smt assms(2) isPullback_def local.compositionExistence fl)
    
    hence "\<exists>!e. p1 \<cdot> e \<simeq> b1 \<and> p2 \<cdot> e \<simeq> b2"
      using assms(1) unfolding isProduct_def by auto 

    then obtain e where e: "p1 \<cdot> e \<simeq> b1 \<and> p2 \<cdot> e \<simeq> b2" by auto
    
    have equalizerProperty: "(f \<cdot> p1) \<cdot> h \<simeq> (g \<cdot> p2) \<cdot> h \<longrightarrow> (\<exists>!u. e \<cdot> u \<simeq> h)" for h
    proof (rule impI) 
      assume "(f \<cdot> p1) \<cdot> h \<simeq> (g \<cdot> p2) \<cdot> h"

      hence "f \<cdot> (p1 \<cdot> h) \<simeq> g \<cdot> (p2 \<cdot> h)" using associativity fl by metis

      hence "\<exists>!u. b1 \<cdot> u \<simeq> p1 \<cdot> h \<and> b2 \<cdot> u \<simeq> p2 \<cdot> h" 
        using assms(2) unfolding isPullback_def associativity by auto

      then obtain u where u: "b1 \<cdot> u \<simeq> p1 \<cdot> h \<and> b2 \<cdot> u \<simeq> p2 \<cdot> h" by auto

      hence "p1 \<cdot> (e \<cdot> u) \<simeq> p1 \<cdot> h \<and> p2 \<cdot> (e \<cdot> u) \<simeq> p2 \<cdot> h" using e associativity fl by auto

      hence "e \<cdot> u \<simeq> h" using uniqueArrowIntoProduct
        using assms(1) by blast

      thus "\<exists>!u. e \<cdot> u \<simeq> h"
        by (metis \<open>\<exists>!u. b1 \<cdot> u \<simeq> p1 \<cdot> h \<and> b2 \<cdot> u \<simeq> p2 \<cdot> h\<close> e local.associativity fl)
    qed

    show ?thesis apply (rule exI[of _ e], auto) 
      subgoal by (metis assms(1) e arrow_def associativity codImpliesExistence compositionExistence 
                  rightCompIdentity products_isomorphism fl) using assms(2)
      subgoal unfolding equalizerOf_def isParallelPair_def equalizes_def factorsUniquelyThrough_def 
              using equalizerProperty isPullback_def associativity compositionExistence 
                    domImpliesExistence leftCompIdentity unfolding fl 
              by (smt ExId_def e)         
      done
  qed

  lemma arrowsToPullback:
    assumes "isPullbackDiagram   \<box> \<midarrow>p2\<rightarrow> \<box>
                               p1\<down>        \<down>g
                                 \<box> \<midarrow> f \<rightarrow>\<box>"
      and "p1 \<cdot> r \<simeq> p1 \<cdot> s" and "p2 \<cdot> s \<simeq> p2 \<cdot> r"
    shows "r \<simeq> s"
  using assms unfolding isPullback_def 
    by (metis local.associativity local.codImpliesExistence fl local.compositionExistence)  

subsection \<open>Guises of pullbacks\<close>

  lemma pullbackOfMonic1:
    assumes "isPullbackDiagram   \<box> \<midarrow>p2\<rightarrow> \<box>
                               p1\<down>        \<down>g
                                 \<box> \<midarrow> f \<rightarrow>\<box>"
        and "isMonic g"
      shows "isMonic p1"
  proof - 
    have "p1 \<cdot> h \<simeq> p1 \<cdot> k \<longrightarrow> h \<simeq> k" for h k
    proof (rule impI) 
      assume "p1 \<cdot> h \<simeq> p1 \<cdot> k"
      hence "g \<cdot> (p2 \<cdot> h) \<simeq> g \<cdot> (p2 \<cdot> k)" 
        using assms(1) unfolding isPullback_def by (smt associativity fl compositionExistence)
      thus "h \<simeq> k" using assms unfolding isMonic_def isPullback_def
        by (smt \<open>p1 \<cdot> h \<simeq> p1 \<cdot> k\<close> associativity codImpliesExistence fl compositionExistence) 
    qed
  
    thus ?thesis unfolding isMonic_def using assms(1) isPullback_def fl by smt
  qed

  lemma pullbackOfMonic2:
    assumes "isPullbackDiagram   \<box> \<midarrow>p2\<rightarrow> \<box>
                               p1\<down>        \<down>g
                                 \<box> \<midarrow> f \<rightarrow>\<box>"
        and "isMonic f"
      shows "isMonic p2"
  proof - 
    have "p2 \<cdot> h \<simeq> p2 \<cdot> k \<longrightarrow> h \<simeq> k" for h k
    proof (rule impI) 
      assume "p2 \<cdot> h \<simeq> p2 \<cdot> k"
      hence "f \<cdot> (p1 \<cdot> h) \<simeq> f \<cdot> (p1 \<cdot> k)" 
        using assms(1) unfolding isPullback_def by (smt associativity fl compositionExistence)
      thus "h \<simeq> k" using assms unfolding isMonic_def isPullback_def
        by (smt \<open>p2 \<cdot> h \<simeq> p2 \<cdot> k\<close> associativity codImpliesExistence fl compositionExistence) 
    qed
  
    thus ?thesis unfolding isMonic_def using assms(1) isPullback_def by (smt fl)
  qed
 
  lemma pullbackInverseImage:
    assumes "isPullbackDiagram   \<box> \<midarrow>p2\<rightarrow> \<box>
                               i'\<down>        \<down>i
                                 \<box> \<midarrow> f \<rightarrow>\<box>"
        and "isMonic i" and "cod x \<simeq> dom f"
      shows "x memberOf i' \<longleftrightarrow> (f \<cdot> x) memberOf i"
      unfolding memberOf_def generalized_element_def using assms unfolding isPullback_def
      by (smt local.arrow_def local.associativity local.compositionExistence 
          local.leftCompIdentity local.rightCompIdentity fl)
  
  lemma pullbackIntersection:
    assumes "isPullbackDiagram   \<box> \<midarrow>p2\<rightarrow> \<box>
                               p1\<down>        \<down>j
                                 \<box> \<midarrow> i \<rightarrow>\<box>"
        and "isMonic i" and "isMonic j" defines "ij \<equiv> composition i p1"
      shows "x memberOf ij \<longleftrightarrow> x memberOf i \<and> x memberOf j"
    using assms unfolding memberOf_def ij_def isPullback_def isMonic_def fl
    by (smt local.arrow_def local.associativity local.compositionExistence local.leftCompIdentity 
        local.rightCompIdentity fl)

subsection \<open>Theorems on Pullbacks\<close>

text \<open>cf. McLarty page 45 Theorem 4.8\<close>
  lemma pullbackDoubleSquare:
    assumes "doubleSquareCommutes
              \<box> \<midarrow>k\<rightarrow> \<box> \<midarrow>m\<rightarrow> \<box>
             h\<down>      i\<down>       \<down>j
              \<box> \<midarrow>f\<rightarrow> \<box> \<midarrow>g\<rightarrow> \<box>"

        and "isPullbackDiagram  \<box> \<midarrow> m\<rightarrow> \<box>
                               i\<down>        \<down>j
                                \<box> \<midarrow> g \<rightarrow>\<box>"

      shows "isPullbackDiagram   \<box> \<midarrow> k \<rightarrow> \<box>
                                h\<down>         \<down>i
                                 \<box> \<midarrow> f \<rightarrow> \<box> 

         \<longleftrightarrow> isPullbackDiagram   \<box> \<midarrow>m \<cdot> k\<rightarrow> \<box>
                                h\<down>          \<down>j
                                 \<box> \<midarrow>g \<cdot> f\<rightarrow> \<box>"
  proof 
    assume leftPullback: "isPullbackDiagram   \<box> \<midarrow> k \<rightarrow> \<box>
                                             h\<down>         \<down>i
                                              \<box> \<midarrow> f \<rightarrow> \<box>"

    hence "(g \<cdot> f) \<cdot> s \<simeq> j \<cdot> r \<Longrightarrow> (\<exists>!u. h \<cdot> u \<simeq> s \<and> (m \<cdot> k) \<cdot> u \<simeq> r)" for r s
      proof - 
       assume asm: "(g \<cdot> f) \<cdot> s \<simeq> j \<cdot> r"
       then obtain u where u: "r \<simeq> m \<cdot> u \<and> f \<cdot> s = i \<cdot> u"
        using assms(2) unfolding isPullback_def 
        by (metis local.associativity fl)
       have "\<exists>!v. u \<simeq> k \<cdot> v \<and> s = h \<cdot> v"
        using leftPullback unfolding isPullback_def
        by (metis asm local.associativity fl local.codImpliesExistence local.compositionExistence u)
       thus ?thesis using u asm assms(1) unfolding squares 
         by (smt arrowsToPullback assms(2) local.associativity local.codImpliesExistence 
            local.compositionExistence fl)
      qed
    
    thus "isPullbackDiagram   \<box> \<midarrow>m \<cdot> k\<rightarrow> \<box>
                             h\<down>          \<down>j
                              \<box> \<midarrow>g \<cdot> f\<rightarrow> \<box>" 
      unfolding isPullback_def using assms(1) unfolding squares using fl catAx by smt

  next

    assume outerPullback: "isPullbackDiagram   \<box> \<midarrow>m \<cdot> k\<rightarrow> \<box>
                                              h\<down>          \<down>j
                                               \<box> \<midarrow>g \<cdot> f\<rightarrow> \<box>"

    have "f \<cdot> t \<simeq> i \<cdot> q \<Longrightarrow> (\<exists>!u. h \<cdot> u \<simeq> t \<and> k \<cdot> u \<simeq> q)" for q t 
    proof - 
      assume "f \<cdot> t \<simeq> i \<cdot> q"
      then obtain u where u: "t \<simeq> h \<cdot> u \<and> m \<cdot> q \<simeq> (m \<cdot> k) \<cdot> u"
        using outerPullback unfolding isPullback_def fl
        by (smt assms(1) local.RDSquareCommutes_def local.associativity 
            local.compositionExistence fl)
      thus ?thesis
        by (smt \<open>f \<cdot> t \<simeq> i \<cdot> q\<close> arrowsToPullback assms(1) assms(2) local.RDSquareCommutes_def 
            local.associativity outerPullback fl)
    qed
    
    thus "isPullbackDiagram   \<box> \<midarrow> k \<rightarrow> \<box>
                             h\<down>         \<down>i
                              \<box> \<midarrow> f \<rightarrow> \<box>"
      unfolding isPullback_def using assms(1) unfolding squares using catAx fl by smt
  qed

  text \<open>Note that this last proof actually needs less symbols than the proof in McLarty e.g. 
        McLarty introduces w in the ==> direction.\<close>

end


end