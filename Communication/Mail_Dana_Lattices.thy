theory Mail_Dana_Lattices imports "../CategoryExamples/CategoryOfPosets" 
                                  "../BinaryProductCategory"

begin

text \<open>Defining bounds and semilattices is straightforward:\<close>

subsection \<open>Definition of bounds and semilattices\<close>

  context poset begin
    definition upper_bound :: "'a \<Rightarrow> 'a set \<Rightarrow> bool" (infix "upper'_bound'_of" 50) where
      "u upper_bound_of S \<equiv> \<forall>s\<in>S. s \<preceq> u"
      
    definition lower_bound :: "'a \<Rightarrow> 'a set \<Rightarrow> bool" (infix "lower'_bound'_of" 50) where
      "l lower_bound_of S \<equiv> \<forall>s\<in>S. l \<preceq> s"
    
    definition least_upper_bound :: "'a \<Rightarrow> 'a set \<Rightarrow> bool" 
      (infix "least'_upper'_bound'_of" 50) where
      "u least_upper_bound_of S \<equiv> u upper_bound_of S \<and> (\<forall>x. x upper_bound_of S \<longrightarrow> u \<preceq> x)"
    
    definition greatest_lower_bound :: "'a \<Rightarrow> 'a set \<Rightarrow> bool" 
      (infix "greatest'_lower'_bound'_of" 50) where
      "l greatest_lower_bound_of S \<equiv> l lower_bound_of S \<and> (\<forall>x. x lower_bound_of S \<longrightarrow> x \<preceq> l)"
  end

  locale join_semilattice = poset +
    fixes join :: "'a \<Rightarrow> 'a \<Rightarrow> 'a" (infix "\<^bold>\<or>" 120)
    assumes join_property: "a \<^bold>\<or> b least_upper_bound_of {a, b}"


text \<open>I proved monotonicity to test the implementation:\<close>

  lemma (in join_semilattice) monotonicity: "a\<^sub>1 \<preceq> a\<^sub>2 \<Longrightarrow> b\<^sub>1 \<preceq> b\<^sub>2 \<Longrightarrow> a\<^sub>1 \<^bold>\<or> b\<^sub>1 \<preceq> a\<^sub>2 \<^bold>\<or> b\<^sub>2" 
    using join_property unfolding least_upper_bound_def upper_bound_def 
    by (smt doubleton_eq_iff insertE insertI1 singleton_iff transitivity)
  
  locale meet_semilattice = poset +
    fixes meet :: "'a \<Rightarrow> 'a \<Rightarrow> 'a" (infix "\<^bold>\<and>" 120)
    assumes meet_property: "a \<^bold>\<and> b greatest_lower_bound_of {a, b}"
  begin
  
    lemma monotonicity: "a\<^sub>1 \<preceq> a\<^sub>2 \<Longrightarrow> b\<^sub>1 \<preceq> b\<^sub>2 \<Longrightarrow> a\<^sub>1 \<^bold>\<and> b\<^sub>1 \<preceq> a\<^sub>2 \<^bold>\<and> b\<^sub>2" 
      using meet_property unfolding greatest_lower_bound_def lower_bound_def 
      by (smt doubleton_eq_iff insertE insertI1 singleton_iff transitivity)
  
  end


subsection \<open>Lattices\<close>

text \<open>With this at hand one can define a lattice and then state the distributive law.\<close>

  locale lattice = join_semilattice + meet_semilattice

  lemma (in lattice) distributive_law: "x \<^bold>\<and> (y \<^bold>\<or> z) = (x \<^bold>\<and> y) \<^bold>\<or> (x \<^bold>\<and> z)"
    nitpick (* only takes a few milliseconds *)
    oops


text \<open>Isabelles model finding tool Nitpick finds a counter-example (which I expected, as this
      situation is not too involved). It picks the N_5 lattice but I'm sure one could tweak it
      to also find the diamond lattice M_3. 

      By proving that lattices form categories one can show that this counterexample holds on
      the category theoretical level for the product/coproduct distributive law, too.\<close>


subsection \<open>Lattices form a category with binary products\<close>

text \<open>I thought that similar to the situation with sets it is not trivial to show that posets form
      a binary product category. The reason for these issue is that one needs to encode all relevant
      information in the product. I knew how to fix that and finished the proof last night. 

      However, I just realized that a slight change in the definition of a category with binary 
      products can make life a lot easier. Concretely, the projection maps currently take one single
      arrow of the form A\<times>B as an argument. But this arrow could also be C\<times>D for some other C, D \<noteq>
      A, B. For this reason there might be several projection maps and thus, more information is 
      required. However, by simply changing the signature of the projection map from taking one 
      single arrow A\<times>B to taking two arrows A, B one can get rid of this issue. I will do this and
      rework the affected code (which might take quite some time). Then, the proof below will become 
      somewhat easier. 

      But for now, the old approach based on tree structures: \<close>

  datatype 'a tree = Root (val: 'a)
                   | Node (left:  "'a tree")
                          (right: "'a tree")         

text \<open>An arrow in the category is then not much more than a tuple (here the entries are suggestively
      named ldom and lcod. \<close>

  datatype 'a latticemap = Latticemap (ldom: "'a tree") (lcod : "'a tree") | Inexistent ("*")

  context lattice
  begin

text \<open>This function allows to get from the tree  \<close>
(*             __
 *            /  \
 *           |    C
 *          / \
 *         A   B
 *)
text \<open>to the meet (A \<and> B) \<and> C that it represents. \<close>

  fun eval :: "'a tree \<Rightarrow> 'a" where
    "eval (Root a) = a" |
    "eval (Node l r) = eval l \<^bold>\<and> eval r"

  lemma eval_left_rel: "\<not> is_Root P \<Longrightarrow> eval P \<preceq> eval (left P)"
    apply (induction P, auto) 
    using greatest_lower_bound_def lower_bound_def meet_property by auto   

  lemma eval_right_rel: "\<not> is_Root P \<Longrightarrow> eval P \<preceq> eval (right P)"
    apply (induction P, auto) 
    using greatest_lower_bound_def lower_bound_def meet_property by auto   

text \<open>Once these concepts are settled, most definitions are straightforward. The following lines 
      just remove some old notation\<close>

  no_notation posetmap_existence ("E")
  no_notation inexistent_element ("*")
  no_notation posetmap_identity ("Id")
  no_notation posetmap_domain ("dom")
  no_notation posetmap_codomain ("cod")
  no_notation posetmap_composition (infix "\<cdot>" 110)
  
text \<open>The definition of existence for a latticemap:\<close>
  definition latticemap_existence :: "'a latticemap \<Rightarrow> bool" ("E") where
    "E (f::'a latticemap) \<equiv> eval (ldom f) \<preceq> eval (lcod f) \<and> f \<noteq> *"

  interpretation free_logic latticemap_existence by (unfold_locales)

  notation KlEq (infixr "\<cong>" 56)
  notation ExId (infixr "\<simeq>" 56) 

  lemma inexistent_element[simp]: "\<not> (E * )"
    by (simp add: latticemap_existence_def)

subsection \<open>Defining domain, codomain and composition\<close>

  definition latticemap_identity :: "'a tree \<Rightarrow> 'a latticemap" ("Id") where
    "Id T = Latticemap T T"
  
  definition latticemap_domain :: "'a latticemap \<Rightarrow> 'a latticemap" ("dom") where
    "dom f \<equiv> if E f 
              then Id (ldom f) 
              else *" 
  
  definition latticemap_codomain :: "'a latticemap \<Rightarrow> 'a latticemap" ("cod") where
    "cod f \<equiv> if E f 
              then Id (lcod f) 
              else *" 

  text \<open>Composable maps and composition:\<close>
  definition lattice_composable :: "'a latticemap \<Rightarrow> 'a latticemap \<Rightarrow> bool" where 
    "lattice_composable f g \<equiv> E f \<and> E g \<and> lcod g = ldom f"

  definition latticemap_composition :: "'a latticemap \<Rightarrow> 'a latticemap \<Rightarrow> 'a latticemap" 
    (infix "\<cdot>" 110) where 
    "f \<cdot> g \<equiv> if lattice_composable f g
              then Latticemap (ldom g) (lcod f)
              else *"

text \<open>Some simple lemmas that will later be used in the interpretation proof.\<close>

  lemma composableExistence:
    "E (f \<cdot> g) \<longleftrightarrow> lattice_composable f g" 
    unfolding latticemap_composition_def apply (cases "lattice_composable f g", simp_all)
    unfolding latticemap_existence_def lattice_composable_def apply simp 
    by (metis transitivity) 

  lemma Id_existence: "E (Id x)"
    by (simp add: latticemap_existence_def latticemap_identity_def reflexivity)


section \<open>The interpretation proof\<close>
text \<open>First, it is proved that the structures defined above form an (ordinary) category. This then 
      allows to use all definitions and lemmas that are defined for categories. \<close>

  interpretation lattice_cat: category latticemap_existence
                                   latticemap_domain 
                                   latticemap_codomain 
                                   latticemap_composition 
                                   Inexistent 
  proof (unfold_locales) 
    show "(f \<cdot> g) \<cdot> h \<cong> f \<cdot> (g \<cdot> h)" for f g h :: "'a latticemap"
      using KlEq_def composableExistence latticemap_composition_def lattice_composable_def by auto
  
    show "x \<cdot> dom x \<cong> x" for x :: "'a latticemap" 
      using Id_existence KlEq_def latticemap_composition_def latticemap_domain_def 
            latticemap_existence_def latticemap_identity_def lattice_composable_def by auto
    
    show "cod x \<cdot> x \<cong> x" for x :: "'a latticemap"
      using Id_existence KlEq_def latticemap_composition_def latticemap_codomain_def 
            latticemap_existence_def latticemap_identity_def lattice_composable_def by auto
  
    show "E (x \<cdot> y) \<longleftrightarrow> dom x \<simeq> cod y" for x y :: "'a latticemap" unfolding fl
      using Id_existence composableExistence latticemap_codomain_def latticemap_domain_def 
            latticemap_identity_def lattice_composable_def by auto
  
    show "E (dom x) \<longrightarrow> E x" for x :: "'a latticemap" 
      by (simp add: latticemap_domain_def latticemap_identity_def)
  
    show "E (cod x) \<longrightarrow> E x" for x :: "'a latticemap" 
      by (simp add: latticemap_codomain_def latticemap_identity_def)
  
    show "\<not> E *" using inexistent_element by auto
  
  qed

text \<open>We introduce notation for the properties that are defined now: \<close>

  notation lattice_cat.isType ("isType")
  notation lattice_cat.arrow ("_:_\<rightarrow>_" [120,120,120] 119) 
  notation lattice_cat.isWedge ("_\<leftarrow>_\<midarrow> _ \<midarrow>_\<rightarrow>_" [120,120,120,120,120] 120)
  notation lattice_cat.isProduct ("isProduct _\<leftarrow>_\<midarrow> _ \<midarrow>_\<rightarrow>_" [120,120,120,120,120] 120) 

text \<open>Also note when the such defined lattice map datatype is a type in the category sense:\<close>

  lemma latticemap_isType: "isType A \<longleftrightarrow> (A = Id (ldom A))"
    by (metis Id_existence free_logic.ExId_def lattice.latticemap_domain_def lattice_cat.isType_def 
        local.lattice_axioms)

text \<open>With this one can define all the structures needed for binary products:\<close>
  
  definition latticemap_product :: "'a latticemap \<Rightarrow> 'a latticemap \<Rightarrow> 'a latticemap" 
    (infix "\<^bold>\<times>" 120) where
    "A \<^bold>\<times> B \<equiv> if (isType A \<and> isType B) 
              then Id (Node (ldom A) (ldom B)) 
              else *"
  
  definition latticemap_projection1 :: "'a latticemap \<Rightarrow> 'a latticemap" ("\<pi>\<^sub>1") where
    "\<pi>\<^sub>1 P \<equiv> if (\<exists>A B. P \<simeq> A \<^bold>\<times> B)
            then Latticemap (ldom P) (left (ldom P))
            else *"
  
  definition latticemap_projection2 :: "'a latticemap \<Rightarrow> 'a latticemap" ("\<pi>\<^sub>2") where
    "\<pi>\<^sub>2 P \<equiv> if (\<exists>A B. P \<simeq> A \<^bold>\<times> B)
            then Latticemap (ldom P) (right (ldom P))
            else *"
  

subsection \<open>The lattice category has binary products\<close>

interpretation binaryProductCategory latticemap_existence
                                     latticemap_domain latticemap_codomain 
                                     latticemap_composition Inexistent
                                     latticemap_product latticemap_projection1 latticemap_projection2
proof(unfold_locales)

text \<open>Isabelle remembers that lattices form a category. It is left to show that the additional 
      structures fulfill the product properties. The following three statements are quite simple: \<close>

  show "E (A \<^bold>\<times> B) \<longrightarrow> isType A \<and> isType B" for A B :: "'a latticemap" 
    using latticemap_product_def by auto
  show "E (\<pi>\<^sub>1 A) \<longrightarrow> (\<exists>B C. A \<simeq> B \<^bold>\<times> C)" for A :: "'a latticemap" 
    by (simp add: latticemap_projection1_def)
  show "E (\<pi>\<^sub>2 A) \<longrightarrow> (\<exists>B C. A \<simeq> B \<^bold>\<times> C)" for A :: "'a latticemap"
    by (simp add: latticemap_projection2_def)

text \<open>This is the key step:\<close>

  show "isType A \<and> isType B 
        \<longrightarrow>  isProduct A \<leftarrow>\<pi>\<^sub>1(A\<^bold>\<times>B)\<midarrow> A\<^bold>\<times>B \<midarrow>\<pi>\<^sub>2(A\<^bold>\<times>B)\<rightarrow> B" for A B :: "'a latticemap" 
  proof (auto)

    assume "isType A" and "isType B"

text \<open>The proof starts by deriving a lot of simple properties which will serve useful later\<close>

    hence "E A" and "E B" 
      by (simp_all add: lattice_cat.typesExist) 
    hence "E (A \<^bold>\<times> B)" 
      by (metis Id_existence \<open>isType A\<close> \<open>isType B\<close> lattice.latticemap_product_def 
          local.lattice_axioms)
    
    have factors_exist: "(\<exists>Aa Ba. A \<^bold>\<times> B \<simeq> Aa \<^bold>\<times> Ba) = True"
      using \<open>E (A \<^bold>\<times> B)\<close> unfolding fl by auto
  
    have not_a_root: "\<not> is_Root (ldom (A \<^bold>\<times> B))"
      unfolding latticemap_product_def using \<open>isType A\<close> \<open>isType B\<close> latticemap_identity_def by simp

    have ldom_product: "ldom (A \<^bold>\<times> B) = Node (ldom A) (ldom B)"
      by (simp add: \<open>isType A\<close> \<open>isType B\<close> latticemap_identity_def latticemap_product_def)

    have "E (\<pi>\<^sub>1 (A \<^bold>\<times> B))" and "E (\<pi>\<^sub>2 (A \<^bold>\<times> B))" 
      unfolding latticemap_projection1_def latticemap_projection2_def factors_exist 
      unfolding latticemap_existence_def
      using eval_left_rel eval_right_rel by (simp_all add: not_a_root)

text \<open>One can then show that the arrows in the diagram are correct: \<close>

    have isWedge: "A\<leftarrow>(\<pi>\<^sub>1 (A \<^bold>\<times> B))\<midarrow>(A \<^bold>\<times> B)\<midarrow>(\<pi>\<^sub>2 (A \<^bold>\<times> B))\<rightarrow>B"
      by (smt ExId_def \<open>E (\<pi>\<^sub>1 (A \<^bold>\<times> B))\<close> \<open>E (\<pi>\<^sub>2 (A \<^bold>\<times> B))\<close> \<open>isType A\<close> \<open>isType B\<close> 
          lattice_cat.arrow_def lattice_cat.isType_def lattice_cat.isWedge_def latticemap.sel(1-2) 
          latticemap_codomain_def latticemap_domain_def latticemap_isType latticemap_product_def 
          latticemap_projection1_def latticemap_projection2_def ldom_product tree.sel(2-3))


text \<open>The core part is the product property, i.e. the unique existence of a map into the product
      for given maps h, k: \<close>

    have product_property: "A \<leftarrow>h\<midarrow> T \<midarrow>k\<rightarrow> B 
                            \<Longrightarrow> (\<exists>!u. \<pi>\<^sub>1 (A \<^bold>\<times> B) \<cdot> u \<simeq> h \<and> \<pi>\<^sub>2 (A \<^bold>\<times> B) \<cdot> u \<simeq> k)" for h T k
    proof -
      assume "A \<leftarrow>h\<midarrow> T \<midarrow>k\<rightarrow> B "
      hence "E h" and "E k" and "h:T\<rightarrow> A" and "k:T\<rightarrow>B"
        unfolding lattice_cat.isWedge_def lattice_cat.arrow_def lattice_cat.isType_def 
        using catAx fl by blast+
      
      hence "lcod T = ldom h" and "lcod T = ldom k"
        by (smt lattice_cat.arrow_def latticemap.sel latticemap_domain_def latticemap_identity_def 
            fl)+

      have "isType T"
        using \<open>k:T\<rightarrow>B\<close> lattice_cat.arrowImpliesIsType by auto

text \<open>Here the map into the product is defined: \<close>

      define u where "u \<equiv> Latticemap (lcod T) (ldom (A \<^bold>\<times> B))"

text \<open>Then the categorical properties of A \<leftarrow>h\<midarrow> T \<midarrow>k\<rightarrow> B are used to derive relations between the
      'objects' (meaning elements of the poset) underlying A, T and B \<close>

      have "eval (lcod T) \<preceq> eval (ldom A)" 
        using ExId_def \<open>h:T\<rightarrow>A\<close> \<open>isType A\<close> \<open>lcod T = ldom h\<close> 
              lattice.latticemap_codomain_def lattice.latticemap_identity_def lattice_cat.arrow_def 
              latticemap.sel(2) latticemap_existence_def latticemap_isType local.lattice_axioms 
        by smt

      moreover have "eval (lcod T) \<preceq> eval (ldom B)"
        by (metis ExId_def \<open>isType B\<close> \<open>k:T\<rightarrow>B\<close> \<open>lcod T = ldom k\<close> lattice.latticemap_codomain_def 
            lattice.latticemap_identity_def lattice_cat.arrow_def latticemap.sel(2) 
            latticemap_existence_def latticemap_isType local.lattice_axioms)

text \<open>With this one can show that the object underlying T is a lower bound\<close>

      ultimately have "eval (lcod T) lower_bound_of {eval (ldom B), eval (ldom A)}"
        unfolding lower_bound_def by auto

text \<open>which in turn implies that the constructed arrow must consist: \<close>

      hence "E u" 
        by (metis eval.simps(2) insert_commute latticemap.distinct(1) latticemap.sel(1-2) 
            latticemap_existence_def ldom_product meet_property poset.greatest_lower_bound_def 
            poset_axioms u_def)

text \<open>Then one only has to outline the rest of the proof, namely that composing with that arrow u
      yields the desired equalities. Automated theorem proving can figure out the details. \<close>
 
      hence u_map: "u:T\<rightarrow>(A \<^bold>\<times> B)" 
        by (metis ExId_def \<open>E h\<close> \<open>h:T\<rightarrow>A\<close> \<open>isType A\<close> \<open>isType B\<close> \<open>lcod T = ldom h\<close> category.arrow_def 
            factors_exist lattice.latticemap_codomain_def lattice.latticemap_domain_def 
            lattice.latticemap_product_def lattice_cat.category_axioms latticemap.sel(1) 
            latticemap.sel(2) ldom_product local.lattice_axioms u_def)

      have comp1: "\<pi>\<^sub>1 (A \<^bold>\<times> B) \<cdot> u \<simeq> h" 
        by (smt \<open>E (\<pi>\<^sub>1 (A \<^bold>\<times> B))\<close> \<open>E u\<close> \<open>h:T\<rightarrow>A\<close> \<open>lcod T = ldom h\<close> \<open>lcod T = ldom k\<close> 
            free_logic.ExId_def lattice.latticemap_codomain_def lattice.latticemap_composition_def 
            lattice.latticemap_identity_def lattice_cat.arrow_def latticemap.exhaust_sel 
            latticemap.sel(1) latticemap.sel(2) latticemap_existence_def latticemap_product_def 
            latticemap_projection1_def lattice_composable_def local.lattice_axioms tree.sel(2) u_def)

      have comp2: "\<pi>\<^sub>2 (A \<^bold>\<times> B) \<cdot> u \<simeq> k"
        by (smt \<open>E (\<pi>\<^sub>2 (A \<^bold>\<times> B))\<close> \<open>E u\<close> \<open>k:T\<rightarrow>B\<close> \<open>lcod T = ldom h\<close> \<open>lcod T = ldom k\<close> 
            free_logic.ExId_def lattice.latticemap_codomain_def lattice.latticemap_composition_def 
            lattice.latticemap_identity_def lattice_cat.arrow_def latticemap.exhaust_sel 
            latticemap.sel(1) latticemap.sel(2) latticemap_existence_def latticemap_product_def 
            latticemap_projection2_def lattice_composable_def local.lattice_axioms tree.sel(3) u_def)

      have uniqueness: "\<pi>\<^sub>1 (A \<^bold>\<times> B) \<cdot> v \<simeq> h \<Longrightarrow> \<pi>\<^sub>2 (A \<^bold>\<times> B) \<cdot> v \<simeq> k \<Longrightarrow> u = v" for v 
        by (metis ExId_def comp1 composableExistence 
            lattice_cat.inexistentElement lattice_composable_def latticemap.expand 
            latticemap.sel(1) latticemap_composition_def)
 
      show "(\<exists>!u. \<pi>\<^sub>1 (A \<^bold>\<times> B) \<cdot> u \<simeq> h \<and> \<pi>\<^sub>2 (A \<^bold>\<times> B) \<cdot> u \<simeq> k)" 
        using comp1 comp2 uniqueness by auto
    qed

text \<open>One can finally conclude the product property\<close>

    show "isProduct A \<leftarrow>\<pi>\<^sub>1 (A \<^bold>\<times> B)\<midarrow> A \<^bold>\<times> B \<midarrow>\<pi>\<^sub>2 (A \<^bold>\<times> B)\<rightarrow> B" 
      unfolding lattice_cat.isProduct_def using isWedge product_property by auto 
  qed

qed

text \<open>which finishes the proof. \<close>



end