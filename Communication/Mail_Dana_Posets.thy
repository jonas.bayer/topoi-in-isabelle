theory Mail_Dana_Posets imports "../Category" HOL.Order_Relation 

begin


section \<open>Posets\<close>

text \<open>After changing some technicalities of the formalization of free logic that Lucca gave me, it 
      was possible to formalize that Posets form a category. This is achieved via the following
      datatype that is meant to represents arrows in the category of Posets. A case for inexistent
      elements is added for convenience.\<close>

datatype 'a posetmap = Posetmap (pdom: 'a) (pcod: 'a) | Inexistent

text \<open>We now fix a relation "\<preceq>" and assume that it forms a poset (on the UNIV of its type). In 
      Isabelle this looks like the following:\<close>

locale poset =
  fixes
    relation :: "'a \<Rightarrow> 'a \<Rightarrow> bool" (infix "\<preceq>" 110)
  assumes 
    reflexivity:  "a \<preceq> a" and
    antisymmetry: "a \<preceq> b \<and> b \<preceq> a \<Longrightarrow> a = b" and
    transitivity: "a \<preceq> b \<and> b \<preceq> c \<Longrightarrow> a \<preceq> c" 
begin

text \<open>We define what existence means for a poset:\<close>

definition inexistent_element :: "'a posetmap" ("*") where
  "* \<equiv> Inexistent"

definition posetmap_existence :: "'a posetmap \<Rightarrow> bool" ("E") where
  "E (f::'a posetmap) \<equiv> pdom f \<preceq> pcod f \<and> f \<noteq> *"

text \<open>The following three commands serve a technical purpose allowing us to later use \<cong> for
      Kleene equality\<close>
interpretation free_logic posetmap_existence by (unfold_locales)
notation KlEq (infixr "\<cong>" 56)
notation ExId (infixr "\<simeq>" 56) 

text \<open>With this at hand we can define what domain, codomain and composition are in the category
      of posets. \<close>

definition posetmap_identity :: "'a \<Rightarrow> 'a posetmap" ("Id") where
  "Id A = Posetmap A A"

definition posetmap_domain :: "'a posetmap \<Rightarrow> 'a posetmap" ("dom") where
  "dom P \<equiv> if E P
            then Id (pdom P)
            else *"

definition posetmap_codomain :: "'a posetmap \<Rightarrow> 'a posetmap" ("cod") where
  "cod P \<equiv> if E P 
            then Id (pcod P) 
            else *"

definition composable :: "'a posetmap \<Rightarrow> 'a posetmap \<Rightarrow> bool" where
  "composable P Q \<equiv> pdom P = pcod Q \<and> E P \<and> E Q"

definition posetmap_composition :: "'a posetmap \<Rightarrow> 'a posetmap \<Rightarrow> 'a posetmap" (infix "\<cdot>" 110) where
  "P \<cdot> Q \<equiv> if composable P Q 
           then Posetmap (pdom Q) (pcod P)
           else *"

text \<open>Finally, we prove that this notion of existence, domain, codomain and composition indeed 
      satisfies the axioms of a category:\<close>

interpretation category posetmap_existence 
                        posetmap_domain posetmap_codomain 
                        posetmap_composition inexistent_element
proof (unfold_locales)

text \<open>Composition with the domain: \<close>
  have "E x \<Longrightarrow> E (Id (pdom x))" for x 
    by (simp add: inexistent_element_def posetmap_existence_def posetmap_identity_def reflexivity)  
  hence compdom: "E x \<Longrightarrow> composable x (dom x)" for x 
    unfolding composable_def posetmap_domain_def posetmap_identity_def by auto
  thus "x \<cdot> dom x \<cong> x" for x 
    unfolding fl
    by (simp add: composable_def inexistent_element_def posetmap_composition_def posetmap_domain_def 
        posetmap_existence_def posetmap_identity_def) 

text \<open>Composition with the codomain: \<close>
  have "E x \<Longrightarrow> E (Id (pcod x))" for x 
    by (simp add: inexistent_element_def posetmap_existence_def posetmap_identity_def reflexivity)  
  hence compcod: "E x \<Longrightarrow> composable (cod x) x" for x 
    unfolding composable_def posetmap_codomain_def posetmap_identity_def by auto
  thus "cod x \<cdot> x \<cong> x" for x
    unfolding fl
    by (simp add: composable_def inexistent_element_def posetmap_composition_def 
        posetmap_codomain_def posetmap_existence_def posetmap_identity_def) 

text \<open>Existence of composite: \<close>
  show "E (x \<cdot> y) = dom x \<simeq> cod y" for x y unfolding fl
    by (smt compcod compdom composable_def inexistent_element_def posetmap.discI posetmap.sel(1-2) 
        posetmap_codomain_def posetmap_composition_def posetmap_domain_def posetmap_existence_def 
        transitivity)

text \<open>Inexistent element:\<close>
  show inexistent_element: "\<not> E *" unfolding posetmap_existence_def by auto

text \<open>Domain implies existence: \<close>
  show "E (dom x) \<longrightarrow> E x" for x 
    by (simp add: inexistent_element posetmap_domain_def)

text \<open>Codomain implies existence: \<close>
  show "E (cod x) \<longrightarrow> E x" for x 
    by (simp add: inexistent_element posetmap_codomain_def)

text \<open>Associativity: \<close>
  show "(x \<cdot> y) \<cdot> z \<cong> x \<cdot> (y \<cdot> z)" for x y z 
    unfolding posetmap_composition_def fl
    by (smt composable_def inexistent_element_def posetmap.discI posetmap.sel(1) posetmap.sel(2) 
        posetmap_existence_def transitivity)

qed


end