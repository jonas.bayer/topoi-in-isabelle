theory Mail_Dana_09_06_2021 imports "../Category" HOL.Order_Relation 

begin

text \<open>In your previous mails you asked about the following topics, which I explored in Isabelle: 
      - Posets from the context of Category theory
      - The distributive law of \<times> and \<squnion> 
      - Axiomatics for products (and coproducts) 
      - How to best formalize functors

      This document presents my current progress on all of these different matters. \<close>

section \<open>Posets\<close>


datatype 'a posetmap = Posetmap (pdom: 'a) (pcod: 'a) | Inexistent

locale poset =
  fixes
    set :: "'a set" and
    relation :: "'a \<Rightarrow> 'a \<Rightarrow> bool" (infix "\<preceq>" 110)
  assumes 
    reflexivity:  "a \<preceq> a" and
    antisymmetry: "a \<preceq> b \<and> b \<preceq> a \<Longrightarrow> a = b" and
    transitivity: "a \<preceq> b \<and> b \<preceq> c \<Longrightarrow> a \<preceq> c" 
begin

definition inexistent_element :: "'a posetmap" ("*") where
  "* \<equiv> Inexistent"

definition posetmap_existence :: "'a posetmap \<Rightarrow> bool" ("E") where
  "E (f::'a posetmap) \<equiv> pdom f \<preceq> pcod f \<and> f \<noteq> *"

interpretation free_logic posetmap_existence by (unfold_locales)

notation KlEq (infixr "\<cong>" 56)
notation ExId (infixr "\<simeq>" 56) 

definition posetmap_identity :: "'a \<Rightarrow> 'a posetmap" ("Id") where
  "Id A = Posetmap A A"

definition posetmap_domain :: "'a posetmap \<Rightarrow> 'a posetmap" ("dom") where
  "dom P \<equiv> if E P
            then Id (pdom P)
            else *"

definition posetmap_codomain :: "'a posetmap \<Rightarrow> 'a posetmap" ("cod") where
  "cod P \<equiv> if E P 
            then Id (pcod P) 
            else *"

definition composable :: "'a posetmap \<Rightarrow> 'a posetmap \<Rightarrow> bool" where
  "composable P Q \<equiv> pdom P = pcod Q \<and> E P \<and> E Q"

definition posetmap_composition :: "'a posetmap \<Rightarrow> 'a posetmap \<Rightarrow> 'a posetmap" (infix "\<cdot>" 110) where
  "P \<cdot> Q \<equiv> if composable P Q 
           then Posetmap (pdom Q) (pcod P)
           else *"

interpretation category posetmap_existence 
                        posetmap_domain posetmap_codomain 
                        posetmap_composition inexistent_element
proof (unfold_locales)
  show inexistent_element: "\<not> E *" unfolding posetmap_existence_def by auto


  have "E x \<Longrightarrow> E (Id (pdom x))" for x 
    by (simp add: inexistent_element_def posetmap_existence_def posetmap_identity_def reflexivity)  
  hence compdom: "E x \<Longrightarrow> composable x (dom x)" for x 
    unfolding composable_def posetmap_domain_def posetmap_identity_def by auto
  thus "x \<cdot> dom x \<cong> x" for x 
    unfolding fl
    by (simp add: composable_def inexistent_element_def posetmap_composition_def posetmap_domain_def 
        posetmap_existence_def posetmap_identity_def) 

  have "E x \<Longrightarrow> E (Id (pcod x))" for x 
    by (simp add: inexistent_element_def posetmap_existence_def posetmap_identity_def reflexivity)  
  hence compcod: "E x \<Longrightarrow> composable (cod x) x" for x 
    unfolding composable_def posetmap_codomain_def posetmap_identity_def by auto
  thus "cod x \<cdot> x \<cong> x" for x
    unfolding fl
    by (simp add: composable_def inexistent_element_def posetmap_composition_def 
        posetmap_codomain_def posetmap_existence_def posetmap_identity_def) 

  show "E (x \<cdot> y) = dom x \<simeq> cod y" for x y unfolding fl
    by (smt compcod compdom composable_def inexistent_element_def posetmap.discI posetmap.sel(1-2) 
        posetmap_codomain_def posetmap_composition_def posetmap_domain_def posetmap_existence_def 
        transitivity)

  show "E (dom x) \<longrightarrow> E x" for x 
    by (simp add: inexistent_element posetmap_domain_def)

  show "E (cod x) \<longrightarrow> E x" for x 
    by (simp add: inexistent_element posetmap_codomain_def)

  show "(x \<cdot> y) \<cdot> z \<cong> x \<cdot> (y \<cdot> z)" for x y z 
    unfolding posetmap_composition_def fl
    by (smt composable_def inexistent_element_def posetmap.discI posetmap.sel(1) posetmap.sel(2) 
        posetmap_existence_def transitivity)

qed


section \<open>The distributive law with \<times> and \<squnion>\<close>

section \<open>Axiomatics for products\<close>

section \<open>Functors and the Category of Categories\<close>



end