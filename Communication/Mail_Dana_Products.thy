theory Mail_Dana_Products imports "../Category" 

begin

section \<open>On axiomatics for products and coproducts\<close>

text \<open>I restricted to investigating products as, of course, it is easy to create the dual version 
      from any axiom/theorem simply by inverting arrows. The current main definition for products
      that I use is the following:  \<close>

context category begin

  definition isWedge ("_\<leftarrow>_\<midarrow> _ \<midarrow>_\<rightarrow>_" [120,120,120,120,120] 120) where
      "A \<leftarrow>f\<midarrow> C \<midarrow>g\<rightarrow> B \<equiv> f:C\<rightarrow>A \<and> g:C\<rightarrow>B"
  
  definition isProduct :: "'a \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> bool" 
      ("isProduct _\<leftarrow>_\<midarrow> _ \<midarrow>_\<rightarrow>_" [120,120,120,120,120] 120) where
      "isProduct A \<leftarrow>p1\<midarrow> P \<midarrow>p2\<rightarrow> B \<equiv> A \<leftarrow>p1\<midarrow> P \<midarrow>p2\<rightarrow> B \<and> 
                             (\<forall>h T k. A \<leftarrow>h\<midarrow> T \<midarrow>k\<rightarrow> B \<longrightarrow> (\<exists>!u. p1\<cdot>u \<simeq> h \<and> p2\<cdot>u \<simeq> k))"

end

text \<open>From this, it is possible to define what it means for a category to have binary products:\<close>

locale binaryProductCategory = category +
    fixes product :: "'a \<Rightarrow> 'a \<Rightarrow> 'a" (infix "\<^bold>\<times>" 120) and
          projection1 :: "'a \<Rightarrow> 'a" ("\<pi>\<^sub>1") 
          (* Takes a product and returns the morphism from the product to its first factor *) and
          projection2 :: "'a \<Rightarrow> 'a" ("\<pi>\<^sub>2")
          (* Takes a product and returns the morphism from the product to its second factor *)
  assumes 
          productOfTypes:  "isType A \<and> isType B \<longrightarrow> 
                            isProduct A \<leftarrow>\<pi>\<^sub>1 (A \<^bold>\<times> B)\<midarrow> (A \<^bold>\<times> B) \<midarrow>\<pi>\<^sub>2 (A \<^bold>\<times> B)\<rightarrow> B" and
          
          productExistence:     "E (A \<^bold>\<times> B) \<longrightarrow> (isType A \<and> isType B)" and
          projection1Existence: "E (\<pi>\<^sub>1 A) \<longrightarrow> (\<exists>B C. A \<simeq> B \<^bold>\<times> C)" and
          projection2Existence: "E (\<pi>\<^sub>2 A) \<longrightarrow> (\<exists>B C. A \<simeq> B \<^bold>\<times> C)"

text \<open>Unfortunately, these axioms have a very different flavour compared to the current 
      formalization of a category. The need to refer to types which is not natural and moreover,
      the axioms projection1Existence and projection2Existence seem quite artificial.

      Still, they seem to be correct as I was able to formalize that sets do not only form a 
      category but also have binary products. This required quite some effort, however, the issues
      were more on a technical side and not so much related to the particular choice of axioms.
  
      There is another positive aspect of this set of axioms: Namely, the axioms are as weak as I 
      think is possible and e.g. do not require that the product of arrows exists etc. Starting from 
      these axioms, the latter becomes a theorem \<close>

(*
theorem product_of_arrows: 
  assumes "E f" and "E g"
    shows "(f \<^bold>\<times>\<^sub>a g):(dom f)\<^bold>\<times>(dom g)\<rightarrow>(cod f)\<^bold>\<times>(cod g)" 
      and "commutes \<box> \<leftarrow>\<pi>\<^sub>1\<midarrow> ((dom f) \<^bold>\<times> (dom g)) \<midarrow>\<pi>\<^sub>2\<rightarrow> \<box>
                    \<down>f                \<down>(f \<^bold>\<times>\<^sub>a g)        \<down>g
                    \<box> \<leftarrow>\<pi>\<^sub>1\<midarrow> ((cod f) \<^bold>\<times> (cod g)) \<midarrow>\<pi>\<^sub>2\<rightarrow> \<box>" 
*)

text \<open>whose somewhat technical proof is omitted here. 

      A more natural, arrow-centered formalization which does not refer to types and defines the 
      product for any two existing f, g will require additional axioms. The above theorem should
      then become an axiom. 
      
      I found it hard to come up with such axioms. So what I did was to have a look at the more 
      general question: How can one define limits in a natural way? Simply entering the respective
      diagram for products should then yield the axioms for the particular case. 

      The formalization of limits in most general form is somewhat laborious and requires a lot of
      preliminary definitions (stating that a given set of arrows is indeed a commutative diagram 
      takes some work). For this reason, I will leave that part out here. Moreover, I have not 
      tested my current implementation of limits sufficiently to be sure that there are no more 
      errors.

      Still, investigating limits has already given me some inspiration for the particular case of 
      products. I currently do not have a complete set of axioms that defines binary products more 
      elegantly, however I found some potential candidates which could eventually combine into
      an axiom set. This is still very much work in progress so I can't present a formal 
      description.  \<close>

section \<open>Exploring the distributive law\<close>

text \<open>Another topic that I investigated was the distributive law for product and coproduct. The
      formal code is fully based on the first axiom set for products (using types) stated above. 
      First, we define coproducts in analogy to the definition of products: \<close>

context category
begin

  definition isCowedge ("_\<midarrow>_\<rightarrow>_\<leftarrow>_\<midarrow>_" [120,120,120,120,120] 120) where
    "A \<midarrow>f\<rightarrow> C \<leftarrow>g\<midarrow> B \<equiv> f:A\<rightarrow>C \<and> g:B\<rightarrow>C"

  definition isCoproduct :: "'a \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> bool" 
    ("isCoproduct _ \<midarrow>_\<rightarrow> _ \<leftarrow>_\<midarrow> _" [120,120,120,120,120] 120) where
    "isCoproduct A \<midarrow>i1\<rightarrow> P \<leftarrow>i2\<midarrow> B \<equiv> A \<midarrow>i1\<rightarrow> P \<leftarrow>i2\<midarrow> B \<and> 
                           (\<forall>h T k. A \<midarrow>h\<rightarrow> T \<leftarrow>k\<midarrow> B \<longrightarrow> (\<exists>!u. u\<cdot>i1 \<simeq> h \<and> u\<cdot>i2 \<simeq> k))"

end

text \<open>We investigate from the viewpoint of a category that has all binary products: \<close>

context binaryProductCategory
begin

text \<open>Isabelle's automated routines can find finite examples of such categories for whatever 
      cardinality I tried: \<close>
lemma "True" nitpick[satisfy, card=3] oops

text \<open>Before investigating the distributive law it is essential to note, that the correct statement 
      is not that A \<times> (B \<squnion> C) is *equal* to (A \<times> B) \<squnion> (A \<times> C) but that they are equal up to
      isomorphism. The latter makes it harder to find counterexamples. Isabelle did not succeed in 
      finding finite counter models for the following lemma: \<close>

lemma
  fixes A B C :: "'a"
  assumes "isType A" and "isType B" and "isType C"
  assumes "isCoproduct B \<midarrow>i1\<rightarrow> B_C \<leftarrow>i2\<midarrow> C" (* read as B_C = B \<squnion> C *)
      and "isCoproduct (A \<^bold>\<times> B) \<midarrow>j1\<rightarrow> AB_AC \<leftarrow>j2\<midarrow> (A \<^bold>\<times> C)" 
          (* read as AB_AC = (A \<^bold>\<times> B) \<squnion> (A \<^bold>\<times> C) *)
  shows "(A \<^bold>\<times> B_C) isomorphicTo AB_AC"
  (* nitpick[card=6]  *)
  (* no counterexamples for card \<le> 6 *)
  oops

text \<open>A possible explanation is that this law holds in all finite categories which would mean that
      finding a counter model is not possible with Isabelle's tool that only consider finite models.
      Another possibility is that categories in which distributivity does not hold are simply quite
      large and the tool runs out of time too quickly. \<close>

end


end