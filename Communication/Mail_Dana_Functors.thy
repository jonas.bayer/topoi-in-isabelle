theory Mail_Dana_Functors imports "../Category" 

begin

section \<open>Functors and the category of categories\<close>

text \<open>This document explains how functors and the category of categories can be formalized based on
      the free logic embedding of category theory\<close>

text \<open>A record in Isabelle is nothing more than a named tuple, the following record only allows to 
      structure the formalization better.\<close>

record 'a abstract_category =
  existence :: "'a \<Rightarrow> bool"
  domain :: "'a \<Rightarrow> 'a"
  codomain :: "'a \<Rightarrow> 'a"
  composition :: "'a \<Rightarrow> 'a \<Rightarrow> 'a"
  inexistent_element :: "'a"

text \<open>We define what it means for this mere collection of functions to actually be a category. The 
      axioms of a category are defined in a different theory file and just referred to via the 
      name "category" below: \<close>

definition is_category :: "'a abstract_category \<Rightarrow> bool" where
  "is_category C \<equiv> category (existence C) 
                            (domain C) (codomain C) 
                            (composition C) (inexistent_element C)"

text \<open>Functors are simply maps between two types 'a and 'b that satisfy some given properties. In 
      order to be able to formalize the category of categories, we will start with a datatype that
      stores not only the map but also information about the category it maps from and to. We call
      these categories the origin and destination of the functor in order to not confuse the terms
      with domain and codomain\<close>

datatype ('a, 'b) abstract_functor = AbstractFunctor (map: "'a \<Rightarrow> 'b")
                                                     (origin: "'a abstract_category")
                                                     (destination: "'b abstract_category")

text \<open>The following definition declares when this abstract structure indeed fulfills the properties
      of a functor. To do so, it is necessary to acccess the notion of existence, domain, codomain
      and composition in both origin and destination category of the functor. For this reason, the 
      definition first defines e.g. E_O to be existence in the origin category in order to then be
      able to state the axioms in a more readable fashion (the axioms are really only the last
      four lines). \<close>

definition is_functor :: "('a, 'b) abstract_functor \<Rightarrow> bool" where
  "is_functor Functor \<equiv> let F = map Functor; 
                      E\<^sub>O = existence (origin Functor);
                      dom\<^sub>O = domain (origin Functor); cod\<^sub>O = codomain (origin Functor); 
                      comp\<^sub>O = composition (origin Functor); 
                      E\<^sub>D = existence (destination Functor);
                      dom\<^sub>D = domain (destination Functor); cod\<^sub>D = codomain (destination Functor); 
                      comp\<^sub>D = composition (destination Functor) 
                in (\<forall>x. E\<^sub>O x \<longleftrightarrow> E\<^sub>D (F x))
                 \<and> (\<forall>x. F (dom\<^sub>O x) = dom\<^sub>D (F x))
                 \<and> (\<forall>x. F (cod\<^sub>O x) = cod\<^sub>D (F x))
                 \<and> (\<forall>x y. F (comp\<^sub>O x y) = comp\<^sub>D (F x) (F y))" 

text \<open>As a mathematician might write this definition of a functor F: 
      - An element x exists in the origin category if and only if F(x) exists in the destination 
        category
      - The domain of F(x) is F(dom x) and the codomain of F(x) is F(cod x)
      - F(x \<cdot> y) = F(x) \<cdot> F(y)
      These can be summarized as F is compatible with existence, domain, codomain and composition.

      The more traditional object-based definitions follow easily from the one chosen here. This can
      straightforwardly be proven on paper, but I have not formalized that. \<close>

subsection \<open>The category of categories\<close>

text \<open>The following part shows how to show that functors form a category. I find this quite
      exciting, as one would not necessarily expect that it is at all possible to formally
      represent such a notion in a system like Isabelle.

      To be able to instantiate the category axioms it is necessary to restrict to functors on one
      type only (i.e. F:'a \<leadsto> 'a). Existence is then defined as follows: \<close>

definition functor_existence :: "('a, 'a) abstract_functor \<Rightarrow> bool" ("E\<^sub>F") where
  "E\<^sub>F F \<equiv> is_functor F \<and> is_category (origin F) \<and> is_category (destination F)"

text \<open>Note that restricting to categories over the type 'a does not mean restricting to
      endofunctors. The functors we allow to exist here can go from one category to a different 
      category, it is only required, that both category structures are built on the same type.
      This means that the formalized version of the category of categories is as "strong" as 
      Isabelle types are. \<close>

text \<open>A sanity check to show that not all abstract functors exist and technicalities in order to 
      obtain an inexistent element which will prove useful later. \<close>

lemma exists_inexistent: "\<exists>(F::('a, 'a) abstract_functor). \<not> E\<^sub>F F"
  unfolding functor_existence_def is_category_def 
  by (metis abstract_functor.sel(2) category_def select_convs(1) select_convs(5))

definition functor_inexistent_element :: "('a, 'a) abstract_functor" ("*") where
  "* \<equiv> (SOME F. \<not> E\<^sub>F F)"

lemma inexistent_element: "\<not> (E\<^sub>F *)" 
  unfolding functor_inexistent_element_def using exists_inexistent someI_ex by metis 

subsection \<open>Defining domain, codomain and composition\<close>

text \<open>The domain of a functor is, of course, the identity functor on its origin when considering
      objects as identity arrows. This motivates the following definition, which then allows to 
      easily define domain and codomain in the category of categories: \<close>

definition identity_functor :: "'a abstract_category \<Rightarrow> ('a, 'a) abstract_functor" where
  "identity_functor C = AbstractFunctor id C C"

definition functor_domain :: "('a, 'a) abstract_functor \<Rightarrow> ('a, 'a) abstract_functor" ("dom\<^sub>F") where
  "functor_domain F \<equiv> if E\<^sub>F F 
                      then identity_functor (origin F)
                      else *"

definition functor_codomain :: "('a, 'a) abstract_functor \<Rightarrow> ('a, 'a) abstract_functor" ("cod\<^sub>F") where
  "functor_codomain F \<equiv> if E\<^sub>F F 
                      then identity_functor (destination F)
                      else *"

subsection \<open>Defining composition\<close>

text \<open>First it is defined what it means for two functors to be composable and from this the 
      composition operation is built up. \<close>

definition composable :: "('a, 'a) abstract_functor \<Rightarrow> ('a, 'a) abstract_functor \<Rightarrow> bool" where
  "composable F G \<equiv> E\<^sub>F F \<and> E\<^sub>F G \<and> destination G = origin F"

definition composite :: "('a, 'a) abstract_functor \<Rightarrow> ('a, 'a) abstract_functor
                         \<Rightarrow> ('a, 'a) abstract_functor" where
  "composite F G \<equiv> AbstractFunctor (map F \<circ> map G) (origin G) (destination F)"

definition functor_composition :: "('a, 'a) abstract_functor \<Rightarrow> ('a, 'a) abstract_functor 
                                   \<Rightarrow> ('a, 'a) abstract_functor" (infix "\<cdot>" 110) where
  "F \<cdot> G \<equiv> if composable F G 
           then composite F G 
           else *"

text \<open>Two auxiliary lemmas that will be helpful in the instantiation proof.\<close>
lemma composite_existence: "composable F G \<Longrightarrow> E\<^sub>F (composite F G)"
  unfolding composable_def composite_def functor_existence_def is_functor_def Let_def 
            functor_domain_def functor_codomain_def identity_functor_def by auto

lemma composable: "E\<^sub>F (F \<cdot> G) \<longleftrightarrow> composable F G"
  apply(cases "composable F G", simp) 
    subgoal using composite_existence  unfolding functor_composition_def by auto 
    subgoal unfolding functor_composition_def using inexistent_element by auto
  done


subsection \<open>Instantiating the category of categories\<close>

text \<open>Define notation for Kleene equality and existing identity: \<close>
interpretation free_logic functor_existence by (unfold_locales)
notation KlEq (infixr "\<cong>" 56)
notation ExId (infixr "\<simeq>" 56) 

text \<open>With all of these definitions, it is now possible to prove that categories with functors as
      arrows form a category.

      All proofs annotated with "by sledgehammer" were entirely found through automated tools. The 
      only part that required some more manual work was proving associativity. \<close>

interpretation category functor_existence
                        functor_domain
                        functor_codomain
                        functor_composition
                        functor_inexistent_element
proof (unfold_locales)

 show "E\<^sub>F (dom\<^sub>F x) \<longrightarrow> E\<^sub>F x" for x :: "('a, 'a) abstract_functor" 
   by (simp add: functor_domain_def inexistent_element) (* by sledgehammer *)

 show "E\<^sub>F (cod\<^sub>F x) \<longrightarrow> E\<^sub>F x" for x :: "('a, 'a) abstract_functor" 
   by (simp add: functor_codomain_def inexistent_element) (* by sledgehammer *)

 show "\<not> E\<^sub>F *" using inexistent_element by auto

 show "E\<^sub>F (x \<cdot> y) = dom\<^sub>F x \<simeq> cod\<^sub>F y" for x y :: "('a, 'a) abstract_functor" 
  unfolding composable fl apply auto (* at this point, sledgehammer could prove all the subgoals *)
    subgoal by (simp add: composable_def functor_domain_def functor_existence_def 
                identity_functor_def is_functor_def)
    subgoal by (smt \<open>composable x y \<Longrightarrow> E\<^sub>F (dom\<^sub>F x)\<close> composable_def functor_codomain_def 
                functor_domain_def)
    subgoal by (simp add: composable_def functor_codomain_def functor_domain_def)
    subgoal by (smt \<open>\<And>x. E\<^sub>F (dom\<^sub>F x) \<longrightarrow> E\<^sub>F x\<close> abstract_functor.inject composable_def 
                functor_codomain_def functor_domain_def identity_functor_def)
    done

 show "x \<cdot> dom\<^sub>F x \<cong> x" for x :: "('a, 'a) abstract_functor" 
  by (smt ExId_def KlEq_def \<open>\<And>x. E\<^sub>F (dom\<^sub>F x) \<longrightarrow> E\<^sub>F x\<close> \<open>\<And>y x. E\<^sub>F (x \<cdot> y) = dom\<^sub>F x \<simeq> cod\<^sub>F y\<close> 
      abstract_functor.collapse abstract_functor.sel comp_id composable 
      composite_def functor_codomain_def functor_composition_def functor_domain_def 
      functor_existence_def id_def identity_functor_def is_functor_def) (* by sledgehammer *)

 show "cod\<^sub>F x \<cdot> x \<cong> x" for x :: "('a, 'a) abstract_functor" 
  by (smt ExId_def KlEq_def \<open>\<And>x. E\<^sub>F (cod\<^sub>F x) \<longrightarrow> E\<^sub>F x\<close> \<open>\<And>y x. E\<^sub>F (x \<cdot> y) = dom\<^sub>F x \<simeq> cod\<^sub>F y\<close> 
      abstract_functor.collapse abstract_functor.sel composable composite_def fun.map_id 
      functor_codomain_def functor_composition_def functor_domain_def functor_existence_def id_def 
      identity_functor_def is_functor_def) (* by sledgehammer *)

 show "(x \<cdot> y) \<cdot> z \<cong> x \<cdot> (y \<cdot> z)" for x y z :: "('a, 'a) abstract_functor" 
  unfolding fl apply (auto)
    subgoal 1
      proof - 
        assume "E\<^sub>F ((x \<cdot> y) \<cdot> z)"
        
        hence comp1: "composable (x \<cdot> y) z" 
          by (simp add: composable)
        
        hence comp2: "composable x y"
          by (simp add: composable composable_def)

        have "composable y z" 
          by (metis abstract_functor.collapse abstract_functor.inject comp1 comp2 composable_def 
              composite_def functor_composition_def)

        have "composable x (y \<cdot> z)" 
          by (metis \<open>composable y z\<close> abstract_functor.collapse abstract_functor.inject comp2 
              composable composable_def composite_def functor_composition_def)

        show "(x \<cdot> y) \<cdot> z = x \<cdot> (y \<cdot> z)" 
          by (metis (no_types, lifting) \<open>composable x (y \<cdot> z)\<close> \<open>composable y z\<close> abstract_functor.sel
               comp1 comp2 comp_assoc composite_def functor_composition_def) (* by sledgehammer *)
      qed
    subgoal
      by (metis (no_types, lifting) 1 abstract_functor.collapse abstract_functor.inject composable 
          composable_def composite_def functor_composition_def)
          (* by sledgehammer *)
    done

qed

end