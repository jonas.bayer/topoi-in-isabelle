theory Exponentials imports BinaryProductCategory

begin

section \<open>Exponentials\<close>

context binaryProductCategory 
begin

subsection \<open>Definition and simple properties\<close>
  
  definition expType :: "'a \<Rightarrow> 'a" where
    "expType e \<equiv> \<Pi>\<^sub>1 (dom e)"
  
  definition expOf :: "'a \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> bool" ("_ expOf _ by _" [120, 120, 120] 120) where
    "e expOf B by A \<equiv> e:(expType e)\<^bold>\<times>A\<rightarrow>B  \<and> (\<forall>g C. g:C\<^bold>\<times>A\<rightarrow>B \<longrightarrow> (\<exists>!g'. g \<simeq> e \<cdot> (g' \<^bold>\<times>\<^sub>a A)))"
  
  lemma expImpliesTypes: "e expOf B by A \<Longrightarrow> isType A \<and> isType B \<and> isType (expType e)"
    using expOf_def local.arrow_def local.productExistence fl
    by (meson expOf_def local.arrowImpliesIsType local.arrow_def local.productExistence)
      
  lemma domCodOfExponential:
    assumes "e expOf B by A"
    shows "dom e \<simeq> (expType e)\<^bold>\<times>A" and "cod e \<simeq> B" 
    using assms unfolding expOf_def arrow_def by blast+

subsection \<open>Exponentials and isomorphisms\<close>

  lemma McLarty6_1a:
    assumes "e expOf B by A"
    defines "I \<equiv> expType (dom e)"
    assumes "i:J\<rightarrow>I" and "isIsomorphism i"
      shows "(e \<cdot> (i \<^bold>\<times>\<^sub>a A)) expOf B by A" 
  proof - 
    have types: "isType A \<and> isType B \<and> isType I" using assms(1) expImpliesTypes I_def
      using assms(3) local.arrowImpliesIsType by blast

    have "e:I\<^bold>\<times>A\<rightarrow>B" 
      using expOf_def assms(1)
      by (metis I_def domCodOfExponential(1) expType_def local.isType_def 
          local.productExistsImpliesIsType ExId_def)
    hence mapOld: "e \<cdot> (i \<^bold>\<times>\<^sub>a A):J \<^bold>\<times> A\<rightarrow>B"
      unfolding arrow_def apply auto 
        subgoal unfolding fl
          using assms(3) local.arrow_def fl local.compositionExistence local.domImpliesExistence 
              local.isType_def local.productOfArrows(1) local.rightCompIdentity types 
          unfolding fl 
          by (smt KlEq_def associativity projection1Map)
        subgoal 
          using assms(3) local.arrowImpliesIsType fl local.productIsType local.typesExist types 
          by (metis \<open>\<lbrakk>dom e \<simeq> I \<^bold>\<times> A; cod e \<simeq> B\<rbrakk> \<Longrightarrow> dom e \<cdot> (i \<^bold>\<times>\<^sub>a A) \<simeq> J \<^bold>\<times> A\<close> associativity 
              compositionExistence domImpliesExistence leftCompIdentity) 
        done
  
    have map: "(e \<cdot> (i \<^bold>\<times>\<^sub>a A)):(expType (e \<cdot> (i \<^bold>\<times>\<^sub>a A))) \<^bold>\<times> A\<rightarrow>B"
      by (metis ExId_def assms(3) expType_def local.arrowImpliesIsType local.arrow_def local.factor1_def 
          local.projection1Map mapOld types)

    obtain j where j: "j inverseOf i"
      using assms(4) unfolding isIsomorphism_def by auto
    
    hence iso: "i \<cdot> j \<simeq> dom j"
      by (smt fl assms(3) local.arrow_def local.associativity local.compositionExistence 
          local.inverseOf_def local.isType_def local.rightCompIdentity types)

    have expProperty: "g:(C\<^bold>\<times>A)\<rightarrow>B \<longrightarrow> (\<exists>!g'. g \<simeq> (e \<cdot> (i \<^bold>\<times>\<^sub>a A)) \<cdot> (g' \<^bold>\<times>\<^sub>a A))" for g C 
    proof (rule impI)
      assume "g:(C\<^bold>\<times>A)\<rightarrow>B"

      hence exUnique: "\<exists>!h. g \<simeq> e \<cdot> (h \<^bold>\<times>\<^sub>a A) \<and> E h"
        using assms(1) unfolding expOf_def
        by (smt fl local.arrowProductExistence local.codImpliesExistence local.compositionExistence)
      
      have arrow: "g \<simeq> e \<cdot> (h \<^bold>\<times>\<^sub>a A) \<Longrightarrow> h:C\<rightarrow>I" for h
      proof - 
        assume asm: "g \<simeq> e \<cdot> (h \<^bold>\<times>\<^sub>a A)"
        hence "dom (h \<^bold>\<times>\<^sub>a A) \<simeq> C\<^bold>\<times>A"
          using exUnique local.arrowProductExistence2 local.associativity local.compositionExistence
                local.rightCompIdentity local.typesExist types \<open>g:C \<^bold>\<times> A\<rightarrow>B\<close> local.arrow_def 
          by (metis fl local.codImpliesExistence)
        moreover have "cod (h \<^bold>\<times>\<^sub>a A) \<simeq> I\<^bold>\<times>A"
          using asm exUnique local.compositionExistence \<open>e:I \<^bold>\<times> A\<rightarrow>B\<close> local.arrow_def fl by auto
        ultimately show ?thesis using domAndArrowProduct codAndArrowProduct
          by (metis ExId_def local.arrow_def local.productsEqualFactorsEqual) 
      qed 

      have keyIdentity: "g \<simeq> e \<cdot> (h \<^bold>\<times>\<^sub>a A) \<longleftrightarrow> (g \<simeq> e \<cdot> ((i \<^bold>\<times>\<^sub>a A) \<cdot> ((j \<cdot> h) \<^bold>\<times>\<^sub>a A)) \<and> h:C\<rightarrow>I)" for h 
      proof - 
        have "(g \<simeq> e \<cdot> (h \<^bold>\<times>\<^sub>a A)) = (g \<simeq> e \<cdot> (h \<^bold>\<times>\<^sub>a A) \<and> h:C\<rightarrow>I)"
          using arrow exUnique by auto
        also have "... = (g \<simeq> e \<cdot> (((i \<cdot> j) \<cdot> h) \<^bold>\<times>\<^sub>a (A \<cdot> A)) \<and> h:C\<rightarrow>I)"
          by (metis assms(3) assms(4) iso j local.arrow_def local.compositionExistence 
              local.isType_def local.isomorphismInverse local.leftCompIdentity 
              local.rightCompIdentity types fl)
       also have "... = (g \<simeq> e \<cdot> ((i \<cdot> (j \<cdot> h)) \<^bold>\<times>\<^sub>a (A \<cdot> A)) \<and> h:C\<rightarrow>I)"
        by (smt associativity assms(4) assms(3) iso j local.arrow_def local.compositionExistence 
            local.isType_def local.isomorphismInverse fl types)
       also have "... = (g \<simeq> e \<cdot> ((i \<^bold>\<times>\<^sub>a A) \<cdot> ((j \<cdot> h) \<^bold>\<times>\<^sub>a A)) \<and> h:C\<rightarrow>I)"
        by (smt \<open>e:I \<^bold>\<times> A\<rightarrow>B\<close> local.arrow_def local.codAndArrowProduct(1) local.codImpliesExistence 
            local.compositionAndArrowProduct fl local.compositionExistence local.isType_def 
            local.productOfArrows local.rightCompIdentity local.typesExist map types)
       finally show ?thesis by auto
      qed

      have exUniqueJ: "\<exists>!h. g \<simeq> e \<cdot> ((i \<^bold>\<times>\<^sub>a A) \<cdot> (h \<^bold>\<times>\<^sub>a A)) \<and> h:C\<rightarrow>J"
      proof -
        obtain h0 where h0: "g \<simeq> e \<cdot> ((i \<^bold>\<times>\<^sub>a A) \<cdot> ((j \<cdot> h0) \<^bold>\<times>\<^sub>a A)) \<and> h0:C\<rightarrow>I" 
          using exUnique keyIdentity by auto
        have exJ: "\<exists>h1. g \<simeq> e \<cdot> ((i \<^bold>\<times>\<^sub>a A) \<cdot> (h1 \<^bold>\<times>\<^sub>a A)) \<and> h1:C\<rightarrow>J"
          apply (rule exI[of _ "j \<cdot> h0"]) using h0 j
          by (metis assms(3) iso local.arrow_def fl local.associativity local.codImpliesExistence 
              local.compositionExistence local.rightCompIdentity)
        then obtain h1 where h1: "g \<simeq> e \<cdot> ((i \<^bold>\<times>\<^sub>a A) \<cdot> (h1 \<^bold>\<times>\<^sub>a A)) \<and> h1:C\<rightarrow>J" by auto
        have unique: "g \<simeq> e \<cdot> ((i \<^bold>\<times>\<^sub>a A) \<cdot> (h2 \<^bold>\<times>\<^sub>a A)) \<and> h2:C\<rightarrow>J \<Longrightarrow> h2 = h1" for h2
            using assms(3) assms(4) exUnique h1 fl local.arrow_def local.associativity 
                local.compositionAndArrowProduct local.compositionExistence 
                local.domImpliesExistence local.isMonic_def local.isType_def 
                local.isomorphismsMonic local.leftCompIdentity local.productOfArrows 
                local.productsEqualFactorsEqual map
            unfolding fl by smt
        show ?thesis using exJ unique by auto 
      qed

      have arrowJ: "g \<simeq> e \<cdot> ((i \<^bold>\<times>\<^sub>a A) \<cdot> (h \<^bold>\<times>\<^sub>a A)) \<Longrightarrow> h:C\<rightarrow>J" for h 
      proof - 
        assume asm: "g \<simeq> e \<cdot> ((i \<^bold>\<times>\<^sub>a A) \<cdot> (h \<^bold>\<times>\<^sub>a A))"
        hence "dom (h \<^bold>\<times>\<^sub>a A) \<simeq> C\<^bold>\<times>A"
          using exUnique local.arrowProductExistence2 local.associativity local.compositionExistence
                local.rightCompIdentity local.typesExist types \<open>g:C \<^bold>\<times> A\<rightarrow>B\<close> local.arrow_def 
          by (metis fl local.codImpliesExistence)
        moreover have "cod (h \<^bold>\<times>\<^sub>a A) \<simeq> J\<^bold>\<times>A"
          by (metis fl asm local.arrow_def local.associativity local.compositionExistence mapOld)
        ultimately show ?thesis using domAndArrowProduct codAndArrowProduct
          by (metis ExId_def local.arrow_def local.productsEqualFactorsEqual) 
      qed 
  
      thus "\<exists>!h. g  \<simeq> (e \<cdot> (i \<^bold>\<times>\<^sub>a A)) \<cdot> (h \<^bold>\<times>\<^sub>a A)" 
        using exUniqueJ arrowJ by (metis local.associativity fl)
    qed

    show ?thesis using map expProperty unfolding expOf_def by auto
  qed

end

end