theory CartesianClosedCategory imports Exponentials CartesianCategory 

begin

section \<open>Cartesian Closed Categories\<close>

locale cartesianClosedCategory = cartesianCategory + 
  fixes evaluation :: "'a \<Rightarrow> 'a \<Rightarrow> 'a" ("ev")

assumes 
        exponential: "isType A \<and> isType B \<Longrightarrow> (ev A B) expOf B by A" and

        evImpliesExistence:  "E (ev A B) \<Longrightarrow> isType A \<and> isType B"
begin
  
  lemma "True" nitpick[satisfy] oops
  
  definition exponential :: "'a \<Rightarrow> 'a \<Rightarrow> 'a" (infix "exp" 120) where
    "B exp A \<equiv> expType (ev A B)"

  lemma evMap: "isType A \<Longrightarrow> isType B \<Longrightarrow> (ev A B):(B exp A)\<^bold>\<times>A\<rightarrow>B"
    unfolding exponential_def using exponential unfolding expOf_def by blast

  lemma expIsType: "isType A \<Longrightarrow> isType B \<Longrightarrow> isType (B exp A)"
    using evMap local.arrowImpliesIsType local.productIsType by blast

subsection \<open>Transposes\<close>
  
subsubsection \<open>Preliminary notions\<close>

  definition isMapFromProduct :: "'a \<Rightarrow> bool" where
    "isMapFromProduct g \<equiv> (\<exists>A B. dom g \<simeq> A \<^bold>\<times> B)"
 
  lemma mapFromProductMap: "isMapFromProduct g \<Longrightarrow> g:\<Pi>\<^sub>1 (dom g)\<^bold>\<times> \<Pi>\<^sub>2 (dom g)\<rightarrow>cod g"
    by (metis isMapFromProduct_def local.arrow_def local.compositionExistence 
        local.domImpliesExistence local.factorizes_def local.leftCompIdentity 
        local.multiplyFactors fl)

  definition isMapFromProduct2 :: "'a \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> bool" ("_:-\<^bold>\<times>_\<rightarrow>_" [120, 120, 120] 120) where
    "f:-\<^bold>\<times>A\<rightarrow>B \<equiv> (\<exists>C. f:C\<^bold>\<times>A\<rightarrow>B)"

  lemma isMapFromProduct2ImpliesType:
    "f:-\<^bold>\<times>A\<rightarrow>B \<Longrightarrow> isType A \<and> isType B"
    unfolding isMapFromProduct2_def using local.arrowImpliesIsType local.productIsType by blast

subsubsection \<open>Reverse transpose\<close>
  
  definition transpose2 :: "'a \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> 'a" 
    ("transpose2 _ for _ exp _" [120, 120, 120] 120) where
    "transpose2 f for B exp A \<equiv> (ev A B) \<cdot> (f \<^bold>\<times>\<^sub>a A)"

  lemma transpose2Map:
    assumes "cod f \<simeq> B exp A" and "isType A" and "isType B"
      shows "transpose2 f for B exp A:(dom f) \<^bold>\<times> A\<rightarrow>B"
    unfolding arrow_def transpose2_def apply (rule conjI)
    using evMap[of A B] apply(simp add: assms) 
      subgoal using productOfArrows(1)[of f A] using assms unfolding arrow_def isType_def 
        by (smt local.associativity local.codImpliesExistence local.compositionExistence 
            local.rightCompIdentity fl) 
      subgoal 
        by (smt fl \<open>\<lbrakk>isType A; isType B\<rbrakk> \<Longrightarrow> ev A B:(B exp A) \<^bold>\<times> A\<rightarrow>B\<close> 
            \<open>ev A B:(B exp A) \<^bold>\<times> A\<rightarrow>B \<Longrightarrow> dom ev A B \<cdot> (f \<^bold>\<times>\<^sub>a A) \<simeq> (dom f) \<^bold>\<times> A\<close> assms(2) assms(3) 
            local.arrow_def local.associativity local.compositionExistence 
            local.domImpliesExistence local.finalMapConcatenation)
      done

subsubsection \<open>Normal transpose\<close>

  definition transpose1 :: "'a \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> 'a" 
    ("transpose1 _ for _ exp _" [120, 120, 120] 120) where
    "transpose1 g for B exp A \<equiv> (THE g'. g:-\<^bold>\<times>A\<rightarrow>B \<longrightarrow> g \<simeq> ev A B \<cdot> (g' \<^bold>\<times>\<^sub>a A) 
                                    \<and> (\<not> g:-\<^bold>\<times>A\<rightarrow>B \<longrightarrow> g' = *))"

  lemma transpose1Unique: 
    assumes "isType A" and "isType B"
    shows "\<exists>!g'. (g:-\<^bold>\<times>A\<rightarrow>B \<longrightarrow> g \<simeq> ev A B \<cdot> (g' \<^bold>\<times>\<^sub>a A)) 
            \<and> (\<not> g:-\<^bold>\<times>A\<rightarrow>B \<longrightarrow> g' = *)"
    apply (cases "g:-\<^bold>\<times>A\<rightarrow>B", simp_all) unfolding isMapFromProduct2_def using assms exponential 
    unfolding expOf_def by auto

  lemma transpose1Map:
    assumes "g:-\<^bold>\<times>A\<rightarrow>B" 
      shows "transpose1 g for B exp A:\<Pi>\<^sub>1 (dom g)\<rightarrow>(B exp A)"
  proof -
    have exUnique: "\<exists>!g'. g \<simeq> ev A B \<cdot> (g' \<^bold>\<times>\<^sub>a A)" 
      using assms isMapFromProduct2ImpliesType transpose1Unique by blast
    define g' where "g' \<equiv> THE g'. g \<simeq> ev A B \<cdot> (g' \<^bold>\<times>\<^sub>a A)"
    have "g \<simeq> ev A B \<cdot> (g' \<^bold>\<times>\<^sub>a A)" 
      unfolding g'_def using exUnique theI'[of "\<lambda>x. g \<simeq> ev A B \<cdot> (x \<^bold>\<times>\<^sub>a A)"] by blast
    hence "g':\<Pi>\<^sub>1 (dom g)\<rightarrow>(B exp A)" 
       by (smt assms(1) exponential_def isMapFromProduct2_def isMapFromProduct_def 
          local.arrowImpliesIsType local.arrowProductExistence local.arrow_def local.associativity 
          local.codImpliesExistence local.compositionExistence local.expType_def local.finalMap 
          local.finalMapConcatenation local.productOfArrows(1) local.productsEqualFactorsEqual 
          mapFromProductMap fl)
    thus ?thesis unfolding g'_def transpose1_def using assms by auto
  qed

  lemma transpose1Property:
    assumes "g:-\<^bold>\<times>A\<rightarrow>B"
      shows "g \<simeq> ev A B \<cdot> ((transpose1 g for B exp A) \<^bold>\<times>\<^sub>a A)"
    unfolding transpose1_def using assms apply simp 
    using theI'[of "\<lambda>g'. g \<simeq> ev A B \<cdot> (g' \<^bold>\<times>\<^sub>a A)"] isMapFromProduct2ImpliesType transpose1Unique
    by (metis (no_types, lifting))

  lemma transpose1Uniqueness:
    assumes "g:-\<^bold>\<times>A\<rightarrow>B"
        and "g \<simeq> ev A B \<cdot> (h \<^bold>\<times>\<^sub>a A)"
      shows "h \<simeq> (transpose1 g for B exp A)"
    by (metis isMapFromProduct2ImpliesType assms(1) assms(2) local.arrowProductExistence ExId_def
        local.codImpliesExistence local.compositionExistence transpose1Unique transpose1Property)

subsubsection \<open>Transpose of transpose\<close>
  
  lemma transpose2Oftranspose1:
    assumes "g:-\<^bold>\<times>A\<rightarrow>B"
    shows "transpose2 (transpose1 g for B exp A) for B exp A \<simeq> g" 
    using assms unfolding transpose1_def transpose2_def apply simp_all 
    using transpose1Property[of g A B] fl transpose1_def by auto

  lemma transpose1Oftranspose2:
    assumes "cod f \<simeq> B exp A"
    shows "transpose1 (transpose2 f for B exp A) for B exp A \<simeq> f"
  proof - 
    have "isType A" and "isType B" using assms
      by (metis exponential_def ExId_def local.codImpliesExistence local.domImpliesExistence 
          local.evImpliesExistence local.expType_def local.factor1_def local.projection1Existence)+

    hence "ev A B \<cdot> (f \<^bold>\<times>\<^sub>a A):-\<^bold>\<times>A\<rightarrow>B"
      by (smt assms evMap fl isMapFromProduct2_def local.arrowComposition local.codImpliesExistence 
          local.compositionExistence local.isType_def local.productOfArrows(1) 
          local.rightCompIdentity fl)

    moreover have "\<exists>!g'. ev A B \<cdot> (f \<^bold>\<times>\<^sub>a A) \<simeq> ev A B \<cdot> (g' \<^bold>\<times>\<^sub>a A)"
      using \<open>ev A B \<cdot> (f \<^bold>\<times>\<^sub>a A):-\<^bold>\<times>A\<rightarrow>B\<close> fl \<open>isType A\<close> \<open>isType B\<close> transpose1Unique by blast
      
    ultimately show ?thesis unfolding transpose2_def transpose1_def fl apply simp
      using assms local.codImpliesExistence fl the1_equality unfolding fl by smt
  qed

  lemma transpose2Equal:
    assumes "transpose2 f for B exp A \<simeq> transpose2 g for B exp A"
      shows "f \<simeq> g"
    using assms unfolding transpose2_def
    by (metis assms evMap local.arrow_def local.codAndArrowProduct(1) local.compositionExistence 
        local.domImpliesExistence local.evImpliesExistence local.productsEqualFactorsEqual 
        transpose1Oftranspose2 ExId_def)

subsection \<open>The name of arrows\<close>

  definition name :: "'a \<Rightarrow> 'a" ("`_\<acute>") where
    "`f\<acute> \<equiv> let A = dom f; B = cod f in 
            transpose1 f \<cdot> \<pi>\<^sub>2 (\<one> \<^bold>\<times> A) for B exp A"
  

  lemma nameMap: "E f \<Longrightarrow> `f\<acute>:\<one>\<rightarrow>(cod f) exp (dom f)"
  proof - 
    assume "E f"
    have "(f \<cdot> \<pi>\<^sub>2 (\<one> \<^bold>\<times> (dom f))):-\<^bold>\<times>dom f\<rightarrow>cod f"
      unfolding isMapFromProduct2_def apply (rule exI[of _ \<one>]) unfolding arrow_def 
      apply (rule conjI)
      subgoal 
        by (smt fl \<open>E f\<close> local.arrow_def local.associativity local.compositionExistence 
            local.domImpliesExistence local.finalMap local.finalMapConcatenation 
            local.finalMapImpliesType local.projection2Map)
      subgoal
        by (metis (no_types, lifting) \<open>dom f \<cdot> \<pi>\<^sub>2 (\<one> \<^bold>\<times> (dom f)) \<simeq> \<one> \<^bold>\<times> (dom f)\<close> local.associativity 
            local.compositionExistence fl local.domImpliesExistence local.finalMapConcatenation)
      done

    moreover have "\<Pi>\<^sub>1 (dom f \<cdot> \<pi>\<^sub>2 (\<one> \<^bold>\<times> (dom f))) \<simeq> \<one>" 
      unfolding factor1_def
      by (smt fl calculation isMapFromProduct2_def local.arrowImpliesIsType local.arrow_def 
          local.associativity local.domImpliesExistence local.finalMap local.finalMapConcatenation 
          local.productExistence local.projection1Map local.projection2Map)

    ultimately show ?thesis unfolding exponential_def name_def Let_def 
      using transpose1Map[of "f \<cdot> \<pi>\<^sub>2 (\<one> \<^bold>\<times> (dom f))"] unfolding arrow_def exponential_def 
      by (metis (full_types) ExId_def)
  qed


subsection \<open>Exponentials of arrows\<close>

  definition arrowExponential :: "'a \<Rightarrow> 'a \<Rightarrow> 'a" ("_ exp\<^sub>a _" [120, 120] 120) where
    "f exp\<^sub>a A \<equiv> let B = dom f; C = cod f in 
                            transpose1 (f \<cdot> ev A B) for C exp A"

  lemma arrowExponentialMap: 
    "E f \<Longrightarrow> isType A \<Longrightarrow> f exp\<^sub>a A:(dom f) exp A\<rightarrow>(cod f) exp A"
  proof -
    assume "E f"
    assume "isType A"
    have "f \<cdot> ev A (dom f):-\<^bold>\<times>A\<rightarrow>cod f" unfolding isMapFromProduct2_def 
      apply (rule exI[of _ "(dom f) exp A"]) using evMap[of A "dom f"] 
      by (meson \<open>E f\<close> \<open>isType A\<close> local.arrowComposition local.arrow_def local.compositionExistence 
          local.finalMapConcatenation fl local.finalMapImpliesType) 

    moreover have "\<Pi>\<^sub>1 (dom f \<cdot> ev A (dom f)) = (dom f) exp A" 
      by (metis \<open>f \<cdot> ev A (dom f):-\<^bold>\<times>A\<rightarrow>cod f\<close> exponential_def isMapFromProduct2_def local.arrow_def 
          local.associativity fl local.codImpliesExistence local.compositionExistence 
          local.expType_def local.rightCompIdentity)

    ultimately show ?thesis unfolding arrowExponential_def Let_def
      using transpose1Map[of "(f \<cdot> ev A (dom f))"]
      using \<open>isType A\<close> isMapFromProduct_def local.arrowImpliesIsType by auto 
  qed

  lemma internalizedComposition:
    assumes "cod h \<simeq> dom f"
      shows "(f exp\<^sub>a (dom h)) \<cdot> (`h\<acute>) \<simeq> `f \<cdot> h\<acute>"
  proof - 
    define A where "A \<equiv> dom h"
    define B where "B \<equiv> cod h"
    define C where "C \<equiv> cod f"
    define p where "p \<equiv> \<pi>\<^sub>2 (\<one> \<^bold>\<times> A)"

    have "f \<cdot> ev (dom h) (dom f):-\<^bold>\<times>A\<rightarrow>C" 
      unfolding A_def C_def isMapFromProduct2_def apply(rule exI[of _ "(dom f) exp (dom h)"]) 
      using evMap[of "dom h" "dom f"] using arrowComposition[of "ev (dom h) (dom f)"]
      by (metis assms local.arrow_def local.codImpliesExistence local.compositionExistence 
          local.isType_def fl local.leftCompIdentity local.rightCompIdentity)

    have "dom f\<cdot>h \<simeq> dom h" and "cod f\<cdot>h \<simeq> cod f" using assms catAx fl by smt+ 

    have map: "(f \<cdot> h) \<cdot> \<pi>\<^sub>2 (\<one> \<^bold>\<times> A):-\<^bold>\<times>A\<rightarrow>C"
      unfolding isMapFromProduct2_def A_def C_def
      by (metis isMapFromProduct2ImpliesType A_def \<open>cod f \<cdot> h \<simeq> cod f\<close> \<open>dom f \<cdot> h \<simeq> dom h\<close> 
          \<open>f \<cdot> ev (dom h) (dom f):-\<^bold>\<times>A\<rightarrow>C\<close> local.arrowComposition local.arrow_def 
          local.finalObject local.isFinal_def local.projection2Map ExId_def)

    have "h \<cdot> \<pi>\<^sub>2 (\<one> \<^bold>\<times> (dom h)):-\<^bold>\<times>A\<rightarrow>B"
      by (smt A_def B_def isMapFromProduct2_def local.arrow_def fl local.associativity 
          local.codImpliesExistence local.compositionExistence local.domImpliesExistence 
          local.finalMap local.finalMapConcatenation local.finalMapImpliesType map)

    have "transpose2 (f exp\<^sub>a A) \<cdot> (`h\<acute>) for C exp A = ev A C \<cdot> (((f exp\<^sub>a A) \<cdot> (`h\<acute>)) \<^bold>\<times>\<^sub>a A)"
      unfolding transpose2_def by simp

    also have "... = ev A C \<cdot> (((f exp\<^sub>a A) \<^bold>\<times>\<^sub>a A) \<cdot> ((`h\<acute>) \<^bold>\<times>\<^sub>a A))"
      by (metis isMapFromProduct2ImpliesType A_def \<open>dom f \<cdot> h \<simeq> dom h\<close> fl arrowExponentialMap 
          local.arrow_def local.compositionAndArrowProduct local.compositionExistence 
          local.domImpliesExistence local.isType_def local.rightCompIdentity map nameMap)
  
    also have "... = (ev A C \<cdot> ((f exp\<^sub>a A) \<^bold>\<times>\<^sub>a A)) \<cdot> ((`h\<acute>) \<^bold>\<times>\<^sub>a A)"
      using associativity 
      by (smt isMapFromProduct2ImpliesType A_def C_def \<open>dom f \<cdot> h \<simeq> dom h\<close> fl arrowExponentialMap 
          local.arrow_def local.compositionExistence local.domImpliesExistence 
          local.productOfArrows(1) local.rightCompIdentity map nameMap transpose1Map 
          transpose1Property) 

    also have "... = (f \<cdot> ev A B) \<cdot> ((`h\<acute>) \<^bold>\<times>\<^sub>a A)"
      using A_def B_def C_def \<open>f \<cdot> ev (dom h) (dom f):-\<^bold>\<times>A\<rightarrow>C\<close> arrowExponential_def assms 
            transpose1Property fl by auto
      
    also have "... = f \<cdot> (ev A B  \<cdot> ((`h\<acute>) \<^bold>\<times>\<^sub>a A))"
      by (smt isMapFromProduct2ImpliesType A_def B_def C_def \<open>dom f \<cdot> h \<simeq> dom h\<close> 
          \<open>f \<cdot> ev (dom h) (dom f):-\<^bold>\<times>A\<rightarrow>C\<close> arrowExponentialMap arrowExponential_def 
          local.arrow_def local.associativity local.compositionExistence local.domImpliesExistence 
          local.isType_def local.productOfArrows(1) local.rightCompIdentity nameMap 
          transpose1Property fl)

    also have "... = f \<cdot> (h \<cdot> p)"
      unfolding p_def name_def Let_def using transpose2Oftranspose1[of _ A B] 
      unfolding transpose2_def using A_def B_def \<open>h \<cdot> \<pi>\<^sub>2 (\<one> \<^bold>\<times> (dom h)):-\<^bold>\<times>A\<rightarrow>B\<close> fl by auto

    also have "... = (f \<cdot> h) \<cdot> p"
      using associativity by (metis map p_def transpose1Property fl)

    also have "... = transpose2 `f \<cdot> h\<acute> for C exp A" 
      by (metis A_def C_def \<open>cod f \<cdot> h \<simeq> cod f\<close> \<open>dom f \<cdot> h \<simeq> dom h\<close> map name_def p_def 
          transpose2Oftranspose1 ExId_def)

    finally have "transpose2 (f exp\<^sub>a A) \<cdot> (`h\<acute>) for C exp A = transpose2 `f \<cdot> h\<acute> for C exp A"
      by auto

    hence "transpose2 (f exp\<^sub>a A) \<cdot> (`h\<acute>) for C exp A \<simeq> transpose2 `f \<cdot> h\<acute> for C exp A"
      using \<open>(f \<cdot> h) \<cdot> p = transpose2 `f \<cdot> h\<acute> for C exp A\<close> map p_def transpose1Property fl by auto

    thus ?thesis using transpose2Equal[of "(f exp\<^sub>a A) \<cdot> (`h\<acute>)" "C" "A" "`f \<cdot> h\<acute>"] 
    unfolding A_def C_def by auto
  qed

subsection \<open>Properties of Exponentials\<close>
  
  text \<open>The right way to think about the next theorem is to consider both sides as identity arrows\<close>
  lemma identityArrowExponential:
    assumes "isType A" and "isType B"
      shows "B exp A \<simeq> B exp\<^sub>a A" 
  proof -
    have "dom B \<simeq> B" and "cod B \<simeq> B" using assms 
      by (smt assms(2) local.compositionExistence local.isType_def local.rightCompIdentity fl)+

    have "B \<cdot> ev A (dom B):-\<^bold>\<times>A\<rightarrow>B"
      by (metis \<open>dom B \<simeq> B\<close> assms(1) assms(2) evMap isMapFromProduct2_def local.compositionExistence 
          local.domCodOfExponential(2) local.exponential local.leftCompIdentity fl)

    moreover have "B \<cdot> ev A (dom B) \<simeq> ev A B \<cdot> ((B exp A) \<^bold>\<times>\<^sub>a A)" 
      by (smt arrowExponentialMap arrowExponential_def assms(1) assms(2) calculation 
          local.arrow_def local.associativity local.compositionAndArrowProduct fl
          local.compositionExistence local.isType_def local.leftCompIdentity 
          local.productOfArrows(1) local.rightCompIdentity transpose1Property transpose2Equal 
          transpose2Oftranspose1)

    ultimately show ?thesis
      unfolding arrowExponential_def Let_def using transpose1Uniqueness 
      by (metis ExId_def \<open>cod B \<simeq> B\<close>)
  qed

end

end