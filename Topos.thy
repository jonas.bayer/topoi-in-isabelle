theory Topos imports CartesianClosedCategory

begin

section \<open>Topoi\<close>

locale topos = cartesianClosedCategory +
  fixes subobjectClassifier :: "'a" ("\<Omega>") and
        true :: "'a" ("t") 
  assumes 
        trueMap: "t:\<one>\<rightarrow>\<Omega>" and
        toposProperty: "isMonic s \<longrightarrow> (\<exists>!\<chi>. \<chi>:dom s\<rightarrow>\<Omega> \<and> isPullbackDiagram \<box> \<midarrow>!\<^sub>1 (dom s)\<rightarrow> \<box> 
                                                                           s\<down>               \<down>t 
                                                                           \<box>     \<midarrow> \<chi>\<rightarrow>     \<box>  )" 
begin

subsection \<open>The classifying arrow\<close>

  definition classifyingArrow :: "'a \<Rightarrow> 'a" ("\<chi>") where
    "\<chi> s \<equiv> (THE x. (isMonic s \<longrightarrow> x:dom s\<rightarrow>\<Omega> \<and> isPullbackDiagram \<box> \<midarrow>!\<^sub>1 (dom s)\<rightarrow> \<box> 
                                                                 s\<down>               \<down>t 
                                                                  \<box>     \<midarrow> x\<rightarrow>    \<box>  ) 
                 \<and> (\<not> isMonic s \<longrightarrow> x = *))"
  
  lemma toposPropertyLemma:
    assumes "isMonic s"
    shows "(\<chi> s):dom s\<rightarrow>\<Omega>" and "isPullbackDiagram \<box> \<midarrow>!\<^sub>1 (dom s)\<rightarrow> \<box> 
                                                 s\<down>               \<down>t 
                                                 \<box>    \<midarrow>\<chi> s\<rightarrow>     \<box> "
  proof - 
    presume "isMonic s"
    hence "\<exists>!x. x:dom s\<rightarrow>\<Omega> \<and> isPullbackDiagram \<box> \<midarrow>!\<^sub>1 (dom s)\<rightarrow> \<box> 
                                              s\<down>               \<down>t 
                                               \<box>     \<midarrow> x\<rightarrow>     \<box>  " using toposProperty by auto
    moreover have "\<chi> s = (THE x::'a. x:dom s\<rightarrow>\<Omega> \<and> isPullbackDiagram \<box> \<midarrow>!\<^sub>1 (dom s)\<rightarrow> \<box> 
                                                                    s\<down>               \<down>t 
                                                                    \<box>     \<midarrow> x\<rightarrow>     \<box> )" 
      using classifyingArrow_def[of s] \<open>isMonic s\<close> by auto

    ultimately show "(\<chi> s):dom s\<rightarrow>\<Omega>" and "isPullbackDiagram \<box> \<midarrow>!\<^sub>1 (dom s)\<rightarrow> \<box> 
                                                            s\<down>               \<down>t 
                                                            \<box>    \<midarrow>\<chi> s\<rightarrow>     \<box> "
      using theI'[of "\<lambda>x. x:dom s\<rightarrow>\<Omega> \<and> isPullbackDiagram \<box> \<midarrow>!\<^sub>1 (dom s)\<rightarrow> \<box> 
                                                         s\<down>               \<down>t 
                                                          \<box>     \<midarrow> x\<rightarrow>     \<box> "] 
      by smt+
  qed (fact assms)+


subsection \<open>Porperties of Monics and Epics in Topoi\<close>
  
  lemma monicsAreEqualizers: "isMonic s \<Longrightarrow> s equalizerOf (\<chi> s) (t \<cdot> !\<^sub>1 (cod s))"
  proof - 
    assume "isMonic s"
    hence characteristicMap: "\<chi> s:dom s\<rightarrow>\<Omega> \<and> isPullbackDiagram \<box> \<midarrow>!\<^sub>1 (dom s)\<rightarrow> \<box>
                                                               s\<down>               \<down>t 
                                                               \<box>    \<midarrow> \<chi> s \<rightarrow>   \<box>"
      using toposPropertyLemma by auto
    
    have parallelPair: "isParallelPair (\<chi> s) (t \<cdot> !\<^sub>1 (cod s))"
      unfolding isParallelPair_def
      by (smt characteristicMap local.arrow_def local.associativity local.compositionExistence 
          local.isPullback_def local.leftCompIdentity local.rightCompIdentity fl)
  
    have equalizes: "s equalizes (\<chi> s) (t \<cdot> !\<^sub>1 (cod s))"
      using finalMapConcatenation[of s] \<open>isMonic s\<close> unfolding equalizes_def isMonic_def 
      using characteristicMap unfolding isPullback_def using associativity fl by smt 
  
    have "h equalizes (\<chi> s) (t \<cdot> !\<^sub>1 (cod s)) \<longrightarrow> h factorsUniquelyThrough s" for h 
    proof (rule impI) 
      assume "h equalizes (\<chi> s) (t \<cdot> !\<^sub>1 (cod s))"
  
      hence "t \<cdot> !\<^sub>1 (dom h) \<simeq> \<chi> s \<cdot> h" unfolding equalizes_def 
        by (metis equalizes local.associativity local.codImpliesExistence local.compositionExistence 
            local.equalizes_def local.finalMapConcatenation fl)
  
      thus "h factorsUniquelyThrough s" 
        unfolding factorsUniquelyThrough_def using characteristicMap 
        unfolding isPullback_def by (metis \<open>isMonic s\<close> ExId_def local.isMonic_def)
    qed
  
    thus ?thesis
    unfolding equalizerOf_def using parallelPair equalizes by auto
  qed
  
  lemma "isMonic m \<Longrightarrow> isEpi m \<Longrightarrow> isIsomorphism m"
    using monicsAreEqualizers epicEqualizerIsIsomorphism by blast

subsection \<open>t is monic\<close>

  lemma tIsMonic: "isMonic t"
    by (smt local.arrowImpliesIsType local.arrow_def local.associativity local.compositionExistence 
        local.finalMap local.finalMapConcatenation local.finalObject local.isFinal_def 
        local.isMonic_def local.leftCompIdentity local.product_final local.products_isomorphism 
        local.trueMap fl)

subsection \<open>Conjunction and intersection\<close>

  (* This lemma is tricky [currently not formulated correctly] and will quite some effort. For now
     let's just hope it's not needed later. *)
  lemma 
    assumes "r isSubobjectOf A" and "s isSubobjectOf A"
    defines "R \<equiv> dom r" and "S \<equiv> dom s"
      shows "isPullbackDiagram  \<box> \<midarrow>(!\<^sub>1 R) \<^bold>\<times>\<^sub>a (t \<cdot> (!\<^sub>1 R))\<rightarrow> \<box> 
                               r\<down>                           \<down>(t \<^bold>\<times>\<^sub>a \<Omega>) 
                                \<box>     \<midarrow> \<langle>\<chi> r, \<chi> s\<rangle> \<rightarrow>       \<box>"

        and "isPullbackDiagram  \<box> \<midarrow>(t \<cdot> (!\<^sub>1 S)) \<^bold>\<times>\<^sub>a (!\<^sub>1 S)\<rightarrow> \<box> 
                               s\<down>                           \<down>(\<Omega> \<^bold>\<times>\<^sub>a t) 
                                \<box>     \<midarrow> \<langle>\<chi> r, \<chi> s\<rangle> \<rightarrow>       \<box> "
    oops


  text \<open>Mc Larty Theorem 13.3\<close>
  lemma tIntersection:
    shows "isPullbackDiagram  \<box>  \<midarrow>\<langle>\<one>, t\<rangle>\<rightarrow>  \<box> 
                        \<langle>t, \<one>\<rangle>\<down>              \<down>(t \<^bold>\<times>\<^sub>a \<Omega>) 
                              \<box> \<midarrow>(\<Omega> \<^bold>\<times>\<^sub>a t)\<rightarrow> \<box>"
    unfolding isPullback_def using inducedByProperty unfolding inducedBy_def sorry 



(* Think about what arrows from A to \<Omega> look like i.e. how they factor through t \<cdot> !\<^sub>1 A *)

subsection \<open>Further definitions\<close>
  
  definition toposAndArrow :: "'a" ("AND\<^sub>E") where
    "AND\<^sub>E \<equiv> \<chi> (\<langle>t, t\<rangle>)"
  
  lemma toposAndArrowMap: "AND\<^sub>E:\<Omega>\<^bold>\<times>\<Omega>\<rightarrow>\<Omega>"
    by (smt local.arrow_def local.compositionExistence local.inducedByAndMonics local.inducedByMap 
        local.isPullback_def local.trueMap tIsMonic toposAndArrow_def toposPropertyLemma(1) 
        toposPropertyLemma(2) fl)


    
  definition toposAnd :: "'a \<Rightarrow> 'a \<Rightarrow> 'a" (infix "\<and>\<^sub>E" 120) where
    "h \<and>\<^sub>E k \<equiv> AND\<^sub>E \<cdot> (\<langle>h, k\<rangle>)" 

  definition ToposLtArrow :: "'a" ("LT\<^sub>E") where
    "LT\<^sub>E \<equiv> AND\<^sub>E \<doteq> \<pi>\<^sub>1(\<Omega> \<^bold>\<times> \<Omega>)"
  
  definition ToposLt :: "'a \<Rightarrow> 'a \<Rightarrow> bool" (infix "\<le>\<^sub>E" 120) where
    "v \<le>\<^sub>E w \<equiv> (\<langle>v, w\<rangle>) memberOf LT\<^sub>E"
  
  definition ToposMaterialConditionalArrow :: "'a" ("MTA\<^sub>E") where
    "MTA\<^sub>E \<equiv> \<chi> LT\<^sub>E"
  
  definition ToposMaterialConditional :: "'a \<Rightarrow> 'a \<Rightarrow> 'a" (infix "\<rightarrow>\<^sub>E" 120) where
    "h \<rightarrow>\<^sub>E k \<equiv> MTA\<^sub>E \<cdot> (\<langle>h, k\<rangle>)"

end


end